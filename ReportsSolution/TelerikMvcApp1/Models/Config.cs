﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace TelerikMvcApp1.Models
{
   
    public  class Config
    {
        ThemisEntities objthemisEntities = new ThemisEntities();
        
        public string strUserKey     {get;set;}
        public List<int>  FacilityKey     { get; set; }
        public List<string> FacilityName        { get; set; }

        public Dictionary<int,string> getDetails(int userid)
        {
            Dictionary<int, string> objdictionary = new Dictionary<int, string>();
            var FacilityName = "";
            var FacilityKey = "";
            var facility= objthemisEntities.usp_FacilitiesForUser_Select(userid).AsEnumerable().ToList();
            foreach(var fk in facility)
            {
               
                if (string.IsNullOrEmpty(FacilityName))
                {
                    FacilityName = fk.FACILITY_NAME.ToString();
                }
                else {
                    FacilityName = FacilityName+"$" + fk.FACILITY_NAME.ToString(); }

               
                if (string.IsNullOrEmpty(FacilityKey))
                {
                    FacilityKey = fk.MEDICAL_FACILITY_KEY.ToString();
                }
                else
                {
                    FacilityKey = FacilityKey+ "$" + fk.MEDICAL_FACILITY_KEY.ToString();
                }
               
                
            }
            if (FacilityKey.Length > 1)
            {
                this.FacilityKey = FacilityKey.Split('$').Select(Int32.Parse).ToList();
                this.FacilityName = FacilityName.Split('$').ToList();
                for (int i = 0; i < this.FacilityKey.Count; i++)
                {
                    objdictionary.Add(this.FacilityKey[i], this.FacilityName[i]);
                }
            }
            return objdictionary;

        }
        
        public IOrderedQueryable<Facilityclass> PopulateFacilityDropDownData(int id,int sptype)
        {
            var tmpList=new List<Facilityclass>();
            if (sptype == 1)
            {
                tmpList = (from a in objthemisEntities.usp_FacilitiesForUser_Select(id)
                               select new Facilityclass
                               {
                                   FacilityKey = a.MEDICAL_FACILITY_KEY,
                                   FacilityName = a.FACILITY_NAME
                               }).ToList();
                return (IOrderedQueryable<Facilityclass>)tmpList.AsQueryable();
            }
            else if(sptype==2)
            {
                tmpList = (from a in objthemisEntities.usp_Metis_FacilitiesForUser_Select(id)
                               select new Facilityclass
                               {
                                   FacilityKey = a.MEDICAL_FACILITY_KEY,
                                   FacilityName = a.FACILITY_NAME
                               }).ToList();
               
            }
            return (IOrderedQueryable<Facilityclass>)tmpList.AsQueryable();

        }

        public IOrderedQueryable<RoutingClass> PopulateRotuingDropDownData(int? rty)
        {
            var tmpList = (from rt in objthemisEntities.usp_ChartRouting_Select(rty)
                           select new RoutingClass
                           {
                               ChartRoutingKey = rt.CHART_ROUTING_KEY,
                               RoutingText = rt.ROUTING_TEXT
                           }).ToList();

            return (IOrderedQueryable<RoutingClass>)tmpList.AsQueryable();
        }

        public IOrderedQueryable<ChartTypeclass> PopulateChartTypeDropDownData(string strType, string strInorout)
        {
            var tmpList = new List<ChartTypeclass>();
            string strReportInorout = "";
            if (strType == "1")
            {
                tmpList = (from ch in objthemisEntities.CHART_TYPE
                           orderby ch.CHART_TYPE_KEY
                           select new ChartTypeclass
                           {
                               ChartTypeKey = ch.CHART_TYPE_KEY,
                               ChartTypeText = ch.CHART_TYPE_TEXT,
                               ChartTypeDesc = ch.CHART_TYPE_DESCR
                           }).ToList();
            }
            if (strType == "2")
            {
                if (strInorout == "In")
                {
                    strReportInorout = "Inpatient";
                }
                if (strInorout == "Out")
                {
                    strReportInorout = "";
                }
                using (var command = objthemisEntities.Database.Connection.CreateCommand())
                {
                    tmpList = (objthemisEntities.CHART_TYPE.Where(
                        t => t.CHART_TYPE_DESCR.Equals(strReportInorout)
                        || (strReportInorout.ToUpper().Equals("INPATIENT") && t.CHART_TYPE_KEY.Equals(1))
                        || (!strReportInorout.ToUpper().Equals("INPATIENT") && !t.CHART_TYPE_KEY.Equals(1))
                        ).Select(ch =>
                                new ChartTypeclass
                                {
                                    ChartTypeKey = ch.CHART_TYPE_KEY,
                                    ChartTypeText = ch.CHART_TYPE_TEXT,
                                    ChartTypeDesc = ch.CHART_TYPE_DESCR
                                }).OrderBy(r => r.ChartTypeKey).ToList());
                }

                // command.CommandText = "SELECT CHART_TYPE_TEXT, CHART_TYPE_KEY   FROM CHART_TYPE   WHERE  '" +strReportInorout+ "'= '' OR ('" + strReportInorout + "' = 'INPATIENT' AND CHART_TYPE_KEY = 1)   	OR ('" + strReportInorout + "' != 'INPATIENT' AND CHART_TYPE_KEY != 1)   ORDER BY CHART_TYPE_KEY ASC";
                //  objthemisEntities.Database.Connection.Open();
                // objthemisEntities.CHART_TYPE.Select(
                //     )
                // using (var result = command.ExecuteReader())
                //{
                // while (result.Read() && result.IsDBNull(0) == false)
                // {
                //  if (result.GetValue(0).ToString() != "")
                //  {
                //     tmpList.Add(new ChartTypeclass() { ChartTypeText = result.GetValue(0).ToString(), ChartTypeKey = int.Parse(result.GetValue(1).ToString()) });
                //  }
                //}
                //}
                //}

            }
            return (IOrderedQueryable<ChartTypeclass>)tmpList.AsQueryable();
        }

        public IOrderedQueryable<Assigneeclass> PopulateAssigneeDropDownData(int id,string strFacilityID)
        {
            Facilityclass obj1 = new Facilityclass();
            string strfacilitykey = "";
            if (!string.IsNullOrEmpty(strFacilityID) && (strFacilityID != "0"))
            {
                strfacilitykey = strFacilityID;
            }
            else
            {
                if (!string.IsNullOrEmpty(this.FacilityKey.ToString()))
                {
                    if (this.FacilityKey.Count != 0)
                    {
                        foreach (var j in this.FacilityKey)
                        {
                            if (strfacilitykey == "")
                            {
                                strfacilitykey = j.ToString();
                            }
                            else
                            {
                                strfacilitykey = strfacilitykey + "," + j;
                            }
                        }
                    }
                }
            }

            var tmpList = (from a in objthemisEntities.spAssignee(id, strfacilitykey, strfacilitykey)
                           select new Assigneeclass
                           {
                               AppUserKey = a.APP_USER_KEY,
                               FullName = a.FULL_NAME
                           }).ToList();

            return (IOrderedQueryable<Assigneeclass>)tmpList.AsQueryable();
        }
       

     public IOrderedQueryable<Coderclass> PopulateCoderDropDownData(int id, string strFacilityID)
        {
            Facilityclass obj1 = new Facilityclass();
            Coderclass objcoder = new Coderclass();
            string strfacilitykey = "";
            if (!string.IsNullOrEmpty(strFacilityID) && (strFacilityID != "0"))
            {
                strfacilitykey = strFacilityID;
            }
            else
            {
                if (!string.IsNullOrEmpty(this.FacilityKey.ToString()))
                {
                    if (this.FacilityKey.Count != 0)
                    {
                        foreach (var j in this.FacilityKey)
                        {
                            if (strfacilitykey == "")
                            {
                                strfacilitykey = j.ToString();
                            }
                            else
                            {
                                strfacilitykey = strfacilitykey + "," + j;
                            }
                        }
                    }
                }
            }

            //            DbRawSqlQuery<CustomerInfo> data = db.Database.SqlQuery<CustomerInfo>
            //                                   ("select customerid,companyname,contactname,country 
            //                                   from customers where country = @p0", "USA");
            //            objcoder.Personnal_ID = objthemisEntities.Database.fro(Type.,"SELECT DISTINCT cp.CRA_PERSONNEL_ID as ID, cp.LAST_NAME + ', ' + cp.FIRST_NAME AS DISPLAY_NAME FROM CRA_PERSONNEL cp JOIN CRA_FACILITY_PERSONNEL_XREF cfpx on cp.CRA_PERSONNEL_ID = cfpx.CRA_PERSONNEL_ID JOIN CRA_FACILITIES cf on cfpx.CRA_FACILITY_ID = cf.CRA_FACILITY_ID  " +
            //" JOIN itvf_fetch_visible_cra_facilities(" + id + ") fac ON cf.MEDICAL_FACILITY_ID = fac.MEDICAL_FACILITY_KEY " +
            //" WHERE cf.MEDICAL_FACILITY_ID in (select[id] from dbo.fnConvertStringToInt('" + strfacilitykey + "'))  ORDER BY cp.LAST_NAME + ', ' + cp.FIRST_NAME ASC").ToList();

            var tmpList = new List<Coderclass>();
            using (var command = objthemisEntities.Database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT DISTINCT cp.CRA_PERSONNEL_ID as ID, cp.LAST_NAME + ', ' + cp.FIRST_NAME AS DISPLAY_NAME FROM CRA_PERSONNEL cp JOIN CRA_FACILITY_PERSONNEL_XREF cfpx on cp.CRA_PERSONNEL_ID = cfpx.CRA_PERSONNEL_ID JOIN CRA_FACILITIES cf on cfpx.CRA_FACILITY_ID = cf.CRA_FACILITY_ID  " +
" JOIN itvf_fetch_visible_cra_facilities(" + id + ") fac ON cf.MEDICAL_FACILITY_ID = fac.MEDICAL_FACILITY_KEY " +
" WHERE cf.MEDICAL_FACILITY_ID in (select[id] from dbo.fnConvertStringToInt('" + strfacilitykey + "'))  ORDER BY cp.LAST_NAME + ', ' + cp.FIRST_NAME ASC";
              objthemisEntities.Database.Connection.Open();

              
                using (var result = command.ExecuteReader())
                {
                    while (result.Read() && result.IsDBNull(0)==false)
                    {
                        if (result.GetValue(0).ToString() != "")
                        {
                            tmpList.Add(new Coderclass() { Personnal_ID = int.Parse(result.GetValue(0).ToString()), DISPLAY_NAME = result.GetValue(1).ToString() });
                        }
                    }          
                }
            }
            



            return (IOrderedQueryable<Coderclass>)tmpList.AsQueryable();
        }


    }


    public class Facilityclass
    {

        public int FacilityKey { get; set; }
        public string FacilityName { get; set; }
        
    }


    public class RoutingClass
    {
        public int ChartRoutingKey { get; set; }
        public string RoutingText { get; set; }

    }


    public class ChartTypeclass
    {
        public int ChartTypeKey { get; set; }
        public string ChartTypeText { get; set; }
        public string ChartTypeDesc { get; set; }
    }



    public class Assigneeclass
    {
        public int AppUserKey { get; set; }
        public string FullName { get; set; }
  
    }

    public class Coderclass
    {
        public int Personnal_ID { get; set; }
        public string DISPLAY_NAME { get; set; }
    }


}