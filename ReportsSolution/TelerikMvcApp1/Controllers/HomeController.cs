﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Telerik.ReportViewer.Mvc;
using Telerik.Reporting;
using System.Web.ModelBinding;
using TelerikMvcApp1.Models;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ReportsLibrary;

namespace TelerikMvcApp1.Controllers
{
    public class HomeController : Controller
    {

        public int loginid = 0;
        ThemisEntities ObjThemisEf = new ThemisEntities();
           
       
        // GET: Home
        public ActionResult Index()
        {
          
            return View();
        }

        public ActionResult MVCReportView1(string ID)
        {
            Dictionary<int, string> objFacility = new Dictionary<int, string>();
            ViewBag.userid = ID;
            Config cs = new Config();
            
            
            bool sameview = false;
            if (TempData["LoginId"] != null && ID !=null)
            {
                TempData["InorOut"] = "";
                cs.strUserKey = ID;
                objFacility = cs.getDetails(int.Parse(ID));
                ViewData["userid"] = ID;
                ViewBag.ReportType = "1";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");

                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return RedirectToAction("Api", new { id = loginid });
            }

        }

        public ActionResult ReportViewerView1(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("ReadyForReporting");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult ReportViewerView2(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("InsertedvsCompleted");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult ReportViewerView3(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("InProgressByStatus");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult ReportViewerView4(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("OldProgressCharts");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult ReportViewerView5(string ID)
        {                       
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"]!= null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("StaleInprogressCharts");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult ReportViewerView6(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("WorkLoadByuser");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult WeeklyThroughtPut(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult InProgressByFacility(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                TempData["InorOut"] = "";
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "1";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View("");
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }


        public ActionResult InpatientFinancialImpact(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult Inpatient_OverAll_Accuracy(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }



        public ActionResult InpatientMasterSummary(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult InpatientUnderLyingCauseOfError(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult InpatientTrendingAccuracy(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                ViewBag.TrendingYesorNot = "Yes";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult InpatientCodingReviewedSheet(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] ="In";
                TempData.Keep("InorOut");                
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult IpCodingReviewedsheet(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult OverView(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "In";
                TempData["InorOut"] = "In";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }


        public ActionResult OPTrendingAccuracy(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                ViewBag.TrendingYesorNot = "Yes";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }


        public ActionResult Outpatient_OverAll_Accuracy(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                ViewBag.TrendingYesorNot = "Yes";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }


        public ActionResult OPFinancialImpact(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult OPMasterSummary(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult OPUnderLyingCauseOfError(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }
        public ActionResult OPCodingReviewedSheet(string ID)
        {
            ViewBag.userid = ID;
            bool sameview = false;
            if (TempData["LoginId"] != null)
            {
                ViewBag.InorOut = "Out";
                TempData["InorOut"] = "Out";
                TempData.Keep("InorOut");
                loginid = Convert.ToInt32(TempData["LoginId"]);
                TempData.Keep("LoginId");
                ViewBag.ReportType = "2";
                if (loginid.ToString() == ID)
                {
                    sameview = true;
                }
            }

            if (sameview)
                return View();
            else
            {
                return Redirect("~/home/Api/" + loginid);
            }

        }

        public ActionResult Api(string ID)

        {
            ViewBag.Reportsurl = ConfigurationManager.AppSettings["TelerikReportsUrl"].ToString();
            ViewBag.ReportsKey = ConfigurationManager.AppSettings["TelerikReportsKey"].ToString();
            if (!string.IsNullOrEmpty(ID))
            {
                TempData["LoginId"] = ID;
                TempData.Keep("LoginId");               
                ViewBag.userkey = ID;
             
            }
           
            return View();
        }

        public ActionResult PartialViewFacility(int ID)
        {
            Dictionary<int, string> objFacility = new Dictionary<int, string>();
            Config cs = new Config();
            cs.strUserKey =Convert.ToString(ID);
            objFacility = cs.getDetails(ID);
            return PartialView(cs);
        }

        public JsonResult Facility_Read(string ID,string strReportType,[DataSourceRequest]DataSourceRequest request)
        {
            List<Facilityclass> tmpFacilityList = new List<Facilityclass>();
            ID = TempData["LoginId"].ToString();
            TempData.Keep("LoginId");
            
                Config cs = new Config();
                Dictionary<int, string> objFacility = new Dictionary<int, string>();
                if (ID != null)
                {
                    cs.strUserKey = Convert.ToString(ID);
                    objFacility = cs.getDetails(int.Parse(ID));

                    if (strReportType == "1" || strReportType == "3" || strReportType == "4" || strReportType == "5" || strReportType == "6" || strReportType == "7")
                    {
                        tmpFacilityList = cs.PopulateFacilityDropDownData(int.Parse(ID), 1).ToList();
                    }
                    else if (strReportType == "2" || strReportType == "8")
                    { tmpFacilityList = cs.PopulateFacilityDropDownData(int.Parse(ID), 2).ToList(); }

                    if (tmpFacilityList.Count > 1)
                    {
                        Facilityclass selectAllItem = new Facilityclass();
                        selectAllItem.FacilityName = "- ALL -";
                        selectAllItem.FacilityKey = 0;

                        tmpFacilityList.Insert(0, selectAllItem);
                        ViewBag.FilterFacility = tmpFacilityList;
                    }


                }
            
            return Json(tmpFacilityList, JsonRequestBehavior.AllowGet);
            
        }
        public JsonResult Routing_Read(string strNeedAllOrNot,[DataSourceRequest]DataSourceRequest request)
        {
            List<RoutingClass> tmpRotuingList = new List<RoutingClass>();

            Config cs = new Config();
            tmpRotuingList = cs.PopulateRotuingDropDownData(null).ToList();
            if (strNeedAllOrNot == "0")
            {
                if (tmpRotuingList.Count > 1)
                {
                    RoutingClass selectAllItem = new RoutingClass();
                    selectAllItem.RoutingText = "- ALL -";
                    selectAllItem.ChartRoutingKey = 0;

                    tmpRotuingList.Insert(0, selectAllItem);
                    ViewBag.FilterRoutingType = tmpRotuingList;
                }
            }
            return Json(tmpRotuingList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ChartType_Read(string strType,[DataSourceRequest]DataSourceRequest request)
        {
            List<ChartTypeclass> tmpChartTypeList = new List<ChartTypeclass>();
            
            Config cs = new Config();
            tmpChartTypeList = cs.PopulateChartTypeDropDownData(strType, TempData["InorOut"].ToString()).ToList();
            if (strType == "1")
            {
                if (tmpChartTypeList.Count > 1)
                {
                    ChartTypeclass selectAllItem = new ChartTypeclass();
                    selectAllItem.ChartTypeText = "- ALL -";
                    selectAllItem.ChartTypeKey = 0;
                    tmpChartTypeList.Insert(0, selectAllItem);
                }
            }

            if(TempData["InorOut"].ToString()=="Out")
            {
                if (tmpChartTypeList.Count > 1)
                {
                    ChartTypeclass selectAllItem = new ChartTypeclass();
                    selectAllItem.ChartTypeText = "- ALL -";
                    selectAllItem.ChartTypeKey = 0;
                    tmpChartTypeList.Insert(0, selectAllItem);
                }
            }


            ViewBag.FilterRoutingType = tmpChartTypeList;
            return Json(tmpChartTypeList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Assignee_Read(string ID,string strFacilityId,[DataSourceRequest]DataSourceRequest request)
        {
            List<Assigneeclass> tmpAssigneeList = new List<Assigneeclass>();
            List<Facilityclass> tmpFacilityList = new List<Facilityclass>();
            ID = TempData["LoginId"].ToString();
            TempData.Keep("LoginId");
            Config cs = new Config();
            Dictionary<int, string> objFacility = new Dictionary<int, string>();
            if (ID != null)
            {

                cs.strUserKey = Convert.ToString(ID);
                objFacility = cs.getDetails(int.Parse(ID));
                tmpAssigneeList = cs.PopulateAssigneeDropDownData(int.Parse(ID), strFacilityId).ToList();
             
                    Assigneeclass selectAllItem = new Assigneeclass();
                    if (tmpAssigneeList.Count > 1)
                    {
                        selectAllItem.FullName = "- ALL -";
                        selectAllItem.AppUserKey = 0;
                        tmpAssigneeList.Insert(0, selectAllItem);
                        ViewBag.FilterRoutingType = tmpAssigneeList;
                    }

                
            }
            return Json(tmpAssigneeList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Coder_Read(string ID, string strFacilityId, [DataSourceRequest]DataSourceRequest request)
        {
            List<Coderclass> tmpCoderList = new List<Coderclass>();

            List<Facilityclass> tmpFacilityList = new List<Facilityclass>();
            ID = TempData["LoginId"].ToString();
            TempData.Keep("LoginId");
            Config cs = new Config();
            Dictionary<int, string> objFacility = new Dictionary<int, string>();
            if (ID != null)
            {
                cs.strUserKey = Convert.ToString(ID);
                objFacility = cs.getDetails(int.Parse(ID));
                if (!string.IsNullOrEmpty(strFacilityId))
                {
                    tmpCoderList = cs.PopulateCoderDropDownData(int.Parse(ID), strFacilityId).ToList();                   
                }
                else
                {
                    tmpCoderList = cs.PopulateCoderDropDownData(int.Parse(ID), strFacilityId).ToList();
                }
                Coderclass selectAllItem = new Coderclass();
                if (tmpCoderList.Count > 1)
                {
                    selectAllItem.DISPLAY_NAME = "- ALL -";
                    selectAllItem.Personnal_ID = 0;
                    tmpCoderList.Insert(0, selectAllItem);
                    ViewBag.FilterRoutingType = tmpCoderList;
                }
            }
            return Json(tmpCoderList, JsonRequestBehavior.AllowGet);
        }


    }



}