//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TelerikMvcApp1
{
    using System;
    using System.Collections.Generic;
    
    public partial class CRRS_REPORT_REF
    {
        public decimal report_seq_no { get; set; }
        public string report_name { get; set; }
        public string master_rpt_flag { get; set; }
        public string report_desc { get; set; }
        public string file_name { get; set; }
        public Nullable<decimal> sort_order { get; set; }
        public string report_rdl_object { get; set; }
        public string cra_report_flag { get; set; }
        public string appendix_flag { get; set; }
        public string appendix_desc { get; set; }
        public Nullable<decimal> master_report_seq_no { get; set; }
        public Nullable<bool> inpatient_report_flag { get; set; }
        public Nullable<int> ICD_TYPE_ID { get; set; }
    }
}
