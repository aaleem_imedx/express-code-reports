//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TelerikMvcApp1
{
    using System;
    using System.Collections.Generic;
    
    public partial class CRA_DISTRIBUTE_REPORT_ERROR
    {
        public int PATIENT_CHART_KEY { get; set; }
        public string ERR_MESSAGE { get; set; }
        public bool ACTIVE { get; set; }
        public int CREATE_USER { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public Nullable<int> UPDATE_USER { get; set; }
        public Nullable<System.DateTime> UPDATE_DATE { get; set; }
        public string FRIENDLY_MESSAGE { get; set; }
        public int REPORT_ICD_TYPE_ID { get; set; }
    
        public virtual APP_USER APP_USER { get; set; }
        public virtual APP_USER APP_USER1 { get; set; }
        public virtual PATIENT_CHART PATIENT_CHART { get; set; }
    }
}
