namespace ReportsLibrary.ThemisReports
{
    partial class WeeklyThroughput
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeeklyThroughput));
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.DateTimeScale dateTimeScale1 = new Telerik.Reporting.DateTimeScale();
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.NumericalScale numericalScale4 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.DateTimeScale dateTimeScale2 = new Telerik.Reporting.DateTimeScale();
            Telerik.Reporting.DateTimeScale dateTimeScale3 = new Telerik.Reporting.DateTimeScale();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition1 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.CategoryScale categoryScale3 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScale categoryScale4 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.DateTimeScale dateTimeScale4 = new Telerik.Reporting.DateTimeScale();
            Telerik.Reporting.NumericalScale numericalScale5 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.DateTimeScale dateTimeScale5 = new Telerik.Reporting.DateTimeScale();
            Telerik.Reporting.NumericalScale numericalScale6 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.FacilitySqlsource = new Telerik.Reporting.SqlDataSource();
            this.RoutingSqlSource = new Telerik.Reporting.SqlDataSource();
            this.ChartTypeSqlSource = new Telerik.Reporting.SqlDataSource();
            this.FacilityCoders = new Telerik.Reporting.SqlDataSource();
            this.Assigneesql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis15 = new Telerik.Reporting.GraphAxis();
            this.graphAxis16 = new Telerik.Reporting.GraphAxis();
            this.WeeklyThroughtPutSql = new Telerik.Reporting.SqlDataSource();
            this.lineSeries1 = new Telerik.Reporting.LineSeries();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            this.graphAxis9 = new Telerik.Reporting.GraphAxis();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.graphAxis6 = new Telerik.Reporting.GraphAxis();
            this.graphAxis7 = new Telerik.Reporting.GraphAxis();
            this.graphAxis8 = new Telerik.Reporting.GraphAxis();
            this.graphAxis10 = new Telerik.Reporting.GraphAxis();
            this.graphAxis11 = new Telerik.Reporting.GraphAxis();
            this.graphAxis12 = new Telerik.Reporting.GraphAxis();
            this.graphAxis13 = new Telerik.Reporting.GraphAxis();
            this.graphAxis14 = new Telerik.Reporting.GraphAxis();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // FacilitySqlsource
            // 
            this.FacilitySqlsource.ConnectionString = "THEMISConnectionString";
            this.FacilitySqlsource.Name = "FacilitySqlsource";
            this.FacilitySqlsource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySqlsource.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySqlsource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSqlSource
            // 
            this.RoutingSqlSource.ConnectionString = "THEMISConnectionString";
            this.RoutingSqlSource.Name = "RoutingSqlSource";
            this.RoutingSqlSource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, null)});
            this.RoutingSqlSource.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSqlSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypeSqlSource
            // 
            this.ChartTypeSqlSource.ConnectionString = "THEMISConnectionString";
            this.ChartTypeSqlSource.Name = "ChartTypeSqlSource";
            this.ChartTypeSqlSource.SelectCommand = "SELECT     CHART_TYPE_TEXT, CHART_TYPE_KEY, CHART_TYPE_DESCR\r\nFROM         CHART_" +
    "TYPE\r\nORDER BY CHART_TYPE_KEY";
            // 
            // FacilityCoders
            // 
            this.FacilityCoders.ConnectionString = "THEMISConnectionString";
            this.FacilityCoders.Name = "FacilityCoders";
            this.FacilityCoders.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "= Join(\',\',Parameters.FacilityName.Value)")});
            this.FacilityCoders.SelectCommand = resources.GetString("FacilityCoders.SelectCommand");
            // 
            // Assigneesql
            // 
            this.Assigneesql.ConnectionString = "THEMISConnectionString";
            this.Assigneesql.Name = "Assigneesql";
            this.Assigneesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "=  join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "= join(\',\', Parameters.FacilityName.Value)")});
            this.Assigneesql.SelectCommand = resources.GetString("Assigneesql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "          WeeklyThroughput";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(5.6000003814697266D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.graph1});
            this.detail.Name = "detail";
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.REVIEWED_DATE"));
            graphGroup1.Name = "rEVIEWED_DATEGroup";
            graphGroup1.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph1.DataSource = this.WeeklyThroughtPutSql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.lineSeries1);
            this.graph1.SeriesGroups.Add(graphGroup2);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.IsInsidePlotArea = false;
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "graph1";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.cartesianCoordinateSystem1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.cartesianCoordinateSystem1.Style.Color = System.Drawing.Color.Black;
            this.cartesianCoordinateSystem1.Style.LineColor = System.Drawing.Color.Transparent;
            this.cartesianCoordinateSystem1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.cartesianCoordinateSystem1.Style.Visible = true;
            this.cartesianCoordinateSystem1.XAxis = this.graphAxis15;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis16;
            // 
            // graphAxis15
            // 
            this.graphAxis15.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Gold;
            this.graphAxis15.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis15.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis15.MajorGridLineStyle.Visible = false;
            this.graphAxis15.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis15.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis15.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis15.MinorGridLineStyle.Visible = false;
            this.graphAxis15.Name = "graphAxis15";
            this.graphAxis15.Scale = numericalScale1;
            this.graphAxis15.TitleStyle.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // graphAxis16
            // 
            this.graphAxis16.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Indigo;
            this.graphAxis16.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis16.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis16.MajorGridLineStyle.Visible = true;
            this.graphAxis16.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis16.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis16.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis16.MinorGridLineStyle.Visible = false;
            this.graphAxis16.Name = "graphAxis16";
            this.graphAxis16.Scale = numericalScale2;
            this.graphAxis16.Style.Visible = true;
            this.graphAxis16.TitleStyle.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dotted;
            this.graphAxis16.TitleStyle.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // WeeklyThroughtPutSql
            // 
            this.WeeklyThroughtPutSql.ConnectionString = "THEMISConnectionString";
            this.WeeklyThroughtPutSql.Name = "WeeklyThroughtPutSql";
            this.WeeklyThroughtPutSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "=Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteiclRouting", System.Data.DbType.String, "=Join(\',\', Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlRouting", System.Data.DbType.String, "=Join(\',\', Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlChartType", System.Data.DbType.String, "=join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlChartType", System.Data.DbType.String, "=join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlAssignee", System.Data.DbType.String, "=join(\',\', Parameters.Assignee.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlAssignee", System.Data.DbType.String, "=join(\',\', Parameters.Assignee.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacCoders", System.Data.DbType.String, "= join(\',\',Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacCoders", System.Data.DbType.String, "= join(\',\',Parameters.Coder.Value)")});
            this.WeeklyThroughtPutSql.SelectCommand = resources.GetString("WeeklyThroughtPutSql.SelectCommand");
            // 
            // lineSeries1
            // 
            this.lineSeries1.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.lineSeries1.CategoryGroup = graphGroup1;
            this.lineSeries1.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.lineSeries1.DataPointLabel = "=\'(\' +  Sum(Fields.REVIEWED_DATE.Hour) + \'; \' +  Sum(Fields.ELAPSED_CODING_MINUTE" +
    "S) + \')\'";
            this.lineSeries1.DataPointLabelStyle.Visible = false;
            this.lineSeries1.DataPointStyle.Visible = true;
            this.lineSeries1.LegendItem.MarkStyle.BackgroundColor = System.Drawing.Color.Maroon;
            this.lineSeries1.LegendItem.Style.Visible = false;
            this.lineSeries1.LegendItem.Value = "= Fields.ELAPSED_CODING_MINUTES";
            this.lineSeries1.LineStyle.BackgroundColor = System.Drawing.Color.Red;
            this.lineSeries1.LineStyle.Font.Bold = true;
            this.lineSeries1.LineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.lineSeries1.LineStyle.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.lineSeries1.LineStyle.Visible = false;
            this.lineSeries1.LineType = Telerik.Reporting.LineSeries.LineTypes.Smooth;
            this.lineSeries1.MarkerMaxSize = Telerik.Reporting.Drawing.Unit.Pixel(50D);
            this.lineSeries1.MarkerMinSize = Telerik.Reporting.Drawing.Unit.Pixel(5D);
            this.lineSeries1.MarkerSize = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.lineSeries1.MarkerType = Telerik.Reporting.DataPointMarkerType.Triangle;
            this.lineSeries1.Name = "lineSeries1";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ELAPSED_CODING_MINUTES"));
            graphGroup2.Name = "eLAPSED_CODING_MINUTESGroup";
            graphGroup2.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.ELAPSED_CODING_MINUTES", Telerik.Reporting.SortDirection.Asc));
            this.lineSeries1.SeriesGroup = graphGroup2;
            this.lineSeries1.Size = null;
            this.lineSeries1.ToolTip.Text = "=\'(\' +  Sum(Fields.REVIEWED_DATE.Hour) + \'; \' +  Sum(Fields.ELAPSED_CODING_MINUTE" +
    "S) + \')\'";
            this.lineSeries1.ToolTip.Title = "= Fields.ELAPSED_CODING_MINUTES";
            this.lineSeries1.X = "= Sum(Fields.REVIEWED_DATE.Hour)";
            this.lineSeries1.Y = "= Sum(Fields.ELAPSED_CODING_MINUTES)";
            // 
            // graphAxis2
            // 
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            dateTimeScale1.LabelUnit = Telerik.Reporting.DateTimeScaleUnits.Minutes;
            this.graphAxis2.Scale = dateTimeScale1;
            // 
            // graphAxis9
            // 
            this.graphAxis9.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MinorGridLineStyle.Visible = false;
            this.graphAxis9.Name = "graphAxis9";
            this.graphAxis9.Scale = categoryScale1;
            // 
            // graphAxis5
            // 
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.Name = "GraphAxis3";
            this.graphAxis5.Scale = numericalScale3;
            // 
            // graphAxis1
            // 
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "GraphAxis1";
            this.graphAxis1.Scale = numericalScale4;
            // 
            // graphAxis3
            // 
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "GraphAxis3";
            this.graphAxis3.Scale = dateTimeScale2;
            // 
            // graphAxis4
            // 
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "GraphAxis1";
            this.graphAxis4.Scale = dateTimeScale3;
            // 
            // graphAxis6
            // 
            this.graphAxis6.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.Visible = false;
            this.graphAxis6.Name = "GraphAxis1";
            categoryScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.Auto;
            categoryScale2.CrossAxisPositions.Add(categoryScaleCrossAxisPosition1);
            this.graphAxis6.Scale = categoryScale2;
            // 
            // graphAxis7
            // 
            this.graphAxis7.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis7.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MinorGridLineStyle.Visible = false;
            this.graphAxis7.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis7.Name = "GraphAxis3";
            // 
            // graphAxis8
            // 
            this.graphAxis8.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.Visible = false;
            this.graphAxis8.Name = "GraphAxis1";
            this.graphAxis8.Scale = categoryScale3;
            // 
            // graphAxis10
            // 
            this.graphAxis10.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MinorGridLineStyle.Visible = false;
            this.graphAxis10.Name = "GraphAxis1";
            this.graphAxis10.Scale = categoryScale4;
            // 
            // graphAxis11
            // 
            this.graphAxis11.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MinorGridLineStyle.Visible = false;
            this.graphAxis11.Name = "GraphAxis1";
            this.graphAxis11.Scale = dateTimeScale4;
            // 
            // graphAxis12
            // 
            this.graphAxis12.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis12.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis12.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis12.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis12.MinorGridLineStyle.Visible = false;
            this.graphAxis12.Name = "GraphAxis2";
            this.graphAxis12.Scale = numericalScale5;
            // 
            // graphAxis13
            // 
            this.graphAxis13.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MinorGridLineStyle.Visible = false;
            this.graphAxis13.Name = "GraphAxis3";
            this.graphAxis13.Scale = dateTimeScale5;
            // 
            // graphAxis14
            // 
            this.graphAxis14.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MinorGridLineStyle.Visible = false;
            this.graphAxis14.Name = "GraphAxis3";
            this.graphAxis14.Scale = numericalScale6;
            // 
            // WeeklyThroughput
            // 
            this.DataSource = this.WeeklyThroughtPutSql;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail});
            this.Name = "WeeklyThroughput";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySqlsource;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSqlSource;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.MultiValue = true;
            reportParameter2.Name = "RoutingType";
            reportParameter2.Value = "= AllDistinctValues(Fields.CHART_ROUTING_KEY)";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypeSqlSource;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "1";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.FacilityCoders;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "= AllDistinctValues(Fields.CRA_PERSONNEL_ID)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.AvailableValues.DataSource = this.Assigneesql;
            reportParameter5.AvailableValues.DisplayMember = "= Fields.FULL_NAME";
            reportParameter5.AvailableValues.ValueMember = "= Fields.APP_USER_KEY";
            reportParameter5.MultiValue = true;
            reportParameter5.Name = "Assignee";
            reportParameter5.Value = "= AllDistinctValues(Fields.APP_USER_KEY)";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "UserId";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter6.Value = "996";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "ChartTypeName";
            reportParameter7.Value = "Inpatient";
            reportParameter7.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(11D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.SqlDataSource WeeklyThroughtPutSql;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.SqlDataSource FacilitySqlsource;
        private Telerik.Reporting.SqlDataSource RoutingSqlSource;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.SqlDataSource ChartTypeSqlSource;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.GraphAxis graphAxis6;
        private Telerik.Reporting.GraphAxis graphAxis7;
        private Telerik.Reporting.GraphAxis graphAxis8;
        private Telerik.Reporting.SqlDataSource Assigneesql;
        private Telerik.Reporting.SqlDataSource FacilityCoders;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.GraphAxis graphAxis9;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.GraphAxis graphAxis10;
        private Telerik.Reporting.GraphAxis graphAxis11;
        private Telerik.Reporting.GraphAxis graphAxis12;
        private Telerik.Reporting.GraphAxis graphAxis13;
        private Telerik.Reporting.GraphAxis graphAxis14;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis15;
        private Telerik.Reporting.GraphAxis graphAxis16;
        private Telerik.Reporting.LineSeries lineSeries1;
    }
}