namespace ReportsLibrary.ThemisReports
{
    partial class OPTrendingAccuracy
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction3 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction4 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction5 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction6 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction7 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction8 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction9 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction10 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction11 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction12 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction13 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction14 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction15 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction16 = new Telerik.Reporting.SortingAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OPTrendingAccuracy));
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup12 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup13 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup14 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup15 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup16 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette1 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup7 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup8 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette2 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup9 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette3 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup10 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette4 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup11 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette5 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.OPTrendinAccuracy = new Telerik.Reporting.SqlDataSource();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.BApc = new Telerik.Reporting.BarSeries();
            this.Bcm = new Telerik.Reporting.BarSeries();
            this.Bpcs = new Telerik.Reporting.BarSeries();
            this.Bcpt = new Telerik.Reporting.BarSeries();
            this.Bmod = new Telerik.Reporting.BarSeries();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.54999840259552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "Date Range";
            // 
            // textBox2
            // 
            sortingAction1.SortingExpression = "= Fields.NumOfCharts";
            sortingAction1.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox2.Action = sortingAction1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93541806936264038D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "# of Charts";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5499976873397827D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.935418963432312D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.91458070278167725D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73750275373458862D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0312467813491821D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.90416747331619263D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.89375394582748413D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.97916585206985474D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.635416567325592D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.83333277702331543D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000014305114746D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0083358287811279D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.81041812896728516D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000014305114746D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73750388622283936D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.84166848659515381D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000014305114746D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table1.Body.SetCellContent(0, 2, this.textBox15);
            this.table1.Body.SetCellContent(0, 3, this.textBox16);
            this.table1.Body.SetCellContent(0, 5, this.textBox17);
            this.table1.Body.SetCellContent(0, 6, this.textBox18);
            this.table1.Body.SetCellContent(0, 11, this.textBox19);
            this.table1.Body.SetCellContent(0, 12, this.textBox20);
            this.table1.Body.SetCellContent(0, 14, this.textBox21);
            this.table1.Body.SetCellContent(0, 15, this.textBox22);
            this.table1.Body.SetCellContent(0, 0, this.textBox13);
            this.table1.Body.SetCellContent(0, 1, this.textBox14);
            this.table1.Body.SetCellContent(0, 4, this.textBox26);
            this.table1.Body.SetCellContent(0, 7, this.textBox28);
            this.table1.Body.SetCellContent(0, 8, this.textBox33);
            this.table1.Body.SetCellContent(0, 9, this.textBox31);
            this.table1.Body.SetCellContent(0, 10, this.textBox12);
            this.table1.Body.SetCellContent(0, 13, this.textBox24);
            this.table1.Body.SetCellContent(0, 16, this.textBox34);
            tableGroup2.Name = "group";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.Name = "startDate";
            tableGroup1.ReportItem = this.textBox1;
            tableGroup4.Name = "endDate";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.Name = "group1";
            tableGroup3.ReportItem = this.textBox2;
            tableGroup5.Name = "tOTAL_APC_ASSIGNED";
            tableGroup5.ReportItem = this.textBox3;
            tableGroup6.Name = "tOTAL_APC_CHANGED";
            tableGroup6.ReportItem = this.textBox4;
            tableGroup7.Name = "group2";
            tableGroup7.ReportItem = this.textBox25;
            tableGroup8.Name = "tOTAL_CM_ASSIGNED";
            tableGroup8.ReportItem = this.textBox5;
            tableGroup9.Name = "tOTAL_CM_CHANGED";
            tableGroup9.ReportItem = this.textBox6;
            tableGroup10.Name = "group3";
            tableGroup10.ReportItem = this.textBox27;
            tableGroup11.Name = "group4";
            tableGroup11.ReportItem = this.textBox29;
            tableGroup12.Name = "group5";
            tableGroup12.ReportItem = this.textBox30;
            tableGroup13.Name = "group6";
            tableGroup13.ReportItem = this.textBox11;
            tableGroup14.Name = "tOTAL_CPT_ASSIGNED";
            tableGroup14.ReportItem = this.textBox7;
            tableGroup15.Name = "tOTAL_CPT_CHANGED";
            tableGroup15.ReportItem = this.textBox8;
            tableGroup16.Name = "group7";
            tableGroup16.ReportItem = this.textBox23;
            tableGroup17.Name = "tOTAL_MOD_ASSIGNED";
            tableGroup17.ReportItem = this.textBox9;
            tableGroup18.Name = "tOTAL_MOD_CHANGED";
            tableGroup18.ReportItem = this.textBox10;
            tableGroup19.Name = "group8";
            tableGroup19.ReportItem = this.textBox32;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.ColumnGroups.Add(tableGroup12);
            this.table1.ColumnGroups.Add(tableGroup13);
            this.table1.ColumnGroups.Add(tableGroup14);
            this.table1.ColumnGroups.Add(tableGroup15);
            this.table1.ColumnGroups.Add(tableGroup16);
            this.table1.ColumnGroups.Add(tableGroup17);
            this.table1.ColumnGroups.Add(tableGroup18);
            this.table1.ColumnGroups.Add(tableGroup19);
            this.table1.DataSource = this.OPTrendinAccuracy;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox26,
            this.textBox17,
            this.textBox18,
            this.textBox28,
            this.textBox33,
            this.textBox31,
            this.textBox12,
            this.textBox19,
            this.textBox20,
            this.textBox24,
            this.textBox21,
            this.textBox22,
            this.textBox34,
            this.textBox1,
            this.textBox3,
            this.textBox4,
            this.textBox25,
            this.textBox5,
            this.textBox6,
            this.textBox27,
            this.textBox29,
            this.textBox30,
            this.textBox11,
            this.textBox7,
            this.textBox8,
            this.textBox23,
            this.textBox9,
            this.textBox10,
            this.textBox32,
            this.textBox2});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.18958847224712372D), Telerik.Reporting.Drawing.Unit.Inch(4.0000004768371582D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            this.table1.NoDataStyle.Font.Bold = true;
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.Name = "detail";
            this.table1.RowGroups.Add(tableGroup20);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(15.812514305114746D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table1.StyleName = "Apex.TableNormal";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91458195447921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox15.StyleName = "Apex.TableBody";
            this.textBox15.Value = "= Fields.TOTAL_APC_ASSIGNED";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7375023365020752D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox16.StyleName = "Apex.TableBody";
            this.textBox16.Value = "= Fields.TOTAL_APC_CHANGED";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90416795015335083D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox17.StyleName = "Apex.TableBody";
            this.textBox17.Value = "= Fields.TOTAL_CM_ASSIGNED";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89375394582748413D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox18.StyleName = "Apex.TableBody";
            this.textBox18.Value = "= Fields.TOTAL_CM_CHANGED";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0083353519439697D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox19.StyleName = "Apex.TableBody";
            this.textBox19.Value = "= Fields.TOTAL_CPT_ASSIGNED";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8104177713394165D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox20.StyleName = "Apex.TableBody";
            this.textBox20.Value = "= Fields.TOTAL_CPT_CHANGED";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73750400543212891D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox21.StyleName = "Apex.TableBody";
            this.textBox21.Value = "= Fields.TOTAL_MOD_ASSIGNED";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84166747331619263D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox22.StyleName = "Apex.TableBody";
            this.textBox22.Value = "= Fields.TOTAL_MOD_CHANGED";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.54999840259552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox13.StyleName = "Apex.TableBody";
            this.textBox13.Value = "=Format(\"{0}-{1}\",Fields.StartDate, Fields.EndDate)";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93541806936264038D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox14.StyleName = "Apex.TableBody";
            this.textBox14.Value = "= Fields.NumOfCharts";
            // 
            // textBox26
            // 
            this.textBox26.Format = "{0:P2}";
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0312469005584717D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox26.StyleName = "Apex.TableBody";
            this.textBox26.Value = "=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 /Fi" +
    "elds.TOTAL_APC_ASSIGNED))))/100,\'\')";
            // 
            // textBox28
            // 
            this.textBox28.Format = "{0:P2}";
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97916597127914429D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox28.StyleName = "Apex.TableBody";
            this.textBox28.Value = "=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
    "lds.TOTAL_CM_ASSIGNED))))/100,\'\')";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63541579246521D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox33.StyleName = "Apex.TableBody";
            this.textBox33.Value = "= Fields.TOTAL_PCS_ASSIGNED";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8333321213722229D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox31.StyleName = "Apex.TableBody";
            this.textBox31.Value = "= Fields.TOTAL_PCS_CHANGED";
            // 
            // textBox12
            // 
            this.textBox12.Format = "{0:P2}";
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox12.StyleName = "Apex.TableBody";
            this.textBox12.Value = "=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
    "ields.TOTAL_PCS_ASSIGNED))))/100,\'\')";
            // 
            // textBox24
            // 
            this.textBox24.Format = "{0:P2}";
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox24.StyleName = "Apex.TableBody";
            this.textBox24.Value = "=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((round(Fields.TOTAL_CPT_CHANGED* 100 / Fi" +
    "elds.TOTAL_CPT_ASSIGNED))))/100,\'\')";
            // 
            // textBox34
            // 
            this.textBox34.Format = "{0:P2}";
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox34.StyleName = "Apex.TableBody";
            this.textBox34.Value = "=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
    "ields.TOTAL_MOD_ASSIGNED))))/100,\'\')";
            // 
            // textBox3
            // 
            sortingAction2.SortingExpression = "= Fields.TOTAL_APC_ASSIGNED";
            sortingAction2.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox3.Action = sortingAction2;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91458195447921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "Count";
            // 
            // textBox4
            // 
            sortingAction3.SortingExpression = "= Fields.TOTAL_APC_CHANGED";
            sortingAction3.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox4.Action = sortingAction3;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7375023365020752D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox4.StyleName = "Apex.TableHeader";
            this.textBox4.Value = "Changes";
            // 
            // textBox25
            // 
            sortingAction4.SortingExpression = "=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 /Fi" +
    "elds.TOTAL_APC_ASSIGNED))))/100,\'\')";
            sortingAction4.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox25.Action = sortingAction4;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0312469005584717D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox25.StyleName = "Apex.TableHeader";
            this.textBox25.Value = "Accuracy";
            // 
            // textBox5
            // 
            sortingAction5.SortingExpression = "= Fields.TOTAL_CM_ASSIGNED";
            sortingAction5.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox5.Action = sortingAction5;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90416795015335083D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.StyleName = "Apex.TableHeader";
            this.textBox5.Value = "Count";
            // 
            // textBox6
            // 
            sortingAction6.SortingExpression = "= Fields.TOTAL_CM_CHANGED";
            sortingAction6.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox6.Action = sortingAction6;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89375394582748413D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox6.StyleName = "Apex.TableHeader";
            this.textBox6.Value = "Changes";
            // 
            // textBox27
            // 
            sortingAction7.SortingExpression = "=(100-((Round(Fields.TOTAL_CM_CHANGED * 100.0 / Fields.TOTAL_CM_ASSIGNED))))/100";
            sortingAction7.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox27.Action = sortingAction7;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97916597127914429D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox27.StyleName = "Apex.TableHeader";
            this.textBox27.Value = "Accuracy";
            // 
            // textBox29
            // 
            sortingAction8.SortingExpression = "= Fields.TOTAL_PCS_ASSIGNED";
            sortingAction8.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox29.Action = sortingAction8;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63541579246521D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox29.StyleName = "Apex.TableHeader";
            this.textBox29.Value = "Count";
            // 
            // textBox30
            // 
            sortingAction9.SortingExpression = "= Fields.TOTAL_PCS_CHANGED";
            sortingAction9.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox30.Action = sortingAction9;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8333321213722229D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox30.StyleName = "Apex.TableHeader";
            this.textBox30.Value = "Changes";
            // 
            // textBox11
            // 
            sortingAction10.SortingExpression = "=(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / Fields.TOTAL_PCS_ASSIGNED))))/100";
            sortingAction10.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox11.Action = sortingAction10;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox11.StyleName = "Apex.TableHeader";
            this.textBox11.Value = "Accuracy";
            // 
            // textBox7
            // 
            sortingAction11.SortingExpression = "= Fields.TOTAL_CPT_ASSIGNED";
            sortingAction11.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox7.Action = sortingAction11;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0083353519439697D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox7.StyleName = "Apex.TableHeader";
            this.textBox7.Value = "Count";
            // 
            // textBox8
            // 
            sortingAction12.SortingExpression = "= Fields.TOTAL_CPT_CHANGED";
            sortingAction12.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox8.Action = sortingAction12;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8104177713394165D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox8.StyleName = "Apex.TableHeader";
            this.textBox8.Value = "Changes";
            // 
            // textBox23
            // 
            sortingAction13.SortingExpression = "=(100-((round(Fields.TOTAL_CPT_CHANGED* 100 / Fields.TOTAL_CPT_ASSIGNED))))/100";
            sortingAction13.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox23.Action = sortingAction13;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox23.StyleName = "Apex.TableHeader";
            this.textBox23.Value = "Accuracy";
            // 
            // textBox9
            // 
            sortingAction14.SortingExpression = "= Fields.TOTAL_MOD_ASSIGNED";
            sortingAction14.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox9.Action = sortingAction14;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73750400543212891D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox9.StyleName = "Apex.TableHeader";
            this.textBox9.Value = "Count";
            // 
            // textBox10
            // 
            sortingAction15.SortingExpression = "= Fields.TOTAL_MOD_CHANGED";
            sortingAction15.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox10.Action = sortingAction15;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84166747331619263D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox10.StyleName = "Apex.TableHeader";
            this.textBox10.Value = "Change";
            // 
            // textBox32
            // 
            sortingAction16.SortingExpression = "=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
    "ields.TOTAL_MOD_ASSIGNED))))/100,\'\')";
            sortingAction16.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox32.Action = sortingAction16;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox32.StyleName = "Apex.TableHeader";
            this.textBox32.Value = "Accuracy";
            // 
            // OPTrendinAccuracy
            // 
            this.OPTrendinAccuracy.ConnectionString = "THEMISConnectionString";
            this.OPTrendinAccuracy.Name = "OPTrendinAccuracy";
            this.OPTrendinAccuracy.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "=join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@BeginDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupMode", System.Data.DbType.Byte, "= Parameters.GroupMode.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupByNumberOfCharts", System.Data.DbType.Int32, "= Parameters.NumberofCharts.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupByTimeFrame", System.Data.DbType.AnsiString, "= Parameters.TimeFrame.Value")});
            this.OPTrendinAccuracy.SelectCommand = "dbo.usp_Metis_OP_Trending_Select";
            this.OPTrendinAccuracy.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox35});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox35
            // 
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4000003337860107D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Name = "Segoe UI";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox35.Style.Font.Underline = true;
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "OutPatient Trending Accuracy";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(4.5D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.graph1});
            this.detail.Name = "detail";
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / F" +
            "ields.TOTAL_APC_ASSIGNED))))/100,0)"));
            graphGroup1.Name = "apc";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
            "lds.TOTAL_CM_ASSIGNED))))/100,0)"));
            graphGroup2.Name = "cm";
            graphGroup3.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
            "ields.TOTAL_PCS_ASSIGNED))))/100,0)"));
            graphGroup3.Name = "pcs";
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / F" +
            "ields.TOTAL_CPT_ASSIGNED))))/100,0)"));
            graphGroup4.Name = "cpt";
            graphGroup5.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
            "ields.TOTAL_MOD_ASSIGNED))))/100,0)"));
            graphGroup5.Name = "mod";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CategoryGroups.Add(graphGroup2);
            this.graph1.CategoryGroups.Add(graphGroup3);
            this.graph1.CategoryGroups.Add(graphGroup4);
            this.graph1.CategoryGroups.Add(graphGroup5);
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph1.DataSource = this.OPTrendinAccuracy;
            this.graph1.Legend.Position = Telerik.Reporting.GraphItemPosition.BottomCenter;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Legend.Style.Visible = false;
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.39999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.BApc);
            this.graph1.Series.Add(this.Bcm);
            this.graph1.Series.Add(this.Bpcs);
            this.graph1.Series.Add(this.Bcpt);
            this.graph1.Series.Add(this.Bmod);
            graphGroup12.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / F" +
            "ields.TOTAL_APC_ASSIGNED))))/100,0)"));
            graphGroup12.Label = "dfgdfg";
            graphGroup12.Name = "apc";
            graphGroup13.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
            "lds.TOTAL_CM_ASSIGNED))))/100,0)"));
            graphGroup13.Name = "cm";
            graphGroup14.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
            "ields.TOTAL_PCS_ASSIGNED))))/100,0)"));
            graphGroup14.Name = "pcs";
            graphGroup15.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / F" +
            "ields.TOTAL_CPT_ASSIGNED))))/100,0)"));
            graphGroup15.Name = "cpt";
            graphGroup16.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
            "ields.TOTAL_MOD_ASSIGNED))))/100,0)"));
            graphGroup16.Name = "mod";
            this.graph1.SeriesGroups.Add(graphGroup12);
            this.graph1.SeriesGroups.Add(graphGroup13);
            this.graph1.SeriesGroups.Add(graphGroup14);
            this.graph1.SeriesGroups.Add(graphGroup15);
            this.graph1.SeriesGroups.Add(graphGroup16);
            this.graph1.SeriesGroups.Add(graphGroup7);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(10.90000057220459D), Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.XAxis = this.graphAxis2;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis1;
            // 
            // graphAxis2
            // 
            this.graphAxis2.LabelFormat = "{0:P0}";
            this.graphAxis2.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MajorGridLineStyle.Visible = false;
            this.graphAxis2.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            categoryScale1.SpacingSlotCount = -18D;
            this.graphAxis2.Scale = categoryScale1;
            this.graphAxis2.Title = "Periods";
            // 
            // graphAxis1
            // 
            this.graphAxis1.LabelFormat = "{0:P0}";
            this.graphAxis1.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.AtMinimum;
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "graphAxis1";
            this.graphAxis1.Scale = numericalScale1;
            this.graphAxis1.Title = "Percentage Accuracy";
            // 
            // BApc
            // 
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / F" +
            "ields.TOTAL_APC_ASSIGNED))))/100,0)"));
            graphGroup6.Name = "apc";
            this.BApc.CategoryGroup = graphGroup6;
            colorPalette1.Colors.Add(System.Drawing.Color.SlateBlue);
            this.BApc.ColorPalette = colorPalette1;
            this.BApc.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.BApc.DataPointLabel = "=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / F" +
    "ields.TOTAL_APC_ASSIGNED))))/100,0)";
            this.BApc.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BApc.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BApc.DataPointLabelFormat = "{0:P0}";
            this.BApc.DataPointLabelStyle.Visible = false;
            this.BApc.Name = "BApc";
            graphGroup7.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.rowNum"));
            graphGroup7.Name = "graphGroup";
            this.BApc.SeriesGroup = graphGroup7;
            this.BApc.ToolTip.Text = "=(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / Fields.TOTAL_APC_ASSIGNED))))";
            this.BApc.ToolTip.Title = "APC% Accuracy";
            this.BApc.Y = "=iif(Fields.TOTAL_APC_ASSIGNED<>0,(100-((Round(Fields.TOTAL_APC_CHANGED * 100 / F" +
    "ields.TOTAL_APC_ASSIGNED))))/100,0)";
            // 
            // Bcm
            // 
            graphGroup8.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
            "lds.TOTAL_CM_ASSIGNED))))/100,0)"));
            graphGroup8.Name = "cm";
            this.Bcm.CategoryGroup = graphGroup8;
            colorPalette2.Colors.Add(System.Drawing.Color.LightGreen);
            this.Bcm.ColorPalette = colorPalette2;
            this.Bcm.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.Bcm.DataPointLabel = "=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
    "lds.TOTAL_CM_ASSIGNED))))/100,0)";
            this.Bcm.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bcm.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bcm.DataPointLabelFormat = "{0:P0}";
            this.Bcm.DataPointLabelStyle.Visible = false;
            this.Bcm.Name = "Bcm";
            this.Bcm.SeriesGroup = graphGroup7;
            this.Bcm.ToolTip.Text = "=(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fields.TOTAL_CM_ASSIGNED))))";
            this.Bcm.ToolTip.Title = "CM% Accuracy";
            this.Bcm.Y = "=iif(Fields.TOTAL_CM_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CM_CHANGED * 100 / Fie" +
    "lds.TOTAL_CM_ASSIGNED))))/100,0)";
            // 
            // Bpcs
            // 
            graphGroup9.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
            "ields.TOTAL_PCS_ASSIGNED))))/100,0)"));
            graphGroup9.Name = "pcs";
            this.Bpcs.CategoryGroup = graphGroup9;
            colorPalette3.Colors.Add(System.Drawing.Color.Orange);
            this.Bpcs.ColorPalette = colorPalette3;
            this.Bpcs.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.Bpcs.DataPointLabel = "=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
    "ields.TOTAL_PCS_ASSIGNED))))/100,0)";
            this.Bpcs.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bpcs.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bpcs.DataPointLabelFormat = "{0:P0}";
            this.Bpcs.DataPointLabelStyle.Visible = false;
            this.Bpcs.Name = "Bpcs";
            this.Bpcs.SeriesGroup = graphGroup7;
            this.Bpcs.ToolTip.Text = "=(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / Fields.TOTAL_PCS_ASSIGNED))))";
            this.Bpcs.ToolTip.Title = "PCS% Accuracy";
            this.Bpcs.Y = "=iif(Fields.TOTAL_PCS_ASSIGNED<>0,(100-((Round(Fields.TOTAL_PCS_CHANGED * 100 / F" +
    "ields.TOTAL_PCS_ASSIGNED))))/100,0)";
            // 
            // Bcpt
            // 
            graphGroup10.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / F" +
            "ields.TOTAL_CPT_ASSIGNED))))/100,0)"));
            graphGroup10.Name = "cpt";
            this.Bcpt.CategoryGroup = graphGroup10;
            colorPalette4.Colors.Add(System.Drawing.Color.DeepSkyBlue);
            this.Bcpt.ColorPalette = colorPalette4;
            this.Bcpt.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.Bcpt.DataPointLabel = "=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / F" +
    "ields.TOTAL_CPT_ASSIGNED))))/100,0)";
            this.Bcpt.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bcpt.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bcpt.DataPointLabelFormat = "{0:P0}";
            this.Bcpt.DataPointLabelStyle.Visible = false;
            this.Bcpt.Name = "Bcpt";
            this.Bcpt.SeriesGroup = graphGroup7;
            this.Bcpt.ToolTip.Text = "=(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / Fields.TOTAL_CPT_ASSIGNED))))";
            this.Bcpt.ToolTip.Title = "CPT% Accuracy";
            this.Bcpt.Y = "=iif(Fields.TOTAL_CPT_ASSIGNED<>0,(100-((Round(Fields.TOTAL_CPT_CHANGED * 100 / F" +
    "ields.TOTAL_CPT_ASSIGNED))))/100,0)";
            // 
            // Bmod
            // 
            graphGroup11.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
            "ields.TOTAL_MOD_ASSIGNED))))/100,0)"));
            graphGroup11.Name = "mod";
            this.Bmod.CategoryGroup = graphGroup11;
            colorPalette5.Colors.Add(System.Drawing.Color.Thistle);
            this.Bmod.ColorPalette = colorPalette5;
            this.Bmod.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.Bmod.DataPointLabel = "=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
    "ields.TOTAL_MOD_ASSIGNED))))/100,0)";
            this.Bmod.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bmod.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Bmod.DataPointLabelFormat = "{0:P0}";
            this.Bmod.DataPointLabelStyle.Visible = false;
            this.Bmod.Name = "Bmod";
            this.Bmod.SeriesGroup = graphGroup7;
            this.Bmod.ToolTip.Text = "=(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / Fields.TOTAL_MOD_ASSIGNED))))";
            this.Bmod.ToolTip.Title = "MOD% Accuracy";
            this.Bmod.Y = "=iif(Fields.TOTAL_MOD_ASSIGNED<>0,(100-((Round(Fields.TOTAL_MOD_CHANGED * 100 / F" +
    "ields.TOTAL_MOD_ASSIGNED))))/100,0)";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // OPTrendingAccuracy
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "OPTrendingAccuracy";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "2";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "= AllDistinctValues(Fields.CHART_TYPE_KEY)";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "= AllDistinctValues(Fields.CRA_PERSONNEL_ID)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.Value = "3655";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartTypeName";
            reportParameter6.Value = "-1";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "IcdType";
            reportParameter7.Value = "2";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "Startdate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "08-05-2002";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "EndDate";
            reportParameter9.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter9.Value = "08-05-2017";
            reportParameter9.Visible = true;
            reportParameter10.AllowBlank = false;
            reportParameter10.AllowNull = true;
            reportParameter10.AutoRefresh = true;
            reportParameter10.Mergeable = false;
            reportParameter10.Name = "GroupMode";
            reportParameter10.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter10.Value = "2";
            reportParameter10.Visible = true;
            reportParameter11.AllowBlank = false;
            reportParameter11.AllowNull = true;
            reportParameter11.AutoRefresh = true;
            reportParameter11.Mergeable = false;
            reportParameter11.Name = "NumberofCharts";
            reportParameter11.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter11.Value = "10";
            reportParameter11.Visible = true;
            reportParameter12.AllowBlank = false;
            reportParameter12.AllowNull = true;
            reportParameter12.AutoRefresh = true;
            reportParameter12.Mergeable = false;
            reportParameter12.Name = "TimeFrame";
            reportParameter12.Value = "week";
            reportParameter12.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(16.002101898193359D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource OPTrendinAccuracy;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.BarSeries BApc;
        private Telerik.Reporting.BarSeries Bcm;
        private Telerik.Reporting.BarSeries Bpcs;
        private Telerik.Reporting.BarSeries Bcpt;
        private Telerik.Reporting.BarSeries Bmod;
        private Telerik.Reporting.TextBox textBox35;
    }
}