namespace ReportsLibrary.ThemisReports
{
    partial class InpatientFinancialImpact
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction3 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction4 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction5 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction6 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction7 = new Telerik.Reporting.SortingAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InpatientFinancialImpact));
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction8 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette1 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette2 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette3 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette4 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.NumericalScaleCrossAxisPosition numericalScaleCrossAxisPosition1 = new Telerik.Reporting.NumericalScaleCrossAxisPosition();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector3 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule6 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector4 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule7 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector5 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.TotalFIValueSql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.crosstab1 = new Telerik.Reporting.Crosstab();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.InpatientFinancialImpactSql = new Telerik.Reporting.SqlDataSource();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.B1 = new Telerik.Reporting.BarSeries();
            this.B2 = new Telerik.Reporting.BarSeries();
            this.B3 = new Telerik.Reporting.BarSeries();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5187505483627319D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox5.StyleName = "Apex.TableGroup";
            this.textBox5.Value = "= Fields.DISPLAY_NAME";
            // 
            // textBox1
            // 
            sortingAction1.SortingExpression = "= Fields.TOTAL_ASSIGNED_DRG";
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detail";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_NAME"));
            tableGroup1.Name = "dISPLAY_NAME";
            tableGroup1.ReportItem = this.textBox5;
            tableGroup1.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.DISPLAY_NAME", Telerik.Reporting.SortDirection.Asc));
            sortingAction1.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox1.Action = sortingAction1;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75834089517593384D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "MS-DRGs Assigned";
            // 
            // textBox2
            // 
            sortingAction2.SortingExpression = "= Fields.TOTAL_CHANGED_DRG";
            sortingAction2.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox2.Action = sortingAction2;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74792569875717163D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "MS-DRGs Changed";
            // 
            // textBox12
            // 
            sortingAction3.SortingExpression = "=((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fields.TOTAL_ASSIGNED" +
    "_DRG * 1.0))";
            sortingAction3.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox12.Action = sortingAction3;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78124898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox12.StyleName = "Apex.TableHeader";
            this.textBox12.Value = "MS-DRGs Accuracy";
            // 
            // textBox3
            // 
            sortingAction4.SortingExpression = "= Fields.TOTAL_ASSIGNED";
            sortingAction4.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox3.Action = sortingAction4;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458588838577271D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "Codes Assigned";
            // 
            // textBox4
            // 
            sortingAction5.SortingExpression = "= Fields.TOTAL_CHANGED";
            sortingAction5.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox4.Action = sortingAction5;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458910703659058D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox4.StyleName = "Apex.TableHeader";
            this.textBox4.Value = "Codes Changed";
            // 
            // textBox15
            // 
            sortingAction6.SortingExpression = "=((Fields.TOTAL_ASSIGNED - Fields.TOTAL_CHANGED) / (Fields.TOTAL_ASSIGNED * 1.0))" +
    "";
            sortingAction6.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox15.Action = sortingAction6;
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333319425582886D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox15.StyleName = "Apex.TableHeader";
            this.textBox15.Value = "Code Accuracy";
            // 
            // textBox22
            // 
            sortingAction7.SortingExpression = "= Fields.FINANCIAL_IMPACT";
            sortingAction7.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox22.Action = sortingAction7;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71875178813934326D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox22.StyleName = "Apex.TableHeader";
            this.textBox22.Value = "Financial Impact";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5187505483627319D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox6.StyleName = "Apex.GrandTotal";
            this.textBox6.Value = "Total";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // TotalFIValueSql
            // 
            this.TotalFIValueSql.ConnectionString = "THEMISConnectionString";
            this.TotalFIValueSql.Name = "TotalFIValueSql";
            this.TotalFIValueSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@idBeginDate", System.Data.DbType.String, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@idEndDate", System.Data.DbType.String, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclChartType", System.Data.DbType.String, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclFacilities", System.Data.DbType.String, "= Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclRouting", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclFacCoders", System.Data.DbType.String, "= Join(\',\', Parameters.Coder.Value)")});
            this.TotalFIValueSql.SelectCommand = resources.GetString("TotalFIValueSql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.50003933906555176D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox27});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.9000003337860107D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Segoe UI";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox27.Style.Font.Underline = true;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "Inpatient  Finanical Impact  ";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(4.6999607086181641D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.crosstab1,
            this.graph1,
            this.textBox26,
            this.textBox28});
            this.detail.Name = "detail";
            // 
            // crosstab1
            // 
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.75834095478057861D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.74792557954788208D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.78124868869781494D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.66458570957183838D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.664588987827301D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.58333313465118408D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71875202655792236D)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.crosstab1.Body.SetCellContent(0, 0, this.textBox8);
            this.crosstab1.Body.SetCellContent(0, 1, this.textBox9);
            this.crosstab1.Body.SetCellContent(0, 3, this.textBox10);
            this.crosstab1.Body.SetCellContent(0, 4, this.textBox11);
            this.crosstab1.Body.SetCellContent(1, 0, this.textBox16);
            this.crosstab1.Body.SetCellContent(1, 1, this.textBox17);
            this.crosstab1.Body.SetCellContent(1, 3, this.textBox18);
            this.crosstab1.Body.SetCellContent(1, 4, this.textBox19);
            this.crosstab1.Body.SetCellContent(0, 2, this.textBox13);
            this.crosstab1.Body.SetCellContent(1, 2, this.textBox14);
            this.crosstab1.Body.SetCellContent(0, 5, this.textBox20);
            this.crosstab1.Body.SetCellContent(1, 5, this.textBox21);
            this.crosstab1.Body.SetCellContent(0, 6, this.textBox23);
            this.crosstab1.Body.SetCellContent(1, 6, this.textBox24);
            tableGroup3.Name = "tOTAL_ASSIGNED_DRG";
            tableGroup3.ReportItem = this.textBox1;
            tableGroup4.Name = "tOTAL_CHANGED_DRG";
            tableGroup4.ReportItem = this.textBox2;
            tableGroup5.Name = "group";
            tableGroup5.ReportItem = this.textBox12;
            tableGroup6.Name = "tOTAL_ASSIGNED";
            tableGroup6.ReportItem = this.textBox3;
            tableGroup7.Name = "tOTAL_CHANGED";
            tableGroup7.ReportItem = this.textBox4;
            tableGroup8.Name = "group1";
            tableGroup8.ReportItem = this.textBox15;
            tableGroup9.Name = "group2";
            tableGroup9.ReportItem = this.textBox22;
            this.crosstab1.ColumnGroups.Add(tableGroup3);
            this.crosstab1.ColumnGroups.Add(tableGroup4);
            this.crosstab1.ColumnGroups.Add(tableGroup5);
            this.crosstab1.ColumnGroups.Add(tableGroup6);
            this.crosstab1.ColumnGroups.Add(tableGroup7);
            this.crosstab1.ColumnGroups.Add(tableGroup8);
            this.crosstab1.ColumnGroups.Add(tableGroup9);
            this.crosstab1.Corner.SetCellContent(0, 0, this.textBox7);
            this.crosstab1.DataSource = this.InpatientFinancialImpactSql;
            this.crosstab1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox13,
            this.textBox10,
            this.textBox11,
            this.textBox20,
            this.textBox23,
            this.textBox16,
            this.textBox17,
            this.textBox14,
            this.textBox18,
            this.textBox19,
            this.textBox21,
            this.textBox24,
            this.textBox1,
            this.textBox2,
            this.textBox12,
            this.textBox3,
            this.textBox4,
            this.textBox15,
            this.textBox22,
            this.textBox5,
            this.textBox6});
            this.crosstab1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(4.0999608039855957D));
            this.crosstab1.Name = "crosstab1";
            this.crosstab1.NoDataMessage = "No Data Found.";
            tableGroup10.Name = "total";
            tableGroup10.ReportItem = this.textBox6;
            this.crosstab1.RowGroups.Add(tableGroup1);
            this.crosstab1.RowGroups.Add(tableGroup10);
            this.crosstab1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.437525749206543D), Telerik.Reporting.Drawing.Unit.Inch(0.59999996423721313D));
            this.crosstab1.StyleName = "Apex.TableNormal";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75834089517593384D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox8.StyleName = "Apex.TableBody";
            this.textBox8.Value = "= Fields.TOTAL_ASSIGNED_DRG";
            // 
            // textBox9
            // 
            this.textBox9.Action = null;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74792569875717163D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox9.StyleName = "Apex.TableBody";
            this.textBox9.Value = "= Fields.TOTAL_CHANGED_DRG";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458588838577271D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox10.StyleName = "Apex.TableBody";
            this.textBox10.Value = "= Fields.TOTAL_ASSIGNED";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458910703659058D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox11.StyleName = "Apex.TableBody";
            this.textBox11.Value = "= Fields.TOTAL_CHANGED";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75834089517593384D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox16.StyleName = "Apex.GrandTotal";
            this.textBox16.Value = "= Sum(Fields.TOTAL_ASSIGNED_DRG)";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74792569875717163D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox17.StyleName = "Apex.GrandTotal";
            this.textBox17.Value = "= Sum(Fields.TOTAL_CHANGED_DRG)";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458588838577271D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox18.StyleName = "Apex.GrandTotal";
            this.textBox18.Value = "= Sum(Fields.TOTAL_ASSIGNED)";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.66458910703659058D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox19.StyleName = "Apex.GrandTotal";
            this.textBox19.Value = "= Sum(Fields.TOTAL_CHANGED)";
            // 
            // textBox13
            // 
            this.textBox13.Format = "{0:P2}";
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78124898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox13.StyleName = "Apex.TableBody";
            this.textBox13.Value = "=iif(Fields.TOTAL_ASSIGNED_DRG=0,0,((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHA" +
    "NGED_DRG) / (Fields.TOTAL_ASSIGNED_DRG * 1)))";
            // 
            // textBox14
            // 
            this.textBox14.Format = "{0:P2}";
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78124898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox14.StyleName = "Apex.GrandTotal";
            this.textBox14.Value = "=iif(CDbl(sum(Fields.TOTAL_ASSIGNED_DRG))<>0, 1-(cDbl(sum(Fields.TOTAL_CHANGED_DR" +
    "G)) / cDbl(sum(Fields.TOTAL_ASSIGNED_DRG))),0) ";
            // 
            // textBox20
            // 
            this.textBox20.Format = "{0:P2}";
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333319425582886D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox20.StyleName = "Apex.TableBody";
            this.textBox20.Value = "=((Fields.TOTAL_ASSIGNED - Fields.TOTAL_CHANGED) / (Fields.TOTAL_ASSIGNED * 1))";
            // 
            // textBox21
            // 
            this.textBox21.Format = "{0:P2}";
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333319425582886D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox21.StyleName = "Apex.GrandTotal";
            this.textBox21.Value = "=iif(CDbl(Sum(Fields.TOTAL_ASSIGNED))<>0,1-(CDbl(Sum(Fields.TOTAL_CHANGED))/ CDbl" +
    "(Sum(Fields.TOTAL_ASSIGNED))),0)";
            // 
            // textBox23
            // 
            this.textBox23.Format = "{0}";
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71875178813934326D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox23.StyleName = "Apex.TableBody";
            this.textBox23.Value = "= \'$\'+ Format(\'{0:N2}\',Fields.FINANCIAL_IMPACT)";
            // 
            // textBox24
            // 
            this.textBox24.Format = "{0}";
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71875178813934326D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox24.StyleName = "Apex.GrandTotal";
            this.textBox24.Value = "=\'$\'+Format(\'{0:N2}\',CDbl(Sum(Fields.FINANCIAL_IMPACT)))";
            // 
            // textBox7
            // 
            sortingAction8.SortingExpression = "= Fields.DISPLAY_NAME";
            sortingAction8.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup1});
            this.textBox7.Action = sortingAction8;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5187505483627319D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox7.StyleName = "Apex.TableHeader";
            this.textBox7.Value = "Coder";
            // 
            // InpatientFinancialImpactSql
            // 
            this.InpatientFinancialImpactSql.ConnectionString = "THEMISConnectionString";
            this.InpatientFinancialImpactSql.Name = "InpatientFinancialImpactSql";
            this.InpatientFinancialImpactSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.InpatientFinancialImpactSql.SelectCommand = "dbo.usp_Metis_MsDrg_FinancialImpact_Select";
            this.InpatientFinancialImpactSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.UNDERCODE_AMOUNT"));
            graphGroup1.Name = "UnderCodeGroup";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.OVERCODE_AMOUNT"));
            graphGroup2.Name = "OverCodeGroup";
            graphGroup3.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FINANCIAL_IMPACT"));
            graphGroup3.Name = "FinanicalImpactGroup";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CategoryGroups.Add(graphGroup2);
            this.graph1.CategoryGroups.Add(graphGroup3);
            colorPalette1.Colors.Add(System.Drawing.Color.Blue);
            colorPalette1.Colors.Add(System.Drawing.Color.Red);
            colorPalette1.Colors.Add(System.Drawing.Color.Green);
            this.graph1.ColorPalette = colorPalette1;
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph1.DataSource = this.TotalFIValueSql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Legend.Style.Visible = true;
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8717865108046681E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.20011806488037109D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.B1);
            this.graph1.Series.Add(this.B2);
            this.graph1.Series.Add(this.B3);
            this.graph1.SeriesGroups.Add(graphGroup4);
            this.graph1.SeriesGroups.Add(graphGroup5);
            this.graph1.SeriesGroups.Add(graphGroup6);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(3.8997635841369629D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "graph1";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.XAxis = this.graphAxis3;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis4;
            // 
            // graphAxis3
            // 
            this.graphAxis3.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.AtMinimum;
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "GraphAxis1";
            this.graphAxis3.Scale = categoryScale1;
            this.graphAxis3.Style.Font.Bold = true;
            this.graphAxis3.Style.Visible = false;
            this.graphAxis3.Title = "X-Axis";
            // 
            // graphAxis4
            // 
            this.graphAxis4.LabelFormat = "{0:$#,##0.00}";
            this.graphAxis4.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.AtMinimum;
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "GraphAxis2";
            this.graphAxis4.Scale = numericalScale1;
            this.graphAxis4.Style.Font.Bold = true;
            this.graphAxis4.Title = "Y-Axis";
            // 
            // B1
            // 
            this.B1.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.B1.CategoryGroup = graphGroup1;
            colorPalette2.Colors.Add(System.Drawing.Color.Blue);
            this.B1.ColorPalette = colorPalette2;
            this.B1.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.B1.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B1.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B1.LegendItem.Value = "Increased";
            this.B1.Name = "B1";
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.UNDERCODE_AMOUNT"));
            graphGroup4.Name = "UnderSeries";
            this.B1.SeriesGroup = graphGroup4;
            this.B1.ToolTip.Text = "Increased";
            this.B1.ToolTip.Title = "=\'$\'+FORMAT(\'{0:N2}\', ABS(sum(Fields.UNDERCODE_AMOUNT))) ";
            this.B1.Y = "=sum(Fields.UNDERCODE_AMOUNT)";
            // 
            // B2
            // 
            this.B2.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.B2.CategoryGroup = graphGroup2;
            colorPalette3.Colors.Add(System.Drawing.Color.Red);
            this.B2.ColorPalette = colorPalette3;
            this.B2.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.B2.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B2.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B2.LegendItem.Value = "Decreased";
            this.B2.Name = "B2";
            graphGroup5.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.OVERCODE_AMOUNT"));
            graphGroup5.Name = "OvercodeSeries";
            this.B2.SeriesGroup = graphGroup5;
            this.B2.ToolTip.Text = "descreased";
            this.B2.ToolTip.Title = "=\'$\'+FORMAT(\'{0:N2}\', sum(Fields.OVERCODE_AMOUNT)) ";
            this.B2.Y = "=sum(Fields.OVERCODE_AMOUNT)";
            // 
            // B3
            // 
            this.B3.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.B3.CategoryGroup = graphGroup3;
            colorPalette4.Colors.Add(System.Drawing.Color.Green);
            this.B3.ColorPalette = colorPalette4;
            this.B3.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.B3.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B3.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B3.LegendItem.Value = "Net Variance";
            this.B3.Name = "B3";
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FINANCIAL_IMPACT"));
            graphGroup6.Name = "FinanicalSeriesGroup";
            this.B3.SeriesGroup = graphGroup6;
            this.B3.ToolTip.Text = "Net Variance";
            this.B3.ToolTip.Title = "=\'$\'+FORMAT(\'{0:N2}\', sum(Fields.FINANCIAL_IMPACT)) ";
            this.B3.Y = "=sum(Fields.FINANCIAL_IMPACT)";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.3333253860473633D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Underline = false;
            this.textBox26.Value = "Financial Impact of MS-DRG Changes:The MS-DRG changes resulted in a net {=iif(Fie" +
    "lds.FINANCIAL_IMPACT > 0, \'increase\',\'decrease\')} of  $                        i" +
    "n Medicare reimbursement.";
            // 
            // textBox28
            // 
            this.textBox28.Format = "{0}";
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0333265066146851D), Telerik.Reporting.Drawing.Unit.Inch(0.19984261691570282D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox28.Value = "= Format(\'{0:N2}\', Fields.FINANCIAL_IMPACT)";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // graphAxis1
            // 
            this.graphAxis1.LabelFormat = "{0:C2}";
            this.graphAxis1.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.AtMinimum;
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "graphAxis1";
            numericalScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.AtMinimum;
            numericalScaleCrossAxisPosition1.Value = 0D;
            numericalScale2.CrossAxisPositions.Add(numericalScaleCrossAxisPosition1);
            numericalScale2.Minimum = 0D;
            this.graphAxis1.Scale = numericalScale2;
            // 
            // graphAxis2
            // 
            this.graphAxis2.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            this.graphAxis2.Scale = categoryScale2;
            // 
            // InpatientFinancialImpact
            // 
            this.DataSource = this.TotalFIValueSql;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "InpatientFinancialImpact";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.Mergeable = false;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.Mergeable = false;
            reportParameter4.Name = "UserId";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter4.Value = "";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.AvailableValues.DataSource = this.CoderSql;
            reportParameter5.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter5.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter5.MultiValue = true;
            reportParameter5.Name = "Coder";
            reportParameter5.Value = "";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartTypeName";
            reportParameter6.Value = "Inpatient";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "IcdType";
            reportParameter7.Value = "2";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "Startdate";
            reportParameter8.Value = "";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "EndDate";
            reportParameter9.Value = "";
            reportParameter9.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.SubTotal")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.GrandTotal")});
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector3});
            styleRule5.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(216)))));
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Font.Bold = true;
            styleRule5.Style.Font.Name = "Book Antiqua";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector4});
            styleRule6.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule6.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule6.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule6.Style.Font.Name = "Book Antiqua";
            styleRule6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableGroup")});
            styleRule7.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector5});
            styleRule7.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule7.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule7.Style.Font.Name = "Book Antiqua";
            styleRule7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5,
            styleRule6,
            styleRule7});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.3332457542419434D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource InpatientFinancialImpactSql;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.Crosstab crosstab1;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.SqlDataSource TotalFIValueSql;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.BarSeries B1;
        private Telerik.Reporting.BarSeries B2;
        private Telerik.Reporting.BarSeries B3;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
    }
}