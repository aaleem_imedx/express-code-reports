namespace ReportsLibrary.ThemisReports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for OPUnderlyingCauseOfError.
    /// </summary>
    public partial class OPUnderlyingCauseOfError : Telerik.Reporting.Report
    {
        public OPUnderlyingCauseOfError()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}