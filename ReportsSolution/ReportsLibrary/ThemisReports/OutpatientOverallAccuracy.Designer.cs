namespace ReportsLibrary.ThemisReports
{
    partial class OutpatientOverallAccuracy
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.GraphAxis graphAxis2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutpatientOverallAccuracy));
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition1 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette1 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette2 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup7 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette3 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup8 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette4 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup9 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette5 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup10 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup11 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup12 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle2 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale3 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette6 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup13 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette7 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup14 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup15 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup16 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle3 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale4 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette8 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup17 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette9 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup18 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup19 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup20 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle4 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale5 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale4 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette10 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup21 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette11 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup22 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup23 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup24 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle5 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale6 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale5 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette12 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup25 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette13 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup26 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup27 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup28 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle6 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale7 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale6 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.Drawing.ColorPalette colorPalette14 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup29 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette15 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup30 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.NumericalScale numericalScale7 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.OPoverAllAccuracysql = new Telerik.Reporting.SqlDataSource();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem2 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.APC = new Telerik.Reporting.BarSeries();
            this.CM = new Telerik.Reporting.BarSeries();
            this.PCs = new Telerik.Reporting.BarSeries();
            this.CPT = new Telerik.Reporting.BarSeries();
            this.MOD = new Telerik.Reporting.BarSeries();
            this.graph2 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis10 = new Telerik.Reporting.GraphAxis();
            this.graphAxis9 = new Telerik.Reporting.GraphAxis();
            this.barSeries1 = new Telerik.Reporting.BarSeries();
            this.barSeries2 = new Telerik.Reporting.BarSeries();
            this.graph5 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem5 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis12 = new Telerik.Reporting.GraphAxis();
            this.graphAxis11 = new Telerik.Reporting.GraphAxis();
            this.barSeries7 = new Telerik.Reporting.BarSeries();
            this.barSeries8 = new Telerik.Reporting.BarSeries();
            this.graph3 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem3 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis6 = new Telerik.Reporting.GraphAxis();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.barSeries3 = new Telerik.Reporting.BarSeries();
            this.barSeries4 = new Telerik.Reporting.BarSeries();
            this.graph4 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem4 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis8 = new Telerik.Reporting.GraphAxis();
            this.graphAxis7 = new Telerik.Reporting.GraphAxis();
            this.barSeries5 = new Telerik.Reporting.BarSeries();
            this.barSeries6 = new Telerik.Reporting.BarSeries();
            this.graph6 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem6 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis14 = new Telerik.Reporting.GraphAxis();
            this.graphAxis13 = new Telerik.Reporting.GraphAxis();
            this.barSeries9 = new Telerik.Reporting.BarSeries();
            this.barSeries10 = new Telerik.Reporting.BarSeries();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            graphAxis2 = new Telerik.Reporting.GraphAxis();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // graphAxis2
            // 
            graphAxis2.LabelAngle = 45;
            graphAxis2.LabelFormat = "{0:P0}";
            graphAxis2.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            graphAxis2.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis2.MajorGridLineStyle.Visible = false;
            graphAxis2.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis2.MinorGridLineStyle.Visible = false;
            graphAxis2.Name = resources.GetString("graphAxis2.Name");
            categoryScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.Auto;
            categoryScale1.CrossAxisPositions.Add(categoryScaleCrossAxisPosition1);
            graphAxis2.Scale = categoryScale1;
            graphAxis2.Title = "MS-DRG       CM          PCS";
            graphAxis2.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.Auto;
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42499911785125732D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "Cases";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42499911785125732D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox33.StyleName = "Apex.TableHeader";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63541591167449951D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox53.StyleName = "Apex.TableHeader";
            this.textBox53.Value = "Assigned";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.625000536441803D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox36.StyleName = "Apex.TableHeader";
            this.textBox36.Value = "Changed";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583307504653931D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox49.StyleName = "Apex.TableHeader";
            this.textBox49.Value = "Accuracy";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9062494039535523D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.StyleName = "Apex.TableHeader";
            this.textBox35.Value = "Diagnosis";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67708367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox51.StyleName = "Apex.TableHeader";
            this.textBox51.Value = "Assigned";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6562504768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.StyleName = "Apex.TableHeader";
            this.textBox5.Value = "Changed";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73958492279052734D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox25.StyleName = "Apex.TableHeader";
            this.textBox25.Value = "Accuracy";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0729191303253174D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox39.StyleName = "Apex.TableHeader";
            this.textBox39.Value = "PC Proceedure";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65624982118606567D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox37.StyleName = "Apex.TableHeader";
            this.textBox37.Value = "Assigned";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67708307504653931D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox59.StyleName = "Apex.TableHeader";
            this.textBox59.Value = "Changed";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.635417103767395D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox56.StyleName = "Apex.TableHeader";
            this.textBox56.Value = "Accuracy";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9687498807907105D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.StyleName = "Apex.TableHeader";
            this.textBox27.Value = "CPT/HCPCS";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69791662693023682D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox7.StyleName = "Apex.TableHeader";
            this.textBox7.Value = "Assigned";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65624994039535522D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox40.StyleName = "Apex.TableHeader";
            this.textBox40.Value = "Changed";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583438634872437D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox17.StyleName = "Apex.TableHeader";
            this.textBox17.Value = "Accuracy";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.StyleName = "Apex.TableHeader";
            this.textBox28.Value = "Modifiers";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "Assigned";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333547115325928D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "Changed";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583480358123779D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox23.StyleName = "Apex.TableHeader";
            this.textBox23.Value = "Accuracy";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9125034809112549D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.StyleName = "Apex.TableHeader";
            this.textBox34.Value = "APC";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\',Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3312885761260986D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8000023365020752D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Segoe UI";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox4.Style.Font.Underline = true;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "OutPatient Overall Accuracy";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(5.6001968383789062D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.graph1,
            this.graph2,
            this.graph5,
            this.graph3,
            this.graph4,
            this.graph6});
            this.detail.Name = "detail";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.42499938607215881D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63541591167449951D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.6250007152557373D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.64583307504653931D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.67708367109298706D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.6562504768371582D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73958504199981689D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.65624940395355225D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.67708367109298706D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63541746139526367D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.69791692495346069D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.65624940395355225D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.645834743976593D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63333320617675781D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63333559036254883D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.645834743976593D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox12);
            this.table1.Body.SetCellContent(0, 13, this.textBox13);
            this.table1.Body.SetCellContent(0, 14, this.textBox14);
            this.table1.Body.SetCellContent(0, 15, this.textBox24);
            this.table1.Body.SetCellContent(0, 1, this.textBox54);
            this.table1.Body.SetCellContent(0, 2, this.textBox38);
            this.table1.Body.SetCellContent(0, 3, this.textBox50);
            this.table1.Body.SetCellContent(0, 4, this.textBox52);
            this.table1.Body.SetCellContent(0, 5, this.textBox15);
            this.table1.Body.SetCellContent(0, 6, this.textBox26);
            this.table1.Body.SetCellContent(0, 7, this.textBox61);
            this.table1.Body.SetCellContent(0, 8, this.textBox60);
            this.table1.Body.SetCellContent(0, 9, this.textBox57);
            this.table1.Body.SetCellContent(0, 10, this.textBox42);
            this.table1.Body.SetCellContent(0, 11, this.textBox41);
            this.table1.Body.SetCellContent(0, 12, this.textBox18);
            tableGroup2.Name = "group5";
            tableGroup2.ReportItem = this.textBox1;
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.Name = "cHART_COUNT";
            tableGroup1.ReportItem = this.textBox33;
            tableGroup4.Name = "group1";
            tableGroup4.ReportItem = this.textBox53;
            tableGroup5.Name = "group22";
            tableGroup5.ReportItem = this.textBox36;
            tableGroup6.Name = "group24";
            tableGroup6.ReportItem = this.textBox49;
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.ChildGroups.Add(tableGroup6);
            tableGroup3.Name = "group";
            tableGroup3.ReportItem = this.textBox35;
            tableGroup8.Name = "group10";
            tableGroup8.ReportItem = this.textBox51;
            tableGroup9.Name = "group21";
            tableGroup9.ReportItem = this.textBox5;
            tableGroup10.Name = "group25";
            tableGroup10.ReportItem = this.textBox25;
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.ChildGroups.Add(tableGroup9);
            tableGroup7.ChildGroups.Add(tableGroup10);
            tableGroup7.Name = "group9";
            tableGroup7.ReportItem = this.textBox39;
            tableGroup12.Name = "group29";
            tableGroup12.ReportItem = this.textBox37;
            tableGroup13.Name = "group27";
            tableGroup13.ReportItem = this.textBox59;
            tableGroup14.Name = "group23";
            tableGroup14.ReportItem = this.textBox56;
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.ChildGroups.Add(tableGroup13);
            tableGroup11.ChildGroups.Add(tableGroup14);
            tableGroup11.Name = "group28";
            tableGroup11.ReportItem = this.textBox27;
            tableGroup16.Name = "group26";
            tableGroup16.ReportItem = this.textBox7;
            tableGroup17.Name = "group13";
            tableGroup17.ReportItem = this.textBox40;
            tableGroup18.Name = "group11";
            tableGroup18.ReportItem = this.textBox17;
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.ChildGroups.Add(tableGroup17);
            tableGroup15.ChildGroups.Add(tableGroup18);
            tableGroup15.Name = "group14";
            tableGroup15.ReportItem = this.textBox28;
            tableGroup20.Name = "group6";
            tableGroup20.ReportItem = this.textBox2;
            tableGroup21.Name = "group7";
            tableGroup21.ReportItem = this.textBox3;
            tableGroup22.Name = "group8";
            tableGroup22.ReportItem = this.textBox23;
            tableGroup19.ChildGroups.Add(tableGroup20);
            tableGroup19.ChildGroups.Add(tableGroup21);
            tableGroup19.ChildGroups.Add(tableGroup22);
            tableGroup19.Name = "tOTAL_APC_ASSIGNED";
            tableGroup19.ReportItem = this.textBox34;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.ColumnGroups.Add(tableGroup15);
            this.table1.ColumnGroups.Add(tableGroup19);
            this.table1.DataSource = this.OPoverAllAccuracysql;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox54,
            this.textBox38,
            this.textBox50,
            this.textBox52,
            this.textBox15,
            this.textBox26,
            this.textBox61,
            this.textBox60,
            this.textBox57,
            this.textBox42,
            this.textBox41,
            this.textBox18,
            this.textBox13,
            this.textBox14,
            this.textBox24,
            this.textBox33,
            this.textBox1,
            this.textBox35,
            this.textBox53,
            this.textBox36,
            this.textBox49,
            this.textBox39,
            this.textBox51,
            this.textBox5,
            this.textBox25,
            this.textBox27,
            this.textBox37,
            this.textBox59,
            this.textBox56,
            this.textBox28,
            this.textBox7,
            this.textBox40,
            this.textBox17,
            this.textBox34,
            this.textBox2,
            this.textBox3,
            this.textBox23});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(5.0001969337463379D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            this.table1.NoDataStyle.Font.Bold = true;
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup23.Name = "detail";
            this.table1.RowGroups.Add(tableGroup23);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(10.285423278808594D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.table1.StyleName = "Apex.TableNormal";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42499911785125732D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox12.StyleName = "Apex.TableBody";
            this.textBox12.Value = "= Fields.CHART_COUNT";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox13.StyleName = "Apex.TableBody";
            this.textBox13.Value = "= Fields.TOTAL_APC_ASSIGNED";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333547115325928D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox14.StyleName = "Apex.TableBody";
            this.textBox14.Value = "= Fields.TOTAL_APC_CHANGED";
            // 
            // textBox24
            // 
            this.textBox24.Format = "{0:P2}";
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583480358123779D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox24.StyleName = "Apex.TableBody";
            this.textBox24.Value = "=iif(Fields.TOTAL_APC_ASSIGNED=0,0,(Fields.TOTAL_APC_ASSIGNED - Fields.TOTAL_APC_" +
    "CHANGED) / (Fields.TOTAL_APC_ASSIGNED*1))";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63541591167449951D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox54.StyleName = "Apex.TableBody";
            this.textBox54.Value = "= Fields.TOTAL_CM_ASSIGNED";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.625000536441803D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox38.StyleName = "Apex.TableBody";
            this.textBox38.Value = "= Fields.TOTAL_CM_CHANGED";
            // 
            // textBox50
            // 
            this.textBox50.Format = "{0:P2}";
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583307504653931D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox50.StyleName = "Apex.TableBody";
            this.textBox50.Value = "=iif(Fields.TOTAL_CM_ASSIGNED=0,0,(Fields.TOTAL_CM_ASSIGNED - Fields.TOTAL_CM_CHA" +
    "NGED) / (Fields.TOTAL_CM_ASSIGNED * 1))";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67708367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox52.StyleName = "Apex.TableBody";
            this.textBox52.Value = "= Fields.TOTAL_PCS_ASSIGNED";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6562504768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox15.StyleName = "Apex.TableBody";
            this.textBox15.Value = "= Fields.TOTAL_PCS_CHANGED";
            // 
            // textBox26
            // 
            this.textBox26.Format = "{0:P2}";
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73958492279052734D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox26.StyleName = "Apex.TableBody";
            this.textBox26.Value = "=iif(Fields.TOTAL_PCS_ASSIGNED=0,0,(Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_" +
    "CHANGED) / (Fields.TOTAL_PCS_ASSIGNED*1))";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65624982118606567D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox61.StyleName = "Apex.TableBody";
            this.textBox61.Value = "= Fields.TOTAL_CPT_ASSIGNED";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67708307504653931D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox60.StyleName = "Apex.TableBody";
            this.textBox60.Value = "= Fields.TOTAL_CPT_CHANGED";
            // 
            // textBox57
            // 
            this.textBox57.Format = "{0:P2}";
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.635417103767395D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox57.StyleName = "Apex.TableBody";
            this.textBox57.Value = "=iif(Fields.TOTAL_CPT_ASSIGNED=0,0,(Fields.TOTAL_CPT_ASSIGNED - Fields.TOTAL_CPT_" +
    "CHANGED) / (Fields.TOTAL_CPT_ASSIGNED*1))";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69791662693023682D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox42.StyleName = "Apex.TableBody";
            this.textBox42.Value = "= Fields.TOTAL_MOD_ASSIGNED";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65624994039535522D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox41.StyleName = "Apex.TableBody";
            this.textBox41.Value = "= Fields.TOTAL_MOD_CHANGED";
            // 
            // textBox18
            // 
            this.textBox18.Format = "{0:P2}";
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583438634872437D), Telerik.Reporting.Drawing.Unit.Inch(0.2000000923871994D));
            this.textBox18.StyleName = "Apex.TableBody";
            this.textBox18.Value = "=iif(Fields.TOTAL_MOD_ASSIGNED=0,0,(Fields.TOTAL_MOD_ASSIGNED - Fields.TOTAL_MOD_" +
    "CHANGED) / (Fields.TOTAL_MOD_ASSIGNED*1))";
            // 
            // OPoverAllAccuracysql
            // 
            this.OPoverAllAccuracysql.ConnectionString = "THEMISConnectionString";
            this.OPoverAllAccuracysql.Name = "OPoverAllAccuracysql";
            this.OPoverAllAccuracysql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.String, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.String, "= join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.String, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.OPoverAllAccuracysql.SelectCommand = "dbo.usp_Metis_OP_OverallAccuracy_Select";
            this.OPoverAllAccuracysql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_APC_ASSIGNED - Fields.TOTAL_APC_CHANGED) / (Fields.TOTAL_APC_ASSIG" +
            "NED*1)"));
            graphGroup1.Name = "APC";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_CM_ASSIGNED - Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1)"));
            graphGroup2.Name = "CM";
            graphGroup3.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSIG" +
            "NED*1)"));
            graphGroup3.Name = "PCS";
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_CPT_ASSIGNED - Fields.TOTAL_CPT_CHANGED) / (Fields.TOTAL_CPT_ASSIG" +
            "NED*1)"));
            graphGroup4.Name = "CPT";
            graphGroup5.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_MOD_ASSIGNED - Fields.TOTAL_MOD_CHANGED) / (Fields.TOTAL_MOD_ASSIG" +
            "NED*1)"));
            graphGroup5.Name = "MOD";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CategoryGroups.Add(graphGroup2);
            this.graph1.CategoryGroups.Add(graphGroup3);
            this.graph1.CategoryGroups.Add(graphGroup4);
            this.graph1.CategoryGroups.Add(graphGroup5);
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem2);
            this.graph1.DataSource = this.OPoverAllAccuracysql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Legend.Style.Visible = false;
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.APC);
            this.graph1.Series.Add(this.CM);
            this.graph1.Series.Add(this.PCs);
            this.graph1.Series.Add(this.CPT);
            this.graph1.Series.Add(this.MOD);
            this.graph1.SeriesGroups.Add(graphGroup6);
            this.graph1.SeriesGroups.Add(graphGroup7);
            this.graph1.SeriesGroups.Add(graphGroup8);
            this.graph1.SeriesGroups.Add(graphGroup9);
            this.graph1.SeriesGroups.Add(graphGroup10);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.Font.Bold = true;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle1.Text = "ICD-10 Coding Accuracy";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem2
            // 
            this.cartesianCoordinateSystem2.Name = "cartesianCoordinateSystem2";
            this.cartesianCoordinateSystem2.XAxis = this.graphAxis3;
            this.cartesianCoordinateSystem2.YAxis = this.graphAxis4;
            // 
            // graphAxis3
            // 
            this.graphAxis3.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis3.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MajorGridLineStyle.Visible = false;
            this.graphAxis3.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "GraphAxis1";
            this.graphAxis3.Scale = categoryScale2;
            this.graphAxis3.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis3.Style.Visible = true;
            this.graphAxis3.Title = "APC      CM      PCS      CPT     MOD";
            this.graphAxis3.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.AtMinimum;
            // 
            // graphAxis4
            // 
            this.graphAxis4.LabelFormat = "{0:P0}";
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "GraphAxis2";
            numericalScale1.Minimum = 0D;
            this.graphAxis4.Scale = numericalScale1;
            this.graphAxis4.Title = "";
            // 
            // APC
            // 
            this.APC.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.APC.CategoryGroup = graphGroup1;
            colorPalette1.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.APC.ColorPalette = colorPalette1;
            this.APC.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.APC.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.APC.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.APC.DataPointLabelFormat = "{0:P2}";
            this.APC.Name = "APC";
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_APC_ASSIGNED - Fields.TOTAL_APC_CHANGED) / (Fields.TOTAL_APC_ASSIG" +
            "NED*1)"));
            graphGroup6.Name = "APC";
            this.APC.SeriesGroup = graphGroup6;
            this.APC.ToolTip.Text = "=iif(Fields.TOTAL_APC_ASSIGNED=0,0,Format(\'{0:P2}\',(Fields.TOTAL_APC_ASSIGNED - F" +
    "ields.TOTAL_APC_CHANGED) / (Fields.TOTAL_APC_ASSIGNED*1)))";
            this.APC.ToolTip.Title = "APC %";
            this.APC.Y = "=iif(Fields.TOTAL_APC_ASSIGNED=0,0,(Fields.TOTAL_APC_ASSIGNED - Fields.TOTAL_APC_" +
    "CHANGED) / (Fields.TOTAL_APC_ASSIGNED*1))";
            // 
            // CM
            // 
            this.CM.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.CM.CategoryGroup = graphGroup2;
            colorPalette2.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.CM.ColorPalette = colorPalette2;
            this.CM.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.CM.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.CM.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.CM.Name = "CM";
            graphGroup7.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_CM_ASSIGNED - Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1)"));
            graphGroup7.Name = "CM";
            this.CM.SeriesGroup = graphGroup7;
            this.CM.ToolTip.Text = "=iif(Fields.TOTAL_CM_ASSIGNED=0,0,Format(\'{0:P2}\',(Fields.TOTAL_CM_ASSIGNED - Fie" +
    "lds.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED * 1)))";
            this.CM.ToolTip.Title = "CM %";
            this.CM.Y = "=iif(Fields.TOTAL_CM_ASSIGNED=0,0,(Fields.TOTAL_CM_ASSIGNED - Fields.TOTAL_CM_CHA" +
    "NGED) / (Fields.TOTAL_CM_ASSIGNED * 1))";
            // 
            // PCs
            // 
            this.PCs.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.PCs.CategoryGroup = graphGroup3;
            colorPalette3.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(173)))), ((int)(((byte)(76))))));
            this.PCs.ColorPalette = colorPalette3;
            this.PCs.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.PCs.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.PCs.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.PCs.Name = "PCs";
            graphGroup8.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSIG" +
            "NED*1)"));
            graphGroup8.Name = "PCS";
            this.PCs.SeriesGroup = graphGroup8;
            this.PCs.ToolTip.Text = "=iif(Fields.TOTAL_PCS_ASSIGNED=0,0,Format(\'{0:P2}\',(Fields.TOTAL_PCS_ASSIGNED - F" +
    "ields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSIGNED*1)))";
            this.PCs.ToolTip.Title = "PCs";
            this.PCs.Y = "=iif(Fields.TOTAL_PCS_ASSIGNED=0,0,(Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_" +
    "CHANGED) / (Fields.TOTAL_PCS_ASSIGNED*1))";
            // 
            // CPT
            // 
            this.CPT.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.CPT.CategoryGroup = graphGroup4;
            colorPalette4.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(150)))), ((int)(((byte)(202))))));
            this.CPT.ColorPalette = colorPalette4;
            this.CPT.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.CPT.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.CPT.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.CPT.Name = "CPT";
            graphGroup9.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_CPT_ASSIGNED - Fields.TOTAL_CPT_CHANGED) / (Fields.TOTAL_CPT_ASSIG" +
            "NED*1)"));
            graphGroup9.Name = "CPT";
            this.CPT.SeriesGroup = graphGroup9;
            this.CPT.ToolTip.Text = "=iif(Fields.TOTAL_CPT_ASSIGNED=0,0,Format(\'{0:P2}\',(Fields.TOTAL_CPT_ASSIGNED - F" +
    "ields.TOTAL_CPT_CHANGED) / (Fields.TOTAL_CPT_ASSIGNED*1)))";
            this.CPT.ToolTip.Title = "CPT %";
            this.CPT.Y = "=iif(Fields.TOTAL_CPT_ASSIGNED=0,0,(Fields.TOTAL_CPT_ASSIGNED - Fields.TOTAL_CPT_" +
    "CHANGED) / (Fields.TOTAL_CPT_ASSIGNED*1))";
            // 
            // MOD
            // 
            this.MOD.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.MOD.CategoryGroup = graphGroup5;
            colorPalette5.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(107)))), ((int)(((byte)(136))))));
            this.MOD.ColorPalette = colorPalette5;
            this.MOD.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.MOD.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.MOD.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.MOD.Name = "MOD";
            graphGroup10.Groupings.Add(new Telerik.Reporting.Grouping("=(Fields.TOTAL_MOD_ASSIGNED - Fields.TOTAL_MOD_CHANGED) / (Fields.TOTAL_MOD_ASSIG" +
            "NED*1)"));
            graphGroup10.Name = "MOD";
            this.MOD.SeriesGroup = graphGroup10;
            this.MOD.ToolTip.Text = "=iif(Fields.TOTAL_MOD_ASSIGNED=0,0,Format(\'{0:P2}\',(Fields.TOTAL_MOD_ASSIGNED - F" +
    "ields.TOTAL_MOD_CHANGED) / (Fields.TOTAL_MOD_ASSIGNED*1)))";
            this.MOD.ToolTip.Title = "MOD %";
            this.MOD.Y = "=iif(Fields.TOTAL_MOD_ASSIGNED=0,0,(Fields.TOTAL_MOD_ASSIGNED - Fields.TOTAL_MOD_" +
    "CHANGED) / (Fields.TOTAL_MOD_ASSIGNED*1))";
            // 
            // graph2
            // 
            graphGroup11.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_APC_ASSIGNED"));
            graphGroup11.Name = "APC assigned";
            graphGroup12.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_APC_CHANGED"));
            graphGroup12.Name = "APC Change";
            this.graph2.CategoryGroups.Add(graphGroup11);
            this.graph2.CategoryGroups.Add(graphGroup12);
            this.graph2.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph2.DataSource = this.OPoverAllAccuracysql;
            this.graph2.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph2.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph2.Legend.Style.Visible = false;
            this.graph2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1001179218292236D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph2.Name = "graph2";
            this.graph2.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph2.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph2.Series.Add(this.barSeries1);
            this.graph2.Series.Add(this.barSeries2);
            this.graph2.SeriesGroups.Add(graphGroup13);
            this.graph2.SeriesGroups.Add(graphGroup14);
            this.graph2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle2.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle2.Style.Font.Bold = true;
            graphTitle2.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle2.Text = "APCs";
            this.graph2.Titles.Add(graphTitle2);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.XAxis = this.graphAxis10;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis9;
            // 
            // graphAxis10
            // 
            this.graphAxis10.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis10.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis10.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MajorGridLineStyle.Visible = false;
            this.graphAxis10.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis10.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MinorGridLineStyle.Visible = false;
            this.graphAxis10.Name = "graphAxis10";
            this.graphAxis10.Scale = categoryScale3;
            this.graphAxis10.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis10.Style.Visible = true;
            this.graphAxis10.Title = "     Assigned                       Changed";
            this.graphAxis10.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.AtMinimum;
            // 
            // graphAxis9
            // 
            this.graphAxis9.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis9.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MinorGridLineStyle.Visible = false;
            this.graphAxis9.Name = "graphAxis9";
            numericalScale2.Minimum = 0D;
            this.graphAxis9.Scale = numericalScale2;
            // 
            // barSeries1
            // 
            this.barSeries1.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries1.CategoryGroup = graphGroup11;
            colorPalette6.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries1.ColorPalette = colorPalette6;
            this.barSeries1.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries1.DataPointLabel = "= Sum(Fields.TOTAL_APC_ASSIGNED)";
            this.barSeries1.DataPointLabelStyle.Visible = false;
            this.barSeries1.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries1.DataPointStyle.Visible = true;
            this.barSeries1.LegendItem.Value = "= Fields.TOTAL_APC_ASSIGNED + \'/\' +  Fields.TOTAL_APC_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "APC_ASSIGNED)\'";
            this.barSeries1.Name = "barSeries1";
            graphGroup13.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_APC_ASSIGNED"));
            graphGroup13.Name = "APC Assigned";
            this.barSeries1.SeriesGroup = graphGroup13;
            this.barSeries1.ToolTip.Text = "= Sum(Fields.TOTAL_APC_ASSIGNED)";
            this.barSeries1.ToolTip.Title = "APC Assigned";
            this.barSeries1.Y = "= Sum(Fields.TOTAL_APC_ASSIGNED)";
            // 
            // barSeries2
            // 
            this.barSeries2.CategoryGroup = graphGroup12;
            colorPalette7.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries2.ColorPalette = colorPalette7;
            this.barSeries2.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries2.DataPointLabel = "= Sum(Fields.TOTAL_APC_CHANGED)";
            this.barSeries2.DataPointLabelStyle.Visible = false;
            this.barSeries2.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries2.DataPointStyle.Visible = true;
            this.barSeries2.LegendItem.Value = "= Fields.TOTAL_APC_ASSIGNED + \'/\' +  Fields.TOTAL_APC_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "APC_CHANGED)\'";
            this.barSeries2.Name = "barSeries2";
            graphGroup14.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_APC_CHANGED"));
            graphGroup14.Name = "APC Changed";
            this.barSeries2.SeriesGroup = graphGroup14;
            this.barSeries2.ToolTip.Text = "= Sum(Fields.TOTAL_APC_CHANGED)";
            this.barSeries2.ToolTip.Title = "APC Changed";
            this.barSeries2.Y = "= Sum(Fields.TOTAL_APC_CHANGED)";
            // 
            // graph5
            // 
            graphGroup15.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_ASSIGNED"));
            graphGroup15.Name = "CM Assigned";
            graphGroup16.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup16.Name = "CM Changed";
            this.graph5.CategoryGroups.Add(graphGroup15);
            this.graph5.CategoryGroups.Add(graphGroup16);
            this.graph5.CoordinateSystems.Add(this.cartesianCoordinateSystem5);
            this.graph5.DataSource = this.OPoverAllAccuracysql;
            this.graph5.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph5.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph5.Legend.Style.Visible = false;
            this.graph5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2001967430114746D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph5.Name = "graph5";
            this.graph5.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph5.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph5.Series.Add(this.barSeries7);
            this.graph5.Series.Add(this.barSeries8);
            this.graph5.SeriesGroups.Add(graphGroup17);
            this.graph5.SeriesGroups.Add(graphGroup18);
            this.graph5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle3.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle3.Style.Font.Bold = true;
            graphTitle3.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle3.Text = "ICD-10-CM";
            this.graph5.Titles.Add(graphTitle3);
            // 
            // cartesianCoordinateSystem5
            // 
            this.cartesianCoordinateSystem5.Name = "cartesianCoordinateSystem5";
            this.cartesianCoordinateSystem5.XAxis = this.graphAxis12;
            this.cartesianCoordinateSystem5.YAxis = this.graphAxis11;
            // 
            // graphAxis12
            // 
            this.graphAxis12.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis12.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis12.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis12.MajorGridLineStyle.Visible = false;
            this.graphAxis12.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis12.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis12.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis12.MinorGridLineStyle.Visible = false;
            this.graphAxis12.Name = "graphAxis12";
            this.graphAxis12.Scale = categoryScale4;
            this.graphAxis12.Title = "      Assigned                 Changed";
            this.graphAxis12.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.AtMinimum;
            // 
            // graphAxis11
            // 
            this.graphAxis11.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis11.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MinorGridLineStyle.Visible = false;
            this.graphAxis11.Name = "graphAxis11";
            numericalScale3.Minimum = 0D;
            this.graphAxis11.Scale = numericalScale3;
            // 
            // barSeries7
            // 
            this.barSeries7.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries7.CategoryGroup = graphGroup15;
            colorPalette8.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries7.ColorPalette = colorPalette8;
            this.barSeries7.CoordinateSystem = this.cartesianCoordinateSystem5;
            this.barSeries7.DataPointLabel = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            this.barSeries7.DataPointLabelStyle.Visible = false;
            this.barSeries7.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries7.DataPointStyle.Visible = true;
            this.barSeries7.LegendItem.Value = "= Fields.TOTAL_CM_ASSIGNED + \'/\' +  Fields.TOTAL_CM_CHANGED + \'/\' + \'Sum(TOTAL_CM" +
    "_ASSIGNED)\'";
            this.barSeries7.Name = "barSeries7";
            graphGroup17.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_ASSIGNED"));
            graphGroup17.Name = "cm assigned";
            this.barSeries7.SeriesGroup = graphGroup17;
            this.barSeries7.ToolTip.Text = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            this.barSeries7.ToolTip.Title = "CM Assigned";
            this.barSeries7.Y = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            // 
            // barSeries8
            // 
            this.barSeries8.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries8.CategoryGroup = graphGroup16;
            colorPalette9.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries8.ColorPalette = colorPalette9;
            this.barSeries8.CoordinateSystem = this.cartesianCoordinateSystem5;
            this.barSeries8.DataPointLabel = "= Sum(Fields.TOTAL_CM_CHANGED)";
            this.barSeries8.DataPointLabelStyle.Visible = false;
            this.barSeries8.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries8.DataPointStyle.Visible = true;
            this.barSeries8.LegendItem.Value = "= Fields.TOTAL_CM_ASSIGNED + \'/\' +  Fields.TOTAL_CM_CHANGED + \'/\' + \'Sum(TOTAL_CM" +
    "_CHANGED)\'";
            this.barSeries8.Name = "barSeries8";
            graphGroup18.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup18.Name = "cm  change";
            this.barSeries8.SeriesGroup = graphGroup18;
            this.barSeries8.ToolTip.Text = "= Sum(Fields.TOTAL_CM_CHANGED)";
            this.barSeries8.ToolTip.Title = "CM Changed";
            this.barSeries8.Y = "= Sum(Fields.TOTAL_CM_CHANGED)";
            // 
            // graph3
            // 
            graphGroup19.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup19.Name = "pcs Assigned";
            graphGroup20.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup20.Name = "PCS chnge";
            this.graph3.CategoryGroups.Add(graphGroup19);
            this.graph3.CategoryGroups.Add(graphGroup20);
            this.graph3.CoordinateSystems.Add(this.cartesianCoordinateSystem3);
            this.graph3.DataSource = this.OPoverAllAccuracysql;
            this.graph3.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph3.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph3.Legend.Style.Visible = false;
            this.graph3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(2.5001182556152344D));
            this.graph3.Name = "graph3";
            this.graph3.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph3.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph3.Series.Add(this.barSeries3);
            this.graph3.Series.Add(this.barSeries4);
            this.graph3.SeriesGroups.Add(graphGroup21);
            this.graph3.SeriesGroups.Add(graphGroup22);
            this.graph3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle4.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle4.Style.Font.Bold = true;
            graphTitle4.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle4.Text = "ICD-10-PCS";
            this.graph3.Titles.Add(graphTitle4);
            // 
            // cartesianCoordinateSystem3
            // 
            this.cartesianCoordinateSystem3.Name = "cartesianCoordinateSystem3";
            this.cartesianCoordinateSystem3.XAxis = this.graphAxis6;
            this.cartesianCoordinateSystem3.YAxis = this.graphAxis5;
            // 
            // graphAxis6
            // 
            this.graphAxis6.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis6.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MajorGridLineStyle.Visible = false;
            this.graphAxis6.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis6.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.Visible = false;
            this.graphAxis6.Name = "graphAxis6";
            this.graphAxis6.Scale = categoryScale5;
            this.graphAxis6.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis6.Title = "   Assigned                         Changed";
            this.graphAxis6.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.AtMinimum;
            // 
            // graphAxis5
            // 
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.Name = "graphAxis5";
            numericalScale4.Minimum = 0D;
            this.graphAxis5.Scale = numericalScale4;
            // 
            // barSeries3
            // 
            this.barSeries3.CategoryGroup = graphGroup19;
            colorPalette10.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries3.ColorPalette = colorPalette10;
            this.barSeries3.CoordinateSystem = this.cartesianCoordinateSystem3;
            this.barSeries3.DataPointLabel = "= Sum(Fields.TOTAL_PCS_ASSIGNED)";
            this.barSeries3.DataPointLabelStyle.Visible = false;
            this.barSeries3.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries3.DataPointStyle.Visible = true;
            this.barSeries3.LegendItem.Value = "= Fields.TOTAL_PCS_ASSIGNED + \'/\' +  Fields.TOTAL_PCS_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "PCS_ASSIGNED)\'";
            this.barSeries3.Name = "barSeries3";
            graphGroup21.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup21.Name = "pcs assigned";
            this.barSeries3.SeriesGroup = graphGroup21;
            this.barSeries3.ToolTip.Text = "= Sum(Fields.TOTAL_PCS_ASSIGNED)";
            this.barSeries3.ToolTip.Title = "PCs Assigned";
            this.barSeries3.Y = "= Sum(Fields.TOTAL_PCS_ASSIGNED)";
            // 
            // barSeries4
            // 
            this.barSeries4.CategoryGroup = graphGroup20;
            colorPalette11.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries4.ColorPalette = colorPalette11;
            this.barSeries4.CoordinateSystem = this.cartesianCoordinateSystem3;
            this.barSeries4.DataPointLabel = "= Sum(Fields.TOTAL_PCS_CHANGED)";
            this.barSeries4.DataPointLabelStyle.Visible = false;
            this.barSeries4.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries4.DataPointStyle.Visible = true;
            this.barSeries4.LegendItem.Value = "= Fields.TOTAL_PCS_ASSIGNED + \'/\' +  Fields.TOTAL_PCS_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "PCS_CHANGED)\'";
            this.barSeries4.Name = "barSeries4";
            graphGroup22.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup22.Name = "pcs chnge";
            this.barSeries4.SeriesGroup = graphGroup22;
            this.barSeries4.ToolTip.Text = "= Sum(Fields.TOTAL_PCS_CHANGED)";
            this.barSeries4.ToolTip.Title = "PCs Changed";
            this.barSeries4.Y = "= Sum(Fields.TOTAL_PCS_CHANGED)";
            // 
            // graph4
            // 
            graphGroup23.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CPT_ASSIGNED"));
            graphGroup23.Name = "CPT assigned";
            graphGroup24.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CPT_CHANGED"));
            graphGroup24.Name = "Cpt Changed";
            this.graph4.CategoryGroups.Add(graphGroup23);
            this.graph4.CategoryGroups.Add(graphGroup24);
            this.graph4.CoordinateSystems.Add(this.cartesianCoordinateSystem4);
            this.graph4.DataSource = this.OPoverAllAccuracysql;
            this.graph4.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph4.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph4.Legend.Style.Visible = false;
            this.graph4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1001179218292236D), Telerik.Reporting.Drawing.Unit.Inch(2.5001184940338135D));
            this.graph4.Name = "graph4";
            this.graph4.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph4.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph4.Series.Add(this.barSeries5);
            this.graph4.Series.Add(this.barSeries6);
            this.graph4.SeriesGroups.Add(graphGroup25);
            this.graph4.SeriesGroups.Add(graphGroup26);
            this.graph4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph4.Style.Font.Bold = false;
            this.graph4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.graph4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle5.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle5.Style.Font.Bold = true;
            graphTitle5.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle5.Text = "CPT/HCPCS Codes";
            this.graph4.Titles.Add(graphTitle5);
            // 
            // cartesianCoordinateSystem4
            // 
            this.cartesianCoordinateSystem4.Name = "cartesianCoordinateSystem4";
            this.cartesianCoordinateSystem4.XAxis = this.graphAxis8;
            this.cartesianCoordinateSystem4.YAxis = this.graphAxis7;
            // 
            // graphAxis8
            // 
            this.graphAxis8.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis8.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis8.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MajorGridLineStyle.Visible = false;
            this.graphAxis8.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis8.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.Visible = false;
            this.graphAxis8.Name = "graphAxis8";
            this.graphAxis8.Scale = categoryScale6;
            this.graphAxis8.Title = "           Assigned                  Changed";
            this.graphAxis8.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.AtMinimum;
            this.graphAxis8.TitleStyle.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.graphAxis8.TitleStyle.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // graphAxis7
            // 
            this.graphAxis7.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis7.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MinorGridLineStyle.Visible = false;
            this.graphAxis7.Name = "graphAxis7";
            numericalScale5.Minimum = 0D;
            this.graphAxis7.Scale = numericalScale5;
            this.graphAxis7.Title = "";
            // 
            // barSeries5
            // 
            this.barSeries5.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries5.CategoryGroup = graphGroup23;
            colorPalette12.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries5.ColorPalette = colorPalette12;
            this.barSeries5.CoordinateSystem = this.cartesianCoordinateSystem4;
            this.barSeries5.DataPointLabel = "= Sum(Fields.TOTAL_CPT_ASSIGNED)";
            this.barSeries5.DataPointLabelStyle.Visible = false;
            this.barSeries5.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries5.DataPointStyle.Visible = true;
            this.barSeries5.LegendItem.Value = "= Fields.TOTAL_CPT_ASSIGNED + \'/\' +  Fields.TOTAL_CPT_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "CPT_ASSIGNED)\'";
            this.barSeries5.Name = "barSeries5";
            graphGroup25.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CPT_ASSIGNED"));
            graphGroup25.Name = "Cpt assigned";
            this.barSeries5.SeriesGroup = graphGroup25;
            this.barSeries5.ToolTip.Text = "= Sum(Fields.TOTAL_CPT_ASSIGNED)";
            this.barSeries5.ToolTip.Title = "CPT Assigned\r\n";
            this.barSeries5.Y = "= Sum(Fields.TOTAL_CPT_ASSIGNED)";
            // 
            // barSeries6
            // 
            this.barSeries6.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries6.CategoryGroup = graphGroup24;
            colorPalette13.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries6.ColorPalette = colorPalette13;
            this.barSeries6.CoordinateSystem = this.cartesianCoordinateSystem4;
            this.barSeries6.DataPointLabel = "= Sum(Fields.TOTAL_CPT_CHANGED)";
            this.barSeries6.DataPointLabelStyle.Visible = false;
            this.barSeries6.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries6.DataPointStyle.Visible = true;
            this.barSeries6.LegendItem.Value = "= Fields.TOTAL_CPT_ASSIGNED + \'/\' +  Fields.TOTAL_CPT_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "CPT_CHANGED)\'";
            this.barSeries6.Name = "barSeries6";
            graphGroup26.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CPT_CHANGED"));
            graphGroup26.Name = "Cpt changed";
            this.barSeries6.SeriesGroup = graphGroup26;
            this.barSeries6.ToolTip.Text = "= Sum(Fields.TOTAL_CPT_CHANGED)";
            this.barSeries6.ToolTip.Title = "CPT Changed";
            this.barSeries6.Y = "= Sum(Fields.TOTAL_CPT_CHANGED)";
            // 
            // graph6
            // 
            graphGroup27.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_MOD_ASSIGNED"));
            graphGroup27.Name = "assogned";
            graphGroup28.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_MOD_CHANGED"));
            graphGroup28.Name = "chnaged";
            this.graph6.CategoryGroups.Add(graphGroup27);
            this.graph6.CategoryGroups.Add(graphGroup28);
            this.graph6.CoordinateSystems.Add(this.cartesianCoordinateSystem6);
            this.graph6.DataSource = this.OPoverAllAccuracysql;
            this.graph6.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph6.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph6.Legend.Style.Visible = false;
            this.graph6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2001967430114746D), Telerik.Reporting.Drawing.Unit.Inch(2.5001184940338135D));
            this.graph6.Name = "graph6";
            this.graph6.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph6.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph6.Series.Add(this.barSeries9);
            this.graph6.Series.Add(this.barSeries10);
            this.graph6.SeriesGroups.Add(graphGroup29);
            this.graph6.SeriesGroups.Add(graphGroup30);
            this.graph6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle6.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle6.Style.Font.Bold = true;
            graphTitle6.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle6.Text = "Modifiers";
            this.graph6.Titles.Add(graphTitle6);
            // 
            // cartesianCoordinateSystem6
            // 
            this.cartesianCoordinateSystem6.Name = "cartesianCoordinateSystem6";
            this.cartesianCoordinateSystem6.XAxis = this.graphAxis14;
            this.cartesianCoordinateSystem6.YAxis = this.graphAxis13;
            // 
            // graphAxis14
            // 
            this.graphAxis14.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis14.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis14.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MajorGridLineStyle.Visible = false;
            this.graphAxis14.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis14.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MinorGridLineStyle.Visible = false;
            this.graphAxis14.Name = "graphAxis14";
            this.graphAxis14.Scale = categoryScale7;
            this.graphAxis14.Title = "Assigned        Changed";
            // 
            // graphAxis13
            // 
            this.graphAxis13.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis13.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MinorGridLineStyle.Visible = false;
            this.graphAxis13.Name = "graphAxis13";
            numericalScale6.Minimum = 0D;
            this.graphAxis13.Scale = numericalScale6;
            // 
            // barSeries9
            // 
            this.barSeries9.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries9.CategoryGroup = graphGroup27;
            colorPalette14.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries9.ColorPalette = colorPalette14;
            this.barSeries9.CoordinateSystem = this.cartesianCoordinateSystem6;
            this.barSeries9.DataPointLabel = "= Sum(Fields.TOTAL_MOD_ASSIGNED)";
            this.barSeries9.DataPointLabelStyle.Visible = false;
            this.barSeries9.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries9.DataPointStyle.Visible = true;
            this.barSeries9.LegendItem.Value = "= Fields.TOTAL_MOD_ASSIGNED + \'/\' +  Fields.TOTAL_MOD_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "MOD_ASSIGNED)\'";
            this.barSeries9.Name = "barSeries9";
            graphGroup29.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_MOD_ASSIGNED"));
            graphGroup29.Name = "Series assigned";
            this.barSeries9.SeriesGroup = graphGroup29;
            this.barSeries9.ToolTip.Text = "= Sum(Fields.TOTAL_MOD_ASSIGNED)";
            this.barSeries9.ToolTip.Title = "Mod Assigned";
            this.barSeries9.Y = "= Sum(Fields.TOTAL_MOD_ASSIGNED)";
            // 
            // barSeries10
            // 
            this.barSeries10.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            this.barSeries10.CategoryGroup = graphGroup28;
            colorPalette15.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries10.ColorPalette = colorPalette15;
            this.barSeries10.CoordinateSystem = this.cartesianCoordinateSystem6;
            this.barSeries10.DataPointLabel = "= Sum(Fields.TOTAL_MOD_CHANGED)";
            this.barSeries10.DataPointLabelStyle.Visible = false;
            this.barSeries10.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries10.DataPointStyle.Visible = true;
            this.barSeries10.LegendItem.Value = "= Fields.TOTAL_MOD_ASSIGNED + \'/\' +  Fields.TOTAL_MOD_CHANGED + \'/\' + \'Sum(TOTAL_" +
    "MOD_CHANGED)\'";
            this.barSeries10.Name = "barSeries10";
            graphGroup30.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_MOD_CHANGED"));
            graphGroup30.Name = "Series Changed";
            this.barSeries10.SeriesGroup = graphGroup30;
            this.barSeries10.ToolTip.Text = "= Sum(Fields.TOTAL_MOD_CHANGED)";
            this.barSeries10.ToolTip.Title = "Mod Changed";
            this.barSeries10.Y = "= Sum(Fields.TOTAL_MOD_CHANGED)";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // graphAxis1
            // 
            this.graphAxis1.LabelFormat = "{0:P0}";
            this.graphAxis1.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.Name = "graphAxis1";
            numericalScale7.Minimum = 0D;
            this.graphAxis1.Scale = numericalScale7;
            // 
            // OutpatientOverallAccuracy
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "OutpatientOverallAccuracy";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "2";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "= AllDistinctValues(Fields.CHART_TYPE_KEY)";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "= AllDistinctValues(Fields.CRA_PERSONNEL_ID)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.Value = "3655";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartTypeName";
            reportParameter6.Value = "-1";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "IcdType";
            reportParameter7.Value = "2";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "Startdate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "08-05-2002";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "EndDate";
            reportParameter9.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter9.Value = "08-05-2017";
            reportParameter9.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(10.212544441223145D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource OPoverAllAccuracysql;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.BarSeries APC;
        private Telerik.Reporting.BarSeries CM;
        private Telerik.Reporting.BarSeries PCs;
        private Telerik.Reporting.BarSeries CPT;
        private Telerik.Reporting.BarSeries MOD;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem2;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.Graph graph2;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis10;
        private Telerik.Reporting.GraphAxis graphAxis9;
        private Telerik.Reporting.BarSeries barSeries1;
        private Telerik.Reporting.BarSeries barSeries2;
        private Telerik.Reporting.Graph graph5;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem5;
        private Telerik.Reporting.GraphAxis graphAxis12;
        private Telerik.Reporting.GraphAxis graphAxis11;
        private Telerik.Reporting.BarSeries barSeries7;
        private Telerik.Reporting.BarSeries barSeries8;
        private Telerik.Reporting.Graph graph3;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem3;
        private Telerik.Reporting.GraphAxis graphAxis6;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.BarSeries barSeries3;
        private Telerik.Reporting.BarSeries barSeries4;
        private Telerik.Reporting.Graph graph4;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem4;
        private Telerik.Reporting.GraphAxis graphAxis8;
        private Telerik.Reporting.GraphAxis graphAxis7;
        private Telerik.Reporting.BarSeries barSeries5;
        private Telerik.Reporting.BarSeries barSeries6;
        private Telerik.Reporting.Graph graph6;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem6;
        private Telerik.Reporting.GraphAxis graphAxis14;
        private Telerik.Reporting.GraphAxis graphAxis13;
        private Telerik.Reporting.BarSeries barSeries9;
        private Telerik.Reporting.BarSeries barSeries10;
        private Telerik.Reporting.TextBox textBox4;
    }
}