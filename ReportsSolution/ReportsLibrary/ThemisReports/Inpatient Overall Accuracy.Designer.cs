namespace ReportsLibrary.ThemisReports
{
    partial class Inpatient_Overall_Accuracy
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.GraphAxis graphAxis2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inpatient_Overall_Accuracy));
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition1 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.GraphGroup graphGroup23 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup24 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup25 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup32 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup33 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup34 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle4 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale4 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup26 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette7 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup27 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup28 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette8 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup29 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup30 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette9 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup31 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.ToggleVisibilityAction toggleVisibilityAction1 = new Telerik.Reporting.ToggleVisibilityAction();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette1 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette2 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup7 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup8 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup13 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup14 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle2 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale3 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition2 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup9 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette3 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup10 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup11 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette4 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup12 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup15 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup16 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup21 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup22 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle3 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale4 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup17 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette5 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup18 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup19 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette6 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup20 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.InpatientOverallAccSql = new Telerik.Reporting.SqlDataSource();
            this.BMSdrg = new Telerik.Reporting.BarSeries();
            this.BCM = new Telerik.Reporting.BarSeries();
            this.BPCS = new Telerik.Reporting.BarSeries();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.graph3 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem3 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis6 = new Telerik.Reporting.GraphAxis();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.barSeries3 = new Telerik.Reporting.BarSeries();
            this.barSeries4 = new Telerik.Reporting.BarSeries();
            this.graph4 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem4 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis8 = new Telerik.Reporting.GraphAxis();
            this.graphAxis7 = new Telerik.Reporting.GraphAxis();
            this.barSeries5 = new Telerik.Reporting.BarSeries();
            this.barSeries6 = new Telerik.Reporting.BarSeries();
            this.graph7 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem7 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis13 = new Telerik.Reporting.GraphAxis();
            this.graphAxis14 = new Telerik.Reporting.GraphAxis();
            this.barSeries7 = new Telerik.Reporting.BarSeries();
            this.barSeries8 = new Telerik.Reporting.BarSeries();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            graphAxis2 = new Telerik.Reporting.GraphAxis();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // graphAxis2
            // 
            graphAxis2.LabelAngle = 45;
            graphAxis2.LabelFormat = "{0:P0}";
            graphAxis2.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            graphAxis2.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis2.MajorGridLineStyle.Visible = false;
            graphAxis2.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis2.MinorGridLineStyle.Visible = false;
            graphAxis2.Name = resources.GetString("graphAxis2.Name");
            categoryScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.Auto;
            categoryScale1.CrossAxisPositions.Add(categoryScaleCrossAxisPosition1);
            graphAxis2.Scale = categoryScale1;
            graphAxis2.Title = "MS-DRG       CM          PCS";
            graphAxis2.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.Auto;
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52083450555801392D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox19.StyleName = "Apex.TableHeader";
            this.textBox19.Value = "Total Charts";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.675005316734314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "MS-DRGs Assigned";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69583356380462646D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "MS-DRGs Changed";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7187458872795105D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.StyleName = "Apex.TableHeader";
            this.textBox5.Value = "MS-DRGs Accuracy";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65416735410690308D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "CM Codes Assigned";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65416598320007324D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox4.StyleName = "Apex.TableHeader";
            this.textBox4.Value = "CM Codes Changed";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6666642427444458D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox13.StyleName = "Apex.TableHeader";
            this.textBox13.Value = "CM Accuracy";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.654171884059906D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox7.StyleName = "Apex.TableHeader";
            this.textBox7.Value = "PCS Codes Assigned";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.56041818857192993D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox8.StyleName = "Apex.TableHeader";
            this.textBox8.Value = "PCS Codes Changed";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69791519641876221D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox21.StyleName = "Apex.TableHeader";
            this.textBox21.Value = "PCS Accuracy";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\',Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(5.4002366065979D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1});
            this.detailSection1.Name = "detailSection1";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.graph3,
            this.graph4,
            this.graph7,
            this.table1,
            this.graph1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(5.4001975059509277D));
            // 
            // graph1
            // 
            graphGroup23.Groupings.Add(new Telerik.Reporting.Grouping("=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
            "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
            "lds.TOTAL_ASSIGNED_DRG * 1)), 0)"));
            graphGroup23.Name = "msdrgGROUP";
            graphGroup24.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1.0))"));
            graphGroup24.Name = "cMGROUP";
            graphGroup25.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSI" +
            "GNED * 1.0))"));
            graphGroup25.Name = "PCsGroup";
            this.graph1.CategoryGroups.Add(graphGroup23);
            this.graph1.CategoryGroups.Add(graphGroup24);
            this.graph1.CategoryGroups.Add(graphGroup25);
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph1.DataSource = this.InpatientOverallAccSql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0999605655670166D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.BMSdrg);
            this.graph1.Series.Add(this.BCM);
            this.graph1.Series.Add(this.BPCS);
            graphGroup32.Groupings.Add(new Telerik.Reporting.Grouping("=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
            "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
            "lds.TOTAL_ASSIGNED_DRG * 1)), 0)\r\n"));
            graphGroup32.Name = "MSDRGGroup";
            graphGroup33.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1.0))"));
            graphGroup33.Name = "CMsGroup";
            graphGroup34.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSI" +
            "GNED * 1.0))"));
            graphGroup34.Name = "PCsGroup";
            this.graph1.SeriesGroups.Add(graphGroup32);
            this.graph1.SeriesGroups.Add(graphGroup33);
            this.graph1.SeriesGroups.Add(graphGroup34);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle4.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle4.Style.Font.Bold = true;
            graphTitle4.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            graphTitle4.Text = "ICD-10 Coding Accuracy";
            this.graph1.Titles.Add(graphTitle4);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.cartesianCoordinateSystem1.XAxis = graphAxis2;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis1;
            // 
            // graphAxis1
            // 
            this.graphAxis1.LabelFormat = "{0:P0}";
            this.graphAxis1.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.Name = "graphAxis1";
            numericalScale4.Minimum = 0D;
            this.graphAxis1.Scale = numericalScale4;
            // 
            // InpatientOverallAccSql
            // 
            this.InpatientOverallAccSql.CommandTimeout = 0;
            this.InpatientOverallAccSql.ConnectionString = "THEMISConnectionString";
            this.InpatientOverallAccSql.Name = "InpatientOverallAccSql";
            this.InpatientOverallAccSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.InpatientOverallAccSql.SelectCommand = "usp_Metis_OverallAccuracy_Select";
            this.InpatientOverallAccSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // BMSdrg
            // 
            this.BMSdrg.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup26.Groupings.Add(new Telerik.Reporting.Grouping("=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
            "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
            "lds.TOTAL_ASSIGNED_DRG * 1)), 0)"));
            graphGroup26.Name = "msdrgGROUP";
            this.BMSdrg.CategoryGroup = graphGroup26;
            colorPalette7.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.BMSdrg.ColorPalette = colorPalette7;
            this.BMSdrg.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.BMSdrg.DataPointLabel = "MSDrg";
            this.BMSdrg.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BMSdrg.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BMSdrg.DataPointLabelStyle.Visible = false;
            this.BMSdrg.DataPointStyle.Visible = true;
            this.BMSdrg.LegendItem.Style.Visible = false;
            this.BMSdrg.Name = "BMSdrg";
            graphGroup27.Groupings.Add(new Telerik.Reporting.Grouping("=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
            "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
            "lds.TOTAL_ASSIGNED_DRG * 1)), 0)\r\n"));
            graphGroup27.Name = "MSDRGGroup";
            this.BMSdrg.SeriesGroup = graphGroup27;
            this.BMSdrg.ToolTip.Text = resources.GetString("BMSdrg.ToolTip.Text");
            this.BMSdrg.ToolTip.Title = "MSDRG%";
            this.BMSdrg.Y = "=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
    "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
    "lds.TOTAL_ASSIGNED_DRG * 1)), 0)";
            // 
            // BCM
            // 
            this.BCM.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup28.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1.0))"));
            graphGroup28.Name = "cMGROUP";
            this.BCM.CategoryGroup = graphGroup28;
            colorPalette8.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.BCM.ColorPalette = colorPalette8;
            this.BCM.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.BCM.DataPointLabel = "CM";
            this.BCM.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BCM.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BCM.DataPointLabelFormat = "{0:P0}";
            this.BCM.DataPointLabelStyle.Visible = false;
            this.BCM.DataPointStyle.Visible = true;
            this.BCM.LegendItem.Style.Visible = false;
            this.BCM.Name = "BCM";
            graphGroup29.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED" +
            " * 1.0))"));
            graphGroup29.Name = "CMsGroup";
            this.BCM.SeriesGroup = graphGroup29;
            this.BCM.ToolTip.Text = "= iif(Fields.TOTAL_CM_ASSIGNED=0,0,Format(\'{0:N2}\',((Fields.TOTAL_CM_ASSIGNED- Fi" +
    "elds.TOTAL_CM_CHANGED) / (Fields.TOTAL_CM_ASSIGNED * 1.0))*100))";
            this.BCM.ToolTip.Title = "CM%";
            this.BCM.Y = "=iif(Fields.TOTAL_CM_ASSIGNED=0,0,((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHA" +
    "NGED) / (Fields.TOTAL_CM_ASSIGNED * 1)))";
            // 
            // BPCS
            // 
            this.BPCS.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup30.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSI" +
            "GNED * 1.0))"));
            graphGroup30.Name = "PCsGroup";
            this.BPCS.CategoryGroup = graphGroup30;
            colorPalette9.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(173)))), ((int)(((byte)(76))))));
            this.BPCS.ColorPalette = colorPalette9;
            this.BPCS.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.BPCS.DataPointLabel = "PCS";
            this.BPCS.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BPCS.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.BPCS.DataPointLabelFormat = "{0:P0}";
            this.BPCS.DataPointLabelStyle.Visible = false;
            this.BPCS.DataPointStyle.Visible = true;
            this.BPCS.LegendItem.Style.Visible = false;
            this.BPCS.Name = "BPCS";
            graphGroup31.Groupings.Add(new Telerik.Reporting.Grouping("=((Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSI" +
            "GNED * 1.0))"));
            graphGroup31.Name = "PCsGroup";
            this.BPCS.SeriesGroup = graphGroup31;
            this.BPCS.ToolTip.Text = "=iif(Fields.TOTAL_PCS_ASSIGNED=0,0,Format(\'{0:N2}\',((Fields.TOTAL_PCS_ASSIGNED - " +
    "Fields.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSIGNED * 1))*100))";
            this.BPCS.ToolTip.Title = "PCS%";
            this.BPCS.Y = "=iif(Fields.TOTAL_PCS_ASSIGNED=0,0,((Fields.TOTAL_PCS_ASSIGNED - Fields.TOTAL_PCS" +
    "_CHANGED) / (Fields.TOTAL_PCS_ASSIGNED * 1)))";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.52083462476730347D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.67500519752502441D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.69583350419998169D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.7187458872795105D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.654167115688324D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.654166042804718D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.66666430234909058D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.65417182445526123D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.56041812896728516D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.69791519641876221D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table1.Body.SetCellContent(0, 1, this.textBox9);
            this.table1.Body.SetCellContent(0, 2, this.textBox10);
            this.table1.Body.SetCellContent(0, 4, this.textBox11);
            this.table1.Body.SetCellContent(0, 5, this.textBox12);
            this.table1.Body.SetCellContent(0, 7, this.textBox15);
            this.table1.Body.SetCellContent(0, 8, this.textBox16);
            this.table1.Body.SetCellContent(0, 0, this.textBox20);
            this.table1.Body.SetCellContent(0, 3, this.textBox6);
            this.table1.Body.SetCellContent(0, 6, this.textBox14);
            this.table1.Body.SetCellContent(0, 9, this.textBox22);
            tableGroup1.Name = "group";
            tableGroup1.ReportItem = this.textBox19;
            tableGroup2.Name = "tOTAL_ASSIGNED_DRG";
            tableGroup2.ReportItem = this.textBox1;
            tableGroup3.Name = "tOTAL_CHANGED_DRG";
            tableGroup3.ReportItem = this.textBox2;
            tableGroup4.Name = "group1";
            tableGroup4.ReportItem = this.textBox5;
            tableGroup5.Name = "tOTAL_CM_ASSIGNED";
            tableGroup5.ReportItem = this.textBox3;
            tableGroup6.Name = "tOTAL_CM_CHANGED";
            tableGroup6.ReportItem = this.textBox4;
            tableGroup7.Name = "group2";
            tableGroup7.ReportItem = this.textBox13;
            tableGroup8.Name = "tOTAL_PCS_ASSIGNED";
            tableGroup8.ReportItem = this.textBox7;
            tableGroup9.Name = "tOTAL_PCS_CHANGED";
            tableGroup9.ReportItem = this.textBox8;
            tableGroup10.Name = "group3";
            tableGroup10.ReportItem = this.textBox21;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.DataSource = this.InpatientOverallAccSql;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20,
            this.textBox9,
            this.textBox10,
            this.textBox6,
            this.textBox11,
            this.textBox12,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox22,
            this.textBox19,
            this.textBox1,
            this.textBox2,
            this.textBox5,
            this.textBox3,
            this.textBox4,
            this.textBox13,
            this.textBox7,
            this.textBox8,
            this.textBox21});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(5.0001969337463379D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.Name = "detail";
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.RowHeadersPrintOnEveryPage = true;
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4979219436645508D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table1.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.675005316734314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox9.StyleName = "Apex.TableBody";
            this.textBox9.Value = "= Fields.TOTAL_ASSIGNED_DRG";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69583356380462646D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox10.StyleName = "Apex.TableBody";
            this.textBox10.Value = "= Fields.TOTAL_CHANGED_DRG";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65416735410690308D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox11.StyleName = "Apex.TableBody";
            this.textBox11.Value = "= Fields.TOTAL_CM_ASSIGNED";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65416598320007324D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox12.StyleName = "Apex.TableBody";
            this.textBox12.Value = "= Fields.TOTAL_CM_CHANGED";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.654171884059906D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox15.StyleName = "Apex.TableBody";
            this.textBox15.Value = "= Fields.TOTAL_PCS_ASSIGNED";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.56041818857192993D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox16.StyleName = "Apex.TableBody";
            this.textBox16.Value = "= Fields.TOTAL_PCS_CHANGED";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52083450555801392D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox20.StyleName = "Apex.TableBody";
            this.textBox20.Value = "=Fields.TOTAL_ASSIGNED_DRG";
            // 
            // textBox6
            // 
            this.textBox6.Format = "{0:P2}";
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7187458872795105D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox6.StyleName = "Apex.TableBody";
            this.textBox6.Value = "=IIF((Fields.TOTAL_ASSIGNED_DRG > 0 And (Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL" +
    "_CHANGED_DRG)>0), ((Fields.TOTAL_ASSIGNED_DRG - Fields.TOTAL_CHANGED_DRG) / (Fie" +
    "lds.TOTAL_ASSIGNED_DRG * 1)), 0)";
            // 
            // textBox14
            // 
            this.textBox14.Format = "{0:P2}";
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6666642427444458D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox14.StyleName = "Apex.TableBody";
            this.textBox14.Value = "=iif(Fields.TOTAL_CM_ASSIGNED=0,0,((Fields.TOTAL_CM_ASSIGNED- Fields.TOTAL_CM_CHA" +
    "NGED) / (Fields.TOTAL_CM_ASSIGNED * 1.0)))";
            // 
            // textBox22
            // 
            this.textBox22.Format = "{0:P2}";
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69791519641876221D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox22.StyleName = "Apex.TableBody";
            this.textBox22.Value = "=iif(CDbl(Fields.TOTAL_PCS_ASSIGNED)=0,0,CDbl(((Fields.TOTAL_PCS_ASSIGNED - Field" +
    "s.TOTAL_PCS_CHANGED) / (Fields.TOTAL_PCS_ASSIGNED * 1))))";
            // 
            // graph3
            // 
            toggleVisibilityAction1.DisplayExpandedMark = false;
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup2.Name = "tOTAL_CM_CHANGEDGroup";
            graphGroup2.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_CHANGED", Telerik.Reporting.SortDirection.Asc));
            graphGroup2.Visible = false;
            toggleVisibilityAction1.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            graphGroup2});
            graphGroup1.Action = toggleVisibilityAction1;
            graphGroup1.ChildGroups.Add(graphGroup2);
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_ASSIGNED"));
            graphGroup1.Name = "tOTAL_CM_ASSIGNEDGroup";
            graphGroup1.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_ASSIGNED", Telerik.Reporting.SortDirection.Asc));
            this.graph3.CategoryGroups.Add(graphGroup1);
            this.graph3.CoordinateSystems.Add(this.cartesianCoordinateSystem3);
            this.graph3.DataSource = this.InpatientOverallAccSql;
            this.graph3.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph3.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph3.Legend.Style.Visible = false;
            this.graph3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0999605655670166D), Telerik.Reporting.Drawing.Unit.Inch(2.5001182556152344D));
            this.graph3.Name = "graph3";
            this.graph3.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph3.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph3.Series.Add(this.barSeries3);
            this.graph3.Series.Add(this.barSeries4);
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup6.Name = "tOTAL_CM_CHANGEDGroup1";
            graphGroup6.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_CHANGED", Telerik.Reporting.SortDirection.Asc));
            graphGroup5.ChildGroups.Add(graphGroup6);
            graphGroup5.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_ASSIGNED"));
            graphGroup5.Name = "tOTAL_CM_ASSIGNEDGroup1";
            graphGroup5.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_ASSIGNED", Telerik.Reporting.SortDirection.Asc));
            this.graph3.SeriesGroups.Add(graphGroup5);
            this.graph3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.Font.Bold = true;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle1.Text = "ICD-10-CM";
            this.graph3.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem3
            // 
            this.cartesianCoordinateSystem3.Name = "cartesianCoordinateSystem3";
            this.cartesianCoordinateSystem3.XAxis = this.graphAxis6;
            this.cartesianCoordinateSystem3.YAxis = this.graphAxis5;
            // 
            // graphAxis6
            // 
            this.graphAxis6.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis6.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis6.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis6.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.Visible = false;
            this.graphAxis6.Name = "graphAxis6";
            this.graphAxis6.Scale = categoryScale2;
            this.graphAxis6.Title = "Assigned     Changed";
            // 
            // graphAxis5
            // 
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis5.Name = "graphAxis5";
            numericalScale1.Minimum = 0D;
            this.graphAxis5.Scale = numericalScale1;
            // 
            // barSeries3
            // 
            graphGroup3.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup3.Name = "tOTAL_CM_CHANGEDGroup";
            graphGroup3.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_CHANGED", Telerik.Reporting.SortDirection.Asc));
            graphGroup3.Visible = false;
            this.barSeries3.CategoryGroup = graphGroup3;
            colorPalette1.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries3.ColorPalette = colorPalette1;
            this.barSeries3.CoordinateSystem = this.cartesianCoordinateSystem3;
            this.barSeries3.DataPointLabel = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            this.barSeries3.DataPointLabelStyle.Visible = false;
            this.barSeries3.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries3.DataPointStyle.Visible = true;
            this.barSeries3.LegendItem.Value = "= Fields.TOTAL_CM_ASSIGNED + \'/\' +  Fields.TOTAL_CM_CHANGED + \'/\' + \'Sum(TOTAL_CM" +
    "_ASSIGNED)\'";
            this.barSeries3.Name = "barSeries3";
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CM_CHANGED"));
            graphGroup4.Name = "tOTAL_CM_CHANGEDGroup1";
            graphGroup4.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.TOTAL_CM_CHANGED", Telerik.Reporting.SortDirection.Asc));
            this.barSeries3.SeriesGroup = graphGroup4;
            this.barSeries3.ToolTip.Text = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            this.barSeries3.ToolTip.Title = " CM Assigned";
            this.barSeries3.Y = "= Sum(Fields.TOTAL_CM_ASSIGNED)";
            // 
            // barSeries4
            // 
            this.barSeries4.CategoryGroup = graphGroup3;
            colorPalette2.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries4.ColorPalette = colorPalette2;
            this.barSeries4.CoordinateSystem = this.cartesianCoordinateSystem3;
            this.barSeries4.DataPointLabel = "= Sum(Fields.TOTAL_CM_CHANGED)";
            this.barSeries4.DataPointLabelStyle.Visible = false;
            this.barSeries4.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries4.DataPointStyle.Visible = true;
            this.barSeries4.LegendItem.Value = "= Fields.TOTAL_CM_ASSIGNED + \'/\' +  Fields.TOTAL_CM_CHANGED + \'/\' + \'Sum(TOTAL_CM" +
    "_CHANGED)\'";
            this.barSeries4.Name = "barSeries4";
            this.barSeries4.SeriesGroup = graphGroup4;
            this.barSeries4.ToolTip.Text = "= Sum(Fields.TOTAL_CM_CHANGED)";
            this.barSeries4.ToolTip.Title = "CM Changed";
            this.barSeries4.Y = "= Sum(Fields.TOTAL_CM_CHANGED)";
            // 
            // graph4
            // 
            graphGroup7.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup7.Name = "graphGroup";
            graphGroup8.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup8.Name = "graphGroup1";
            this.graph4.CategoryGroups.Add(graphGroup7);
            this.graph4.CategoryGroups.Add(graphGroup8);
            this.graph4.CoordinateSystems.Add(this.cartesianCoordinateSystem4);
            this.graph4.DataSource = this.InpatientOverallAccSql;
            this.graph4.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph4.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph4.Legend.Style.Visible = false;
            this.graph4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.39996075630188D), Telerik.Reporting.Drawing.Unit.Inch(2.5001184940338135D));
            this.graph4.Name = "graph4";
            this.graph4.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph4.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph4.Series.Add(this.barSeries5);
            this.graph4.Series.Add(this.barSeries6);
            graphGroup13.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup13.Name = "graphGroup2";
            graphGroup14.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup14.Name = "graphGroup3";
            this.graph4.SeriesGroups.Add(graphGroup13);
            this.graph4.SeriesGroups.Add(graphGroup14);
            this.graph4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle2.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle2.Style.Font.Bold = true;
            graphTitle2.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle2.Text = "ICD-10-PCS";
            this.graph4.Titles.Add(graphTitle2);
            // 
            // cartesianCoordinateSystem4
            // 
            this.cartesianCoordinateSystem4.Name = "cartesianCoordinateSystem4";
            this.cartesianCoordinateSystem4.XAxis = this.graphAxis8;
            this.cartesianCoordinateSystem4.YAxis = this.graphAxis7;
            // 
            // graphAxis8
            // 
            this.graphAxis8.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis8.MajorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            this.graphAxis8.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis8.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.Visible = false;
            this.graphAxis8.Name = "graphAxis8";
            categoryScaleCrossAxisPosition2.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.Auto;
            categoryScale3.CrossAxisPositions.Add(categoryScaleCrossAxisPosition2);
            this.graphAxis8.Scale = categoryScale3;
            this.graphAxis8.Style.Visible = true;
            this.graphAxis8.Title = "   Assigned            Changed";
            this.graphAxis8.TitlePlacement = Telerik.Reporting.GraphAxisTitlePlacement.Centered;
            // 
            // graphAxis7
            // 
            this.graphAxis7.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.White;
            this.graphAxis7.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis7.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MinorGridLineStyle.Visible = false;
            this.graphAxis7.Name = "graphAxis7";
            numericalScale2.Minimum = 0D;
            this.graphAxis7.Scale = numericalScale2;
            this.graphAxis7.Style.LineColor = System.Drawing.Color.Black;
            // 
            // barSeries5
            // 
            this.barSeries5.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup9.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup9.Name = "graphGroup";
            this.barSeries5.CategoryGroup = graphGroup9;
            colorPalette3.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries5.ColorPalette = colorPalette3;
            this.barSeries5.CoordinateSystem = this.cartesianCoordinateSystem4;
            this.barSeries5.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries5.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            graphGroup10.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_ASSIGNED"));
            graphGroup10.Name = "graphGroup2";
            this.barSeries5.SeriesGroup = graphGroup10;
            this.barSeries5.ToolTip.Text = "= Sum(Fields.TOTAL_PCS_ASSIGNED)";
            this.barSeries5.ToolTip.Title = "PC Assigned";
            this.barSeries5.Y = "= Sum(Fields.TOTAL_PCS_ASSIGNED)";
            // 
            // barSeries6
            // 
            this.barSeries6.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup11.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup11.Name = "graphGroup1";
            this.barSeries6.CategoryGroup = graphGroup11;
            colorPalette4.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries6.ColorPalette = colorPalette4;
            this.barSeries6.CoordinateSystem = this.cartesianCoordinateSystem4;
            this.barSeries6.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries6.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            graphGroup12.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_PCS_CHANGED"));
            graphGroup12.Name = "graphGroup3";
            this.barSeries6.SeriesGroup = graphGroup12;
            this.barSeries6.ToolTip.Text = "=Sum(Fields.TOTAL_PCS_CHANGED)";
            this.barSeries6.ToolTip.Title = "PC Changed";
            this.barSeries6.Y = "= Sum(Fields.TOTAL_PCS_CHANGED)";
            // 
            // graph7
            // 
            graphGroup15.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.TOTAL_ASSIGNED_DRG"));
            graphGroup15.Name = "assignedGROUP";
            graphGroup16.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CHANGED_DRG"));
            graphGroup16.Name = "chngeGROUP";
            this.graph7.CategoryGroups.Add(graphGroup15);
            this.graph7.CategoryGroups.Add(graphGroup16);
            this.graph7.CoordinateSystems.Add(this.cartesianCoordinateSystem7);
            this.graph7.DataSource = this.InpatientOverallAccSql;
            this.graph7.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph7.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.39996075630188D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph7.Name = "graph7";
            this.graph7.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph7.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph7.Series.Add(this.barSeries7);
            this.graph7.Series.Add(this.barSeries8);
            graphGroup21.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_ASSIGNED_DRG"));
            graphGroup21.Name = "AsgGroup";
            graphGroup22.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CHANGED_DRG"));
            graphGroup22.Name = "chnGroup";
            this.graph7.SeriesGroups.Add(graphGroup21);
            this.graph7.SeriesGroups.Add(graphGroup22);
            this.graph7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.graph7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle3.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle3.Style.Font.Bold = true;
            graphTitle3.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            graphTitle3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            graphTitle3.Text = "ICD-10 MS-DRG";
            this.graph7.Titles.Add(graphTitle3);
            // 
            // cartesianCoordinateSystem7
            // 
            this.cartesianCoordinateSystem7.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem7.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.cartesianCoordinateSystem7.XAxis = this.graphAxis13;
            this.cartesianCoordinateSystem7.YAxis = this.graphAxis14;
            // 
            // graphAxis13
            // 
            this.graphAxis13.LabelFormat = "";
            this.graphAxis13.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis13.MajorGridLineStyle.BackgroundColor = System.Drawing.Color.Transparent;
            this.graphAxis13.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MajorGridLineStyle.Visible = false;
            this.graphAxis13.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis13.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis13.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis13.MinorGridLineStyle.Visible = false;
            this.graphAxis13.Name = "graphAxis2";
            this.graphAxis13.Scale = categoryScale4;
            this.graphAxis13.Title = "Assigned             Changed";
            // 
            // graphAxis14
            // 
            this.graphAxis14.LabelFormat = "{0:N0}";
            this.graphAxis14.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis14.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis14.MaxSize = Telerik.Reporting.Drawing.Unit.Inch(12121212D);
            this.graphAxis14.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis14.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis14.MinorGridLineStyle.Visible = false;
            this.graphAxis14.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis14.Name = "graphAxis1";
            numericalScale3.Minimum = 0D;
            this.graphAxis14.Scale = numericalScale3;
            // 
            // barSeries7
            // 
            this.barSeries7.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup17.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.TOTAL_ASSIGNED_DRG"));
            graphGroup17.Name = "assignedGROUP";
            this.barSeries7.CategoryGroup = graphGroup17;
            colorPalette5.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(84)))), ((int)(((byte)(147))))));
            this.barSeries7.ColorPalette = colorPalette5;
            this.barSeries7.CoordinateSystem = this.cartesianCoordinateSystem7;
            this.barSeries7.DataPointLabel = "= Fields.TOTAL_ASSIGNED_DRG";
            this.barSeries7.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries7.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries7.DataPointLabelStyle.Visible = false;
            this.barSeries7.DataPointStyle.Visible = true;
            this.barSeries7.LegendItem.Style.Visible = false;
            this.barSeries7.Name = "BMSdrg";
            graphGroup18.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_ASSIGNED_DRG"));
            graphGroup18.Name = "AsgGroup";
            this.barSeries7.SeriesGroup = graphGroup18;
            this.barSeries7.ToolTip.Text = "= Fields.TOTAL_ASSIGNED_DRG";
            this.barSeries7.ToolTip.Title = "MS DRG ASSIGNED";
            this.barSeries7.Y = "= Fields.TOTAL_ASSIGNED_DRG";
            // 
            // barSeries8
            // 
            this.barSeries8.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Overlapped;
            graphGroup19.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CHANGED_DRG"));
            graphGroup19.Name = "chngeGROUP";
            this.barSeries8.CategoryGroup = graphGroup19;
            colorPalette6.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(186)))), ((int)(((byte)(44))))));
            this.barSeries8.ColorPalette = colorPalette6;
            this.barSeries8.CoordinateSystem = this.cartesianCoordinateSystem7;
            this.barSeries8.DataPointLabel = "= Fields.TOTAL_CHANGED_DRG";
            this.barSeries8.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries8.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries8.DataPointLabelFormat = "{0:P0}";
            this.barSeries8.DataPointLabelStyle.Visible = false;
            this.barSeries8.DataPointStyle.Visible = true;
            this.barSeries8.LegendItem.Style.Visible = false;
            this.barSeries8.Name = "BCM";
            graphGroup20.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.TOTAL_CHANGED_DRG"));
            graphGroup20.Name = "chnGroup";
            this.barSeries8.SeriesGroup = graphGroup20;
            this.barSeries8.ToolTip.Text = "= Fields.TOTAL_CHANGED_DRG";
            this.barSeries8.ToolTip.Title = "MS DRG Change";
            this.barSeries8.Y = "= Fields.TOTAL_CHANGED_DRG";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.CommandTimeout = 0;
            this.sqlDataSource1.ConnectionString = "THEMISConnectionString";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.sqlDataSource1.SelectCommand = "usp_Metis_OverallAccuracy_Select";
            this.sqlDataSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox17.Name = "ReportNameTextBox";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.9055118560791016D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Segoe UI";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox17.Style.Font.Underline = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "Inpatient_Overall_Accuracy";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0688321590423584D), Telerik.Reporting.Drawing.Unit.Inch(0.19976361095905304D));
            this.textBox18.Name = "ReportPageNumberTextBox";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5748031139373779D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox18.Style.Font.Name = "Segoe UI";
            this.textBox18.Value = "Page: {PageNumber}";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.49976348876953125D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // Inpatient_Overall_Accuracy
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "Inpatient_Overall_Accuracy";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AutoRefresh = true;
            reportParameter1.Name = "Startdate";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter1.Value = "08-05-2002";
            reportParameter1.Visible = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.Name = "EndDate";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter2.Value = "08-05-2017";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.FacilitySql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter3.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "FacilityName";
            reportParameter3.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "= AllDistinctValues(Fields.CRA_PERSONNEL_ID)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.AvailableValues.DataSource = this.RoutingSql;
            reportParameter5.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter5.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "Routing";
            reportParameter5.Value = "2";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter6.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter6.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartType";
            reportParameter6.Value = "1";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Name = "UserId";
            reportParameter7.Value = "996";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Name = "IcdType";
            reportParameter8.Value = "2";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "ChartTypeName";
            reportParameter9.Value = "inpatient";
            reportParameter9.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.5D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.SqlDataSource InpatientOverallAccSql;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.BarSeries BMSdrg;
        private Telerik.Reporting.BarSeries BCM;
        private Telerik.Reporting.BarSeries BPCS;
        private Telerik.Reporting.Graph graph3;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem3;
        private Telerik.Reporting.GraphAxis graphAxis6;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.BarSeries barSeries3;
        private Telerik.Reporting.BarSeries barSeries4;
        private Telerik.Reporting.Graph graph4;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem4;
        private Telerik.Reporting.GraphAxis graphAxis8;
        private Telerik.Reporting.GraphAxis graphAxis7;
        private Telerik.Reporting.BarSeries barSeries5;
        private Telerik.Reporting.BarSeries barSeries6;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.Graph graph7;
        private Telerik.Reporting.GraphAxis graphAxis13;
        private Telerik.Reporting.GraphAxis graphAxis14;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem7;
        private Telerik.Reporting.BarSeries barSeries7;
        private Telerik.Reporting.BarSeries barSeries8;
    }
}