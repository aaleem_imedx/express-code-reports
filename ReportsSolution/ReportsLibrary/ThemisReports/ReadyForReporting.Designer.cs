namespace ReportsLibrary.ThemisReports
{
    partial class ReadyForReporting
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.ToggleVisibilityAction toggleVisibilityAction1 = new Telerik.Reporting.ToggleVisibilityAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadyForReporting));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ToggleVisibilityAction toggleVisibilityAction2 = new Telerik.Reporting.ToggleVisibilityAction();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule6 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector3 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule7 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector4 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule8 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector5 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule9 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector6 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule10 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector7 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule11 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule12 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector8 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule13 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector9 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule14 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector10 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule15 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector11 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule16 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector12 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule17 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector13 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule18 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector14 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule19 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector15 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule20 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector16 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.FacilitySqlsource = new Telerik.Reporting.SqlDataSource();
            this.RoutingSqlSource = new Telerik.Reporting.SqlDataSource();
            this.ChartTypeSqlSource = new Telerik.Reporting.SqlDataSource();
            this.CountSql = new Telerik.Reporting.SqlDataSource();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.crosstab2 = new Telerik.Reporting.Crosstab();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.ReadyForReportinngII = new Telerik.Reporting.SqlDataSource();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.ReadyForReportingSql = new Telerik.Reporting.SqlDataSource();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox26 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox12
            // 
            this.textBox12.Action = null;
            this.textBox12.CanShrink = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0373667478561401D), Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D));
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.Visible = false;
            this.textBox12.StyleName = "Corporate.TableGroup";
            this.textBox12.Value = "";
            // 
            // textBox22
            // 
            this.textBox22.CanShrink = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D));
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox22.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Visible = false;
            this.textBox22.StyleName = "Corporate.TableBody";
            this.textBox22.Value = "= Fields.CHART_COUNT";
            // 
            // textBox5
            // 
            this.textBox5.Action = sortingAction1;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.20644265413284302D));
            this.textBox5.StyleName = "Corporate.TableHeader";
            this.textBox5.Value = "Routing";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.20644265413284302D));
            this.textBox6.StyleName = "Corporate.TableHeader";
            this.textBox6.Value = "ChartType";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.20644265413284302D));
            this.textBox7.StyleName = "Corporate.TableHeader";
            this.textBox7.Value = "FacilityCoder";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.20644265413284302D));
            this.textBox8.StyleName = "Corporate.TableHeader";
            this.textBox8.Value = "ChartCount";
            // 
            // textBox11
            // 
            toggleVisibilityAction1.DisplayExpandedMark = false;
            toggleVisibilityAction1.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.textBox12,
            this.textBox22});
            this.textBox11.Action = toggleVisibilityAction1;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0373667478561401D), Telerik.Reporting.Drawing.Unit.Inch(0.23063507676124573D));
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox11.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox11.StyleName = "Corporate.SubTotal";
            this.textBox11.Value = "= Fields.FACILITY_NAME";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0373667478561401D), Telerik.Reporting.Drawing.Unit.Inch(0.23063500225543976D));
            this.textBox13.StyleName = "Corporate.GrandTotal";
            this.textBox13.Value = "Total";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.24583350121974945D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "FACILITY_NAME";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.24583350121974945D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "ROUTING_TEXT";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.24583350121974945D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "CHART_TYPE_DESCR";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.24583350121974945D));
            this.textBox4.StyleName = "Apex.TableHeader";
            this.textBox4.Value = "CHART_COUNT";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.24468657374382019D));
            this.textBox27.StyleName = "Apex.TableHeader";
            this.textBox27.Value = "FACILITY_CODER";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.24468657374382019D));
            this.textBox28.StyleName = "Apex.TableHeader";
            this.textBox28.Value = "CHART_COUNT";
            // 
            // FacilitySqlsource
            // 
            this.FacilitySqlsource.ConnectionString = "THEMISConnectionString";
            this.FacilitySqlsource.Name = "FacilitySqlsource";
            this.FacilitySqlsource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySqlsource.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySqlsource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSqlSource
            // 
            this.RoutingSqlSource.ConnectionString = "THEMISConnectionString";
            this.RoutingSqlSource.Name = "RoutingSqlSource";
            this.RoutingSqlSource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, null)});
            this.RoutingSqlSource.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSqlSource.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypeSqlSource
            // 
            this.ChartTypeSqlSource.ConnectionString = "THEMISConnectionString";
            this.ChartTypeSqlSource.Name = "ChartTypeSqlSource";
            this.ChartTypeSqlSource.SelectCommand = "SELECT     CHART_TYPE_TEXT, CHART_TYPE_KEY, CHART_TYPE_DESCR\r\nFROM         CHART_" +
    "TYPE\r\nORDER BY CHART_TYPE_KEY";
            // 
            // CountSql
            // 
            this.CountSql.ConnectionString = "THEMISConnectionString";
            this.CountSql.Name = "CountSql";
            this.CountSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "= ReportItem.DataObject.MEDICAL_FACILITY_KEY"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlRouting", System.Data.DbType.String, "= ReportItem.DataObject.CHART_ROUTING_KEY"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuotRequesticlChartType", System.Data.DbType.String, "= ReportItem.DataObject.CHART_TYPE_KEY")});
            this.CountSql.SelectCommand = resources.GetString("CountSql.SelectCommand");
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(5.1999993324279785D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.crosstab2,
            this.table1});
            this.detailSection1.Name = "detailSection1";
            this.detailSection1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.detailSection1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // crosstab2
            // 
            this.crosstab2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D)));
            this.crosstab2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D)));
            this.crosstab2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D)));
            this.crosstab2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D)));
            this.crosstab2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.23063510656356812D)));
            this.crosstab2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D)));
            this.crosstab2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.23063497245311737D)));
            this.crosstab2.Body.SetCellContent(0, 3, this.textBox18);
            this.crosstab2.Body.SetCellContent(1, 0, this.textBox19);
            this.crosstab2.Body.SetCellContent(1, 1, this.textBox20);
            this.crosstab2.Body.SetCellContent(1, 2, this.textBox21);
            this.crosstab2.Body.SetCellContent(1, 3, this.textBox22);
            this.crosstab2.Body.SetCellContent(2, 3, this.textBox36);
            this.crosstab2.Body.SetCellContent(0, 0, this.textBox15, 1, 3);
            this.crosstab2.Body.SetCellContent(2, 0, this.textBox23, 1, 3);
            tableGroup1.Name = "rOUTING_TEXT";
            tableGroup1.ReportItem = this.textBox5;
            tableGroup2.Name = "cHART_TYPE_DESCR";
            tableGroup2.ReportItem = this.textBox6;
            tableGroup3.Name = "fACILITY_CODER";
            tableGroup3.ReportItem = this.textBox7;
            tableGroup4.Name = "cHART_COUNT";
            tableGroup4.ReportItem = this.textBox8;
            this.crosstab2.ColumnGroups.Add(tableGroup1);
            this.crosstab2.ColumnGroups.Add(tableGroup2);
            this.crosstab2.ColumnGroups.Add(tableGroup3);
            this.crosstab2.ColumnGroups.Add(tableGroup4);
            this.crosstab2.ColumnHeadersPrintOnEveryPage = true;
            this.crosstab2.Corner.SetCellContent(0, 0, this.textBox14);
            this.crosstab2.DataSource = this.ReadyForReportinngII;
            this.crosstab2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox15,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox36,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox11,
            this.textBox12,
            this.textBox13});
            this.crosstab2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.crosstab2.Name = "crosstab2";
            tableGroup14.Name = "fACILITY_NAME1";
            tableGroup14.ReportItem = this.textBox11;
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detail";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.ReportItem = this.textBox12;
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FACILITY_NAME"));
            tableGroup13.Name = "fACILITY_NAME";
            tableGroup13.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.FACILITY_NAME", Telerik.Reporting.SortDirection.Asc));
            tableGroup17.Name = "total";
            tableGroup17.ReportItem = this.textBox13;
            this.crosstab2.RowGroups.Add(tableGroup13);
            this.crosstab2.RowGroups.Add(tableGroup17);
            this.crosstab2.RowHeadersPrintOnEveryPage = true;
            this.crosstab2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.89996063709259033D));
            this.crosstab2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.crosstab2.Style.Visible = false;
            this.crosstab2.StyleName = "Corporate.TableNormal";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23063507676124573D));
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox18.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox18.StyleName = "Corporate.SubTotal";
            this.textBox18.Value = "= \"Total Chart Count : \"+ Sum(Fields.CHART_COUNT)";
            // 
            // textBox19
            // 
            this.textBox19.CanShrink = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D));
            this.textBox19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox19.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.Visible = false;
            this.textBox19.StyleName = "Corporate.TableBody";
            this.textBox19.Value = "= Fields.ROUTING_TEXT";
            // 
            // textBox20
            // 
            this.textBox20.CanShrink = true;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D));
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox20.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.Visible = false;
            this.textBox20.StyleName = "Corporate.TableBody";
            this.textBox20.Value = "= Fields.CHART_TYPE_DESCR";
            // 
            // textBox21
            // 
            this.textBox21.CanShrink = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23224790394306183D));
            this.textBox21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox21.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox21.Style.Visible = false;
            this.textBox21.StyleName = "Corporate.TableBody";
            this.textBox21.Value = "= Fields.FACILITY_CODER";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.190658450126648D), Telerik.Reporting.Drawing.Unit.Inch(0.23063500225543976D));
            this.textBox36.StyleName = "Corporate.GrandTotal";
            this.textBox36.Value = "= Sum(Fields.CHART_COUNT)";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5719754695892334D), Telerik.Reporting.Drawing.Unit.Inch(0.23063507676124573D));
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox15.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.StyleName = "Corporate.SubTotal";
            this.textBox15.Value = "= \"Total Rows :  \" + Count(Fields.FACILITY_CODER)";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5719754695892334D), Telerik.Reporting.Drawing.Unit.Inch(0.23063500225543976D));
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.StyleName = "Corporate.GrandTotal";
            this.textBox23.Value = "Total Rows :  {Count(Fields.FACILITY_CODER)}";
            // 
            // textBox14
            // 
            sortingAction2.SortingExpression = "= Fields.FACILITY_NAME";
            tableGroup8.Name = "group3";
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.Name = "group";
            tableGroup6.ChildGroups.Add(tableGroup7);
            tableGroup6.Name = "fACILITY_NAME1";
            tableGroup6.ReportItem = this.textBox11;
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detail";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Name = "group4";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.Name = "group1";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.ReportItem = this.textBox12;
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup9);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FACILITY_NAME"));
            tableGroup5.Name = "fACILITY_NAME";
            tableGroup5.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.FACILITY_NAME", Telerik.Reporting.SortDirection.Asc));
            sortingAction2.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            tableGroup5});
            this.textBox14.Action = sortingAction2;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0373667478561401D), Telerik.Reporting.Drawing.Unit.Inch(0.20644265413284302D));
            this.textBox14.StyleName = "Corporate.TableHeader";
            this.textBox14.Value = "Facility Name";
            // 
            // ReadyForReportinngII
            // 
            this.ReadyForReportinngII.ConnectionString = "THEMISConnectionString";
            this.ReadyForReportinngII.Name = "ReadyForReportinngII";
            this.ReadyForReportinngII.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "= Join(\',\',Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclFacilities", System.Data.DbType.String, "= Join(\',\',Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlRouting", System.Data.DbType.String, "= Join(\',\',Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclRouting", System.Data.DbType.String, "= Join(\',\',Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuotRequesticlChartType", System.Data.DbType.String, "= Join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclChartType", System.Data.DbType.String, "= Join(\',\', Parameters.ChartType.Value)")});
            this.ReadyForReportinngII.SelectCommand = resources.GetString("ReadyForReportinngII.SelectCommand");
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16249987483024597D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.5791664719581604D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox16);
            this.table1.Body.SetCellContent(0, 1, this.textBox17);
            this.table1.Body.SetCellContent(0, 2, this.textBox24);
            this.table1.Body.SetCellContent(0, 3, this.textBox25);
            this.table1.Body.SetCellContent(2, 0, this.table2, 1, 4);
            this.table1.Body.SetCellContent(1, 0, this.textBox26, 1, 4);
            tableGroup21.Name = "fACILITY_NAME2";
            tableGroup21.ReportItem = this.textBox1;
            tableGroup22.Name = "rOUTING_TEXT1";
            tableGroup22.ReportItem = this.textBox2;
            tableGroup23.Name = "cHART_TYPE_DESCR1";
            tableGroup23.ReportItem = this.textBox3;
            tableGroup24.Name = "cHART_COUNT1";
            tableGroup24.ReportItem = this.textBox4;
            this.table1.ColumnGroups.Add(tableGroup21);
            this.table1.ColumnGroups.Add(tableGroup22);
            this.table1.ColumnGroups.Add(tableGroup23);
            this.table1.ColumnGroups.Add(tableGroup24);
            this.table1.DataSource = this.ReadyForReportingSql;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox17,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.table2,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.199999213218689D), Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            this.table1.NoDataStyle.Font.Bold = true;
            tableGroup26.Name = "group";
            tableGroup27.Name = "group2";
            tableGroup28.Name = "group1";
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.ChildGroups.Add(tableGroup27);
            tableGroup25.ChildGroups.Add(tableGroup28);
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detail";
            this.table1.RowGroups.Add(tableGroup25);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.1874998807907105D));
            this.table1.StyleName = "Apex.TableNormal";
            // 
            // textBox16
            // 
            toggleVisibilityAction2.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table2,
            this.textBox26});
            this.textBox16.Action = toggleVisibilityAction2;
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.16249985992908478D));
            this.textBox16.StyleName = "Apex.TableBody";
            this.textBox16.Value = "= Fields.FACILITY_NAME";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.16249985992908478D));
            this.textBox17.StyleName = "Apex.TableBody";
            this.textBox17.Value = "= Fields.ROUTING_TEXT";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.16249985992908478D));
            this.textBox24.StyleName = "Apex.TableBody";
            this.textBox24.Value = "= Fields.CHART_TYPE_DESCR";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.16249985992908478D));
            this.textBox25.StyleName = "Apex.TableBody";
            this.textBox25.Value = "= Fields.CHART_COUNT";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.33447983860969543D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox32);
            this.table2.Body.SetCellContent(0, 1, this.textBox33);
            tableGroup18.Name = "fACILITY_CODER1";
            tableGroup18.ReportItem = this.textBox27;
            tableGroup19.Name = "cHART_COUNT2";
            tableGroup19.ReportItem = this.textBox28;
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.ColumnGroups.Add(tableGroup19);
            this.table2.DataSource = this.CountSql;
            this.table2.Filters.Add(new Telerik.Reporting.Filter("= Fields.MEDICAL_FACILITY_KEY", Telerik.Reporting.FilterOperator.Equal, "= ReportItem.DataObject.MEDICAL_FACILITY_KEY"));
            this.table2.Filters.Add(new Telerik.Reporting.Filter("= Fields.CHART_ROUTING_KEY", Telerik.Reporting.FilterOperator.Equal, "= ReportItem.DataObject.CHART_ROUTING_KEY"));
            this.table2.Filters.Add(new Telerik.Reporting.Filter("= Fields.CHART_TYPE_KEY", Telerik.Reporting.FilterOperator.Equal, "= ReportItem.DataObject.CHART_TYPE_KEY"));
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox32,
            this.textBox33,
            this.textBox27,
            this.textBox28});
            this.table2.Name = "table2";
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.Name = "detail";
            this.table2.RowGroups.Add(tableGroup20);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.5791664719581604D));
            this.table2.StyleName = "Apex.TableBody";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.33447986841201782D));
            this.textBox32.StyleName = "Apex.TableBody";
            this.textBox32.Value = "= Fields.FACILITY_CODER";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.33447986841201782D));
            this.textBox33.StyleName = "Apex.TableBody";
            this.textBox33.Value = "= Fields.CHART_COUNT";
            // 
            // ReadyForReportingSql
            // 
            this.ReadyForReportingSql.ConnectionString = "THEMISConnectionString";
            this.ReadyForReportingSql.Name = "ReadyForReportingSql";
            this.ReadyForReportingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "= join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclFacilities", System.Data.DbType.String, "= join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlRouting", System.Data.DbType.String, "= Join(\',\', Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclRouting", System.Data.DbType.String, "= Join(\',\', Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuotRequesticlChartType", System.Data.DbType.String, "= Join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@iclChartType", System.Data.DbType.String, "= Join(\',\', Parameters.ChartType.Value)")});
            this.ReadyForReportingSql.SelectCommand = resources.GetString("ReadyForReportingSql.SelectCommand");
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox9.Name = "ReportNameTextBox";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.9999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Segoe UI";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox9.Style.Font.Underline = true;
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "ReadyForReporting";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.80000013113021851D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox9});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.425196647644043D), Telerik.Reporting.Drawing.Unit.Inch(0.60629922151565552D));
            this.textBox10.Name = "ReportPageNumberTextBox";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5748031139373779D), Telerik.Reporting.Drawing.Unit.Inch(0.39370077848434448D));
            this.textBox10.Style.Font.Name = "Segoe UI";
            this.textBox10.Value = "Page: {PageNumber}";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Underline = true;
            this.textBox26.StyleName = "Apex.TableBody";
            this.textBox26.Value = "UnReported Coder Chart";
            // 
            // ReadyForReporting
            // 
            this.DataSource = this.ReadyForReportingSql;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "ReadyForReporting";
            this.PageSettings.ContinuousPaper = true;
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySqlsource;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSqlSource;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.MultiValue = true;
            reportParameter2.Name = "RoutingType";
            reportParameter2.Value = "= AllDistinctValues(Fields.CHART_ROUTING_KEY)";
            reportParameter2.Visible = true;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypeSqlSource;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "= AllDistinctValues(Fields.CHART_TYPE_KEY)";
            reportParameter3.Visible = true;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.Name = "UserId";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter4.Value = "4850";
            reportParameter4.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Color = System.Drawing.Color.Black;
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector3});
            styleRule6.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule6.Style.Font.Name = "Tahoma";
            styleRule6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule7.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector4});
            styleRule7.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule7.Style.Font.Name = "Tahoma";
            styleRule7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableGroup")});
            styleRule8.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector5});
            styleRule8.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule8.Style.Font.Name = "Tahoma";
            styleRule8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.SubTotal")});
            styleRule9.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector6});
            styleRule9.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule9.Style.Font.Name = "Tahoma";
            styleRule9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector7.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.GrandTotal")});
            styleRule10.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector7});
            styleRule10.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule10.Style.Font.Bold = true;
            styleRule10.Style.Font.Name = "Tahoma";
            styleRule10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule11.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Corporate.TableNormal")});
            styleRule11.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule11.Style.Color = System.Drawing.Color.Black;
            styleRule11.Style.Font.Name = "Tahoma";
            styleRule11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector8.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableBody")});
            styleRule12.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector8});
            styleRule12.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule12.Style.Font.Name = "Tahoma";
            styleRule12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector9.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableHeader")});
            styleRule13.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector9});
            styleRule13.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            styleRule13.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule13.Style.Color = System.Drawing.Color.White;
            styleRule13.Style.Font.Name = "Tahoma";
            styleRule13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector10.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableGroup")});
            styleRule14.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector10});
            styleRule14.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(237)))));
            styleRule14.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule14.Style.Font.Name = "Tahoma";
            styleRule14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector11.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.GrandTotal")});
            styleRule15.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector11});
            styleRule15.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(156)))), ((int)(((byte)(183)))));
            styleRule15.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule15.Style.Font.Bold = true;
            styleRule15.Style.Font.Name = "Tahoma";
            styleRule15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector12.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.SubTotal")});
            styleRule16.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector12});
            styleRule16.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(206)))), ((int)(((byte)(219)))));
            styleRule16.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule16.Style.Font.Name = "Tahoma";
            styleRule16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector13.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.SubTotal")});
            styleRule17.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector13});
            styleRule17.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule17.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule17.Style.Font.Name = "Book Antiqua";
            styleRule17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector14.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.GrandTotal")});
            styleRule18.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector14});
            styleRule18.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(216)))));
            styleRule18.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule18.Style.Font.Bold = true;
            styleRule18.Style.Font.Name = "Book Antiqua";
            styleRule18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            descendantSelector15.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableGroup")});
            styleRule19.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector15});
            styleRule19.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule19.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule19.Style.Font.Name = "Book Antiqua";
            styleRule19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector16.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableCorner")});
            styleRule20.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector16});
            styleRule20.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule20.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule20.Style.Font.Name = "Book Antiqua";
            styleRule20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5,
            styleRule6,
            styleRule7,
            styleRule8,
            styleRule9,
            styleRule10,
            styleRule11,
            styleRule12,
            styleRule13,
            styleRule14,
            styleRule15,
            styleRule16,
            styleRule17,
            styleRule18,
            styleRule19,
            styleRule20});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(10.90000057220459D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.SqlDataSource ReadyForReportingSql;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource CountSql;
        private Telerik.Reporting.SqlDataSource FacilitySqlsource;
        private Telerik.Reporting.SqlDataSource RoutingSqlSource;
        private Telerik.Reporting.SqlDataSource ChartTypeSqlSource;
        private Telerik.Reporting.SqlDataSource ReadyForReportinngII;
        private Telerik.Reporting.Crosstab crosstab2;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox26;
    }
}