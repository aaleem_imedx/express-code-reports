namespace ReportsLibrary.ThemisReports
{
    partial class InsertedvsCompleted
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.GraphAxis graphAxis12;
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertedvsCompleted));
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.LogarithmicScale logarithmicScale1 = new Telerik.Reporting.LogarithmicScale();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScale categoryScale3 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.NumericalScale numericalScale4 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.LogarithmicScale logarithmicScale2 = new Telerik.Reporting.LogarithmicScale();
            Telerik.Reporting.LogarithmicScaleCrossAxisPosition logarithmicScaleCrossAxisPosition1 = new Telerik.Reporting.LogarithmicScaleCrossAxisPosition();
            Telerik.Reporting.NumericalScale numericalScale5 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale4 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition1 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.CategoryScaleCrossAxisPosition categoryScaleCrossAxisPosition2 = new Telerik.Reporting.CategoryScaleCrossAxisPosition();
            Telerik.Reporting.NumericalScale numericalScale6 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.Facailitiessql = new Telerik.Reporting.SqlDataSource();
            this.ChartsqlDataSource = new Telerik.Reporting.SqlDataSource();
            this.Routingsql = new Telerik.Reporting.SqlDataSource();
            this.Assigneesql = new Telerik.Reporting.SqlDataSource();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis11 = new Telerik.Reporting.GraphAxis();
            this.InsertedvsCompletedSql = new Telerik.Reporting.SqlDataSource();
            this.barSeries1 = new Telerik.Reporting.BarSeries();
            this.barSeries2 = new Telerik.Reporting.BarSeries();
            this.barSeries3 = new Telerik.Reporting.BarSeries();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.graphAxis6 = new Telerik.Reporting.GraphAxis();
            this.graphAxis9 = new Telerik.Reporting.GraphAxis();
            this.graphAxis10 = new Telerik.Reporting.GraphAxis();
            this.graphAxis8 = new Telerik.Reporting.GraphAxis();
            this.graphAxis7 = new Telerik.Reporting.GraphAxis();
            graphAxis12 = new Telerik.Reporting.GraphAxis();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // graphAxis12
            // 
            graphAxis12.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            graphAxis12.MajorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            graphAxis12.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis12.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            graphAxis12.MinorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            graphAxis12.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            graphAxis12.MinorGridLineStyle.Visible = false;
            graphAxis12.Name = "graphAxis12";
            graphAxis12.Scale = categoryScale1;
            graphAxis12.Style.LineColor = System.Drawing.Color.Black;
            // 
            // Facailitiessql
            // 
            this.Facailitiessql.ConnectionString = "THEMISConnectionString";
            this.Facailitiessql.Name = "Facailitiessql";
            this.Facailitiessql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.Facailitiessql.SelectCommand = "dbo.usp_FacilitiesForUser_Select";
            this.Facailitiessql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartsqlDataSource
            // 
            this.ChartsqlDataSource.ConnectionString = "THEMISConnectionString";
            this.ChartsqlDataSource.Name = "ChartsqlDataSource";
            this.ChartsqlDataSource.SelectCommand = "SELECT     CHART_TYPE_TEXT, CHART_TYPE_KEY, CHART_TYPE_DESCR\r\nFROM         CHART_" +
    "TYPE\r\nORDER BY CHART_TYPE_KEY";
            // 
            // Routingsql
            // 
            this.Routingsql.ConnectionString = "THEMISConnectionString";
            this.Routingsql.Name = "Routingsql";
            this.Routingsql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, null)});
            this.Routingsql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.Routingsql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // Assigneesql
            // 
            this.Assigneesql.ConnectionString = "THEMISConnectionString";
            this.Assigneesql.Name = "Assigneesql";
            this.Assigneesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FunctionUserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "= join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "= join(\',\', Parameters.FacilityName.Value)")});
            this.Assigneesql.SelectCommand = resources.GetString("Assigneesql.SelectCommand");
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.1000399589538574D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.graph1});
            this.detailSection1.Name = "detailSection1";
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DateValue"));
            graphGroup1.Name = "dateValueGroup";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem1);
            this.graph1.DataSource = this.InsertedvsCompletedSql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Legend.Style.Visible = false;
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.graph1.Name = "graph1";
            this.graph1.NoDataMessage = "No Data Found.";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.barSeries1);
            this.graph1.Series.Add(this.barSeries2);
            this.graph1.Series.Add(this.barSeries3);
            this.graph1.SeriesGroups.Add(graphGroup2);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "graph1";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.XAxis = graphAxis12;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis11;
            // 
            // graphAxis11
            // 
            this.graphAxis11.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis11.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis11.MinorGridLineStyle.Visible = false;
            this.graphAxis11.Name = "graphAxis11";
            numericalScale1.Minimum = 0D;
            this.graphAxis11.Scale = numericalScale1;
            this.graphAxis11.Style.LineColor = System.Drawing.Color.Transparent;
            // 
            // InsertedvsCompletedSql
            // 
            this.InsertedvsCompletedSql.ConnectionString = "THEMISConnectionString";
            this.InsertedvsCompletedSql.Name = "InsertedvsCompletedSql";
            this.InsertedvsCompletedSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@flag", System.Data.DbType.Int16, "= Parameters.flag.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequestisLastNDays", System.Data.DbType.Int32, "= Parameters.LastDays.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@UserID", System.Data.DbType.Int32, "= Parameters.UserId.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlRouting", System.Data.DbType.String, "=join(\',\',Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlRouting", System.Data.DbType.String, "=join(\',\',Parameters.RoutingType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlChartType", System.Data.DbType.String, "= join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlChartType", System.Data.DbType.String, "=join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@SingleQuoteRequesticlIvCAssignee", System.Data.DbType.String, "=join(\',\',Parameters.Assignee.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlIvCAssignee", System.Data.DbType.String, "=join(\',\',Parameters.Assignee.Value)")});
            this.InsertedvsCompletedSql.SelectCommand = resources.GetString("InsertedvsCompletedSql.SelectCommand");
            // 
            // barSeries1
            // 
            this.barSeries1.CategoryGroup = graphGroup1;
            this.barSeries1.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries1.DataPointLabel = "= Sum(Fields.CHARTS_INSERTED)";
            this.barSeries1.DataPointLabelStyle.Visible = false;
            this.barSeries1.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries1.DataPointStyle.Visible = true;
            this.barSeries1.LegendItem.Value = "\'Sum(CHARTS_INSERTED)\'";
            this.barSeries1.Name = "barSeries1";
            graphGroup2.Name = "seriesGroup";
            this.barSeries1.SeriesGroup = graphGroup2;
            this.barSeries1.ToolTip.Text = "=Format(\'CHARTS INSERTED : {0}     \r\nCHARTS COMPLETED :{1} \r\nTURNAROUND TIME : {2" +
    "}\', Sum(Fields.CHARTS_INSERTED) , Sum(Fields.CHARTS_COMPLETED) ,  Sum(Fields.TUR" +
    "NAROUND_TIME) \r\n)\r\n\r\n";
            this.barSeries1.ToolTip.Title = "= Fields.DateValue";
            this.barSeries1.Y = "= Sum(Fields.CHARTS_INSERTED)";
            // 
            // barSeries2
            // 
            this.barSeries2.CategoryGroup = graphGroup1;
            this.barSeries2.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries2.DataPointLabel = "= Sum(Fields.CHARTS_COMPLETED)";
            this.barSeries2.DataPointLabelStyle.Visible = false;
            this.barSeries2.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries2.DataPointStyle.Visible = true;
            this.barSeries2.LegendItem.Value = "\'Sum(CHARTS_COMPLETED)\'";
            this.barSeries2.Name = "barSeries2";
            this.barSeries2.SeriesGroup = graphGroup2;
            this.barSeries2.ToolTip.Text = "=Format(\'CHARTS INSERTED : {0}     \r\nCHARTS COMPLETED :{1} \r\nTURNAROUND TIME : {2" +
    "}\', Sum(Fields.CHARTS_INSERTED) , Sum(Fields.CHARTS_COMPLETED) ,  Sum(Fields.TUR" +
    "NAROUND_TIME) \r\n)\r\n\r\n";
            this.barSeries2.ToolTip.Title = "= Fields.DateValue";
            this.barSeries2.Y = "= Sum(Fields.CHARTS_COMPLETED)";
            // 
            // barSeries3
            // 
            this.barSeries3.CategoryGroup = graphGroup1;
            this.barSeries3.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries3.DataPointLabel = "= Sum(Fields.TURNAROUND_TIME)";
            this.barSeries3.DataPointLabelStyle.Visible = false;
            this.barSeries3.DataPointStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.barSeries3.DataPointStyle.Visible = true;
            this.barSeries3.LegendItem.Value = "\'Sum(TURNAROUND_TIME)\'";
            this.barSeries3.Name = "barSeries3";
            this.barSeries3.SeriesGroup = graphGroup2;
            this.barSeries3.ToolTip.Text = "=Format(\'CHARTS INSERTED : {0}     \r\nCHARTS COMPLETED :{1} \r\nTURNAROUND TIME : {2" +
    "}\', Sum(Fields.CHARTS_INSERTED) , Sum(Fields.CHARTS_COMPLETED) ,  Sum(Fields.TUR" +
    "NAROUND_TIME) \r\n)\r\n\r\n";
            this.barSeries3.ToolTip.Title = "= Fields.DateValue";
            this.barSeries3.Y = "= Sum(Fields.TURNAROUND_TIME)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox1.Name = "ReportNameTextBox";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.3999607264995575D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Inserted   Vs  Completed";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000006556510925D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D));
            this.textBox2.Name = "ReportPageNumberTextBox";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5748031139373779D), Telerik.Reporting.Drawing.Unit.Inch(0.39370077848434448D));
            this.textBox2.Style.Font.Name = "Segoe UI";
            this.textBox2.Value = "Page: {PageNumber}";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.39370074868202209D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // graphAxis1
            // 
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "graphAxis1";
            this.graphAxis1.Scale = numericalScale2;
            // 
            // graphAxis2
            // 
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            logarithmicScale1.DataPointTicks = true;
            this.graphAxis2.Scale = logarithmicScale1;
            // 
            // graphAxis3
            // 
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "GraphAxis1";
            this.graphAxis3.Scale = categoryScale2;
            // 
            // graphAxis4
            // 
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "GraphAxis1";
            this.graphAxis4.Scale = categoryScale3;
            // 
            // graphAxis5
            // 
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.Name = "GraphAxis2";
            this.graphAxis5.Scale = numericalScale3;
            // 
            // graphAxis6
            // 
            this.graphAxis6.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.Visible = false;
            this.graphAxis6.Name = "GraphAxis1";
            this.graphAxis6.Scale = numericalScale4;
            // 
            // graphAxis9
            // 
            this.graphAxis9.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis9.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis9.MinorGridLineStyle.Visible = false;
            this.graphAxis9.Name = "GraphAxis3";
            logarithmicScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.Auto;
            logarithmicScaleCrossAxisPosition1.Value = 1D;
            logarithmicScale2.CrossAxisPositions.Add(logarithmicScaleCrossAxisPosition1);
            logarithmicScale2.DataPointTicks = true;
            logarithmicScale2.Maximum = 100D;
            logarithmicScale2.Minimum = 10D;
            this.graphAxis9.Scale = logarithmicScale2;
            // 
            // graphAxis10
            // 
            this.graphAxis10.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis10.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis10.MinorGridLineStyle.Visible = false;
            this.graphAxis10.Name = "GraphAxis4";
            this.graphAxis10.Scale = numericalScale5;
            // 
            // graphAxis8
            // 
            this.graphAxis8.LabelAngle = 45;
            this.graphAxis8.LabelFormat = "";
            this.graphAxis8.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis8.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MajorGridLineStyle.Visible = true;
            this.graphAxis8.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis8.MaxSize = Telerik.Reporting.Drawing.Unit.Inch(10D);
            this.graphAxis8.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.Visible = true;
            this.graphAxis8.MinSize = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.graphAxis8.MultiLevelCategoryLabels = true;
            this.graphAxis8.Name = "graphAxis8";
            categoryScaleCrossAxisPosition1.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.AtMinimum;
            categoryScaleCrossAxisPosition2.Position = Telerik.Reporting.GraphScaleCrossAxisPosition.AtMaximum;
            categoryScale4.CrossAxisPositions.Add(categoryScaleCrossAxisPosition1);
            categoryScale4.CrossAxisPositions.Add(categoryScaleCrossAxisPosition2);
            this.graphAxis8.Scale = categoryScale4;
            // 
            // graphAxis7
            // 
            this.graphAxis7.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MajorGridLineStyle.Visible = true;
            this.graphAxis7.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MinorGridLineStyle.Visible = true;
            this.graphAxis7.MultiLevelCategoryLabels = true;
            this.graphAxis7.Name = "graphAxis7";
            this.graphAxis7.Scale = numericalScale6;
            this.graphAxis7.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.graphAxis7.Style.Visible = true;
            // 
            // InsertedvsCompleted
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "InsertedvsCompleted";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.Facailitiessql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY\r\n";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.ChartsqlDataSource;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter2.MultiValue = true;
            reportParameter2.Name = "ChartType";
            reportParameter2.Value = "= AllDistinctValues(Fields.CHART_TYPE_KEY)";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.Routingsql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "RoutingType";
            reportParameter3.Value = "= AllDistinctValues(Fields.CHART_ROUTING_KEY)";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.Assigneesql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.FULL_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.APP_USER_KEY";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Assignee";
            reportParameter4.Value = "= AllDistinctValues(Fields.APP_USER_KEY)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.Value = "3655";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "LastDays";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter6.Value = "7";
            reportParameter6.Visible = true;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "flag";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter7.Value = "1";
            reportParameter7.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.SqlDataSource InsertedvsCompletedSql;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource Facailitiessql;
        private Telerik.Reporting.SqlDataSource ChartsqlDataSource;
        private Telerik.Reporting.SqlDataSource Routingsql;
        private Telerik.Reporting.SqlDataSource Assigneesql;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.GraphAxis graphAxis6;
        private Telerik.Reporting.GraphAxis graphAxis9;
        private Telerik.Reporting.GraphAxis graphAxis10;
        private Telerik.Reporting.GraphAxis graphAxis8;
        private Telerik.Reporting.GraphAxis graphAxis7;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis11;
        private Telerik.Reporting.BarSeries barSeries1;
        private Telerik.Reporting.BarSeries barSeries2;
        private Telerik.Reporting.BarSeries barSeries3;
    }
}