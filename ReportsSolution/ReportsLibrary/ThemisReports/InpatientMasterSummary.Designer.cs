namespace ReportsLibrary.ThemisReports
{
    partial class InpatientMasterSummary
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InpatientMasterSummary));
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule6 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector3 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule7 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector4 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule8 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector5 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.MasterSummarysql1 = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.DtlMasterSummarysql2 = new Telerik.Reporting.SqlDataSource();
            this.crosstab1 = new Telerik.Reporting.Crosstab();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7048444747924805D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox26.StyleName = "Apex.TableHeader";
            this.textBox26.Value = "Cases Reviewed";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2543160915374756D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox40.StyleName = "Apex.TableHeader";
            this.textBox40.Value = "Varience";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox43.StyleName = "Apex.TableHeader";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.43054258823394775D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox27.StyleName = "Apex.TableHeader";
            this.textBox27.Value = "A";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37222689390182495D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox28.StyleName = "Apex.TableHeader";
            this.textBox28.Value = "C";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3491189181804657D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox29.StyleName = "Apex.TableHeader";
            this.textBox29.Value = "D";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42098313570022583D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.StyleName = "Apex.TableHeader";
            this.textBox42.Value = "R";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.545982837677002D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox31.StyleName = "Apex.TableHeader";
            this.textBox31.Value = "Changes";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8070992231369019D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox30.StyleName = "Apex.TableHeader";
            this.textBox30.Value = "Assigned By Facility";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox32.StyleName = "Apex.TableHeader";
            this.textBox32.Value = "Percent Accuracy";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox54.StyleName = "Apex.TableHeader";
            this.textBox54.Value = "Amphion Standard";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84166437387466431D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox62.StyleName = "Apex.TableHeader";
            this.textBox62.Value = "Facility Code";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9249994158744812D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox63.StyleName = "Apex.TableHeader";
            this.textBox63.Value = "Consultant Code";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.591665506362915D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox64.StyleName = "Apex.TableHeader";
            this.textBox64.Value = "Change Type";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3937486410140991D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox65.StyleName = "Apex.TableHeader";
            this.textBox65.Value = "Change Reason";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87291759252548218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox72.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox72.StyleName = "Apex.TableGroup";
            this.textBox72.Value = "= Fields.ConsultantDischDisp";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70624923706054688D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox71.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox71.StyleName = "Apex.TableGroup";
            this.textBox71.Value = "= Fields.FacilityDischDisp";
            // 
            // textBox89
            // 
            this.textBox89.Format = "{0:N2}";
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox89.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox89.StyleName = "Apex.TableBody";
            this.textBox89.Value = "=IIF(Fields.PAID_BY_MSDRG_APC = 0, Fields.Payer, \r\nIIF((IsNull(Fields.DRGVariance" +
    ", 0)) = 0, \'$\'+0, \r\n\'$\' +FORMAT(\'{0:N2}\', Fields.DRGVariance) \r\n)\r\n)";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3729126453399658D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox70.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox70.StyleName = "Apex.TableGroup";
            this.textBox70.Value = "= Fields.ConsultantMSDRGCode";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2166663408279419D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox69.StyleName = "Apex.TableGroup";
            this.textBox69.Value = "= Fields.FacilityMSDRGCode";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0395818948745728D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox68.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox68.StyleName = "Apex.TableGroup";
            this.textBox68.Value = "= Fields.FacilityCoder";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2375013828277588D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox67.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox67.StyleName = "Apex.TableGroup";
            this.textBox67.Value = "=Format(\'{0:d}\', Fields.REVIEWED_DATE)";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4249992370605469D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox66.StyleName = "Apex.TableGroup";
            this.textBox66.Value = "= Fields.ENCOUNTER_NUMBER";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // MasterSummarysql1
            // 
            this.MasterSummarysql1.ConnectionString = "THEMISConnectionString";
            this.MasterSummarysql1.Name = "MasterSummarysql1";
            this.MasterSummarysql1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.MasterSummarysql1.SelectCommand = "dbo.usp_Metis_MasterSummary_Select";
            this.MasterSummarysql1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4000003337860107D), Telerik.Reporting.Drawing.Unit.Inch(0.60000014305114746D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Inpatient Master Summary";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2,
            this.crosstab1});
            this.detail.Name = "detail";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7048443555831909D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2543165683746338D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0459830760955811D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.43054324388504028D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.37222757935523987D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.34911853075027466D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.42098334431648254D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.5459835529327393D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8070989847183228D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0459830760955811D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000003576278687D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox34);
            this.table2.Body.SetCellContent(0, 3, this.textBox35);
            this.table2.Body.SetCellContent(0, 4, this.textBox36);
            this.table2.Body.SetCellContent(0, 5, this.textBox37);
            this.table2.Body.SetCellContent(0, 8, this.textBox38);
            this.table2.Body.SetCellContent(0, 6, this.textBox44);
            this.table2.Body.SetCellContent(0, 7, this.textBox39);
            this.table2.Body.SetCellContent(0, 9, this.textBox33);
            this.table2.Body.SetCellContent(0, 2, this.textBox45);
            this.table2.Body.SetCellContent(1, 0, this.textBox46);
            this.table2.Body.SetCellContent(1, 1, this.textBox47);
            this.table2.Body.SetCellContent(1, 2, this.textBox48);
            this.table2.Body.SetCellContent(1, 3, this.textBox49);
            this.table2.Body.SetCellContent(1, 4, this.textBox50);
            this.table2.Body.SetCellContent(1, 5, this.textBox51);
            this.table2.Body.SetCellContent(1, 6, this.textBox52);
            this.table2.Body.SetCellContent(1, 7, this.textBox53);
            this.table2.Body.SetCellContent(1, 8, this.textBox56);
            this.table2.Body.SetCellContent(0, 10, this.textBox57);
            this.table2.Body.SetCellContent(1, 10, this.textBox58);
            this.table2.Body.SetCellContent(0, 1, this.table3);
            this.table2.Body.SetCellContent(1, 9, this.textBox41);
            tableGroup3.Name = "cHART_COUNT";
            tableGroup3.ReportItem = this.textBox26;
            tableGroup4.Name = "group4";
            tableGroup4.ReportItem = this.textBox40;
            tableGroup5.Name = "group5";
            tableGroup5.ReportItem = this.textBox43;
            tableGroup6.Name = "aDD_COUNT";
            tableGroup6.ReportItem = this.textBox27;
            tableGroup7.Name = "cHANGE_COUNT";
            tableGroup7.ReportItem = this.textBox28;
            tableGroup8.Name = "dELETE_COUNT";
            tableGroup8.ReportItem = this.textBox29;
            tableGroup9.Name = "group2";
            tableGroup9.ReportItem = this.textBox42;
            tableGroup10.Name = "group1";
            tableGroup10.ReportItem = this.textBox31;
            tableGroup11.Name = "fACILITY_CODE_COUNT";
            tableGroup11.ReportItem = this.textBox30;
            tableGroup12.Name = "group3";
            tableGroup12.ReportItem = this.textBox32;
            tableGroup13.Name = "group8";
            tableGroup13.ReportItem = this.textBox54;
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.DataSource = this.DtlMasterSummarysql2;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox34,
            this.table3,
            this.textBox45,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox44,
            this.textBox39,
            this.textBox38,
            this.textBox33,
            this.textBox57,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox56,
            this.textBox41,
            this.textBox58,
            this.textBox26,
            this.textBox40,
            this.textBox43,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox42,
            this.textBox31,
            this.textBox30,
            this.textBox32,
            this.textBox54});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.7000001072883606D));
            this.table2.Name = "table2";
            this.table2.NoDataMessage = "No Data Found.";
            tableGroup15.Name = "group6";
            tableGroup16.Name = "group7";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup14.Name = "detail";
            this.table2.RowGroups.Add(tableGroup14);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(11.977083206176758D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.table2.StyleName = "Apex.TableNormal";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7048444747924805D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox34.StyleName = "Apex.TableBody";
            this.textBox34.Value = "= Fields.CHART_COUNT";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.43054258823394775D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox35.StyleName = "Apex.TableBody";
            this.textBox35.Value = "= Fields.ADD_COUNT";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37222689390182495D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox36.StyleName = "Apex.TableBody";
            this.textBox36.Value = "= Fields.CHANGE_COUNT";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3491189181804657D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox37.StyleName = "Apex.TableBody";
            this.textBox37.Value = "= Fields.DELETE_COUNT";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8070992231369019D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox38.StyleName = "Apex.TableBody";
            this.textBox38.Value = "= Fields.FACILITY_CODE_COUNT";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42098313570022583D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox44.StyleName = "Apex.TableBody";
            this.textBox44.Value = "= Fields.RESEQUENCE_COUNT";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.545982837677002D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox39.StyleName = "Apex.TableBody";
            this.textBox39.Value = "= Fields.TOTAL_CHANGES";
            // 
            // textBox33
            // 
            this.textBox33.Format = "{0:P2}";
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox33.StyleName = "Apex.TableBody";
            this.textBox33.Value = "=iif(Fields.FACILITY_CODE_COUNT = 0, 0, 1-((Fields.TOTAL_CHANGES * 1) / Fields.FA" +
    "CILITY_CODE_COUNT))";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox45.StyleName = "Apex.TableBody";
            this.textBox45.Value = "Code Totals";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7048444747924805D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox46.StyleName = "Apex.TableBody";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2543160915374756D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox47.StyleName = "Apex.TableBody";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox48.StyleName = "Apex.TableBody";
            this.textBox48.Value = "MS DRG Total";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.43054258823394775D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox49.StyleName = "Apex.TableBody";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37222689390182495D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox50.StyleName = "Apex.TableBody";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3491189181804657D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox51.StyleName = "Apex.TableBody";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42098313570022583D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox52.StyleName = "Apex.TableBody";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.545982837677002D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox53.StyleName = "Apex.TableBody";
            this.textBox53.Value = "= Fields.MSDRG_CHANGE_COUNT";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8070992231369019D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox56.StyleName = "Apex.TableBody";
            this.textBox56.Value = "= Fields.CHART_COUNT";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox57.StyleName = "Apex.TableBody";
            this.textBox57.Value = "95%";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox58.StyleName = "Apex.TableBody";
            this.textBox58.Value = "95%";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2543166875839233D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox60);
            tableGroup1.Name = "_1";
            this.table3.ColumnGroups.Add(tableGroup1);
            this.table3.DataSource = this.sqlDataSource1;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox60});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.table3.Name = "table3";
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detail";
            this.table3.RowGroups.Add(tableGroup2);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2543166875839233D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.table3.StyleName = "Apex.TableBody";
            // 
            // textBox60
            // 
            this.textBox60.Format = "{0:$#,##0.00}";
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2543166875839233D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox60.StyleName = "Normal.TableBody";
            this.textBox60.Value = "=iif(Fields.[1]<>0,Fields.[1],0)";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "THEMISConnectionString";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.String, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.String, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.String, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.String, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.String, "= Parameters.EndDate.Value")});
            this.sqlDataSource1.SelectCommand = resources.GetString("sqlDataSource1.SelectCommand");
            // 
            // textBox41
            // 
            this.textBox41.Format = "{0:P2}";
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0459835529327393D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox41.StyleName = "Apex.TableBody";
            this.textBox41.Value = "=iif(Fields.CHART_COUNT = 0, 0, 1-((Fields.MSDRG_CHANGE_COUNT* 1) / Fields.CHART_" +
    "COUNT))";
            // 
            // DtlMasterSummarysql2
            // 
            this.DtlMasterSummarysql2.ConnectionString = "THEMISConnectionString";
            this.DtlMasterSummarysql2.Name = "DtlMasterSummarysql2";
            this.DtlMasterSummarysql2.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestidBeginDate", System.Data.DbType.String, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequestidEndDate", System.Data.DbType.String, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlChartType", System.Data.DbType.String, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlRouting", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacCoders", System.Data.DbType.String, "=Join(\',\', Parameters.Coder.Value)")});
            this.DtlMasterSummarysql2.SelectCommand = resources.GetString("DtlMasterSummarysql2.SelectCommand");
            // 
            // crosstab1
            // 
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.84166431427001953D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.92499935626983643D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.59166538715362549D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.39374840259552D)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.crosstab1.Body.SetCellContent(0, 0, this.textBox83);
            this.crosstab1.Body.SetCellContent(0, 1, this.textBox84);
            this.crosstab1.Body.SetCellContent(0, 2, this.textBox85);
            this.crosstab1.Body.SetCellContent(0, 3, this.textBox86);
            tableGroup17.Name = "facilityMSDRGCode1";
            tableGroup17.ReportItem = this.textBox62;
            tableGroup18.Name = "consultantMSDRGCode1";
            tableGroup18.ReportItem = this.textBox63;
            tableGroup19.Name = "facilityDischDisp1";
            tableGroup19.ReportItem = this.textBox64;
            tableGroup20.Name = "consultantDischDisp1";
            tableGroup20.ReportItem = this.textBox65;
            this.crosstab1.ColumnGroups.Add(tableGroup17);
            this.crosstab1.ColumnGroups.Add(tableGroup18);
            this.crosstab1.ColumnGroups.Add(tableGroup19);
            this.crosstab1.ColumnGroups.Add(tableGroup20);
            this.crosstab1.Corner.SetCellContent(0, 0, this.textBox73);
            this.crosstab1.Corner.SetCellContent(0, 1, this.textBox74);
            this.crosstab1.Corner.SetCellContent(0, 2, this.textBox75);
            this.crosstab1.Corner.SetCellContent(0, 3, this.textBox76);
            this.crosstab1.Corner.SetCellContent(0, 6, this.textBox78);
            this.crosstab1.Corner.SetCellContent(0, 7, this.textBox79);
            this.crosstab1.Corner.SetCellContent(0, 5, this.textBox87);
            this.crosstab1.Corner.SetCellContent(0, 4, this.textBox55);
            this.crosstab1.DataSource = this.MasterSummarysql1;
            this.crosstab1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox55,
            this.textBox87,
            this.textBox78,
            this.textBox79,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox89});
            this.crosstab1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.crosstab1.Name = "crosstab1";
            this.crosstab1.NoDataMessage = "No Data Found.";
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "detail";
            tableGroup28.ChildGroups.Add(tableGroup29);
            tableGroup28.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ConsultantDischDisp"));
            tableGroup28.Name = "consultantDischDisp2";
            tableGroup28.ReportItem = this.textBox72;
            tableGroup28.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup27.ChildGroups.Add(tableGroup28);
            tableGroup27.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FacilityDischDisp"));
            tableGroup27.Name = "facilityDischDisp2";
            tableGroup27.ReportItem = this.textBox71;
            tableGroup27.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup26.ChildGroups.Add(tableGroup27);
            tableGroup26.Name = "group10";
            tableGroup26.ReportItem = this.textBox89;
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ConsultantMSDRGCode"));
            tableGroup25.Name = "consultantMSDRGCode2";
            tableGroup25.ReportItem = this.textBox70;
            tableGroup25.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FacilityMSDRGCode"));
            tableGroup24.Name = "facilityMSDRGCode2";
            tableGroup24.ReportItem = this.textBox69;
            tableGroup24.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FacilityCoder"));
            tableGroup23.Name = "facilityCoder2";
            tableGroup23.ReportItem = this.textBox68;
            tableGroup23.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup22.ChildGroups.Add(tableGroup23);
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.REVIEWED_DATE"));
            tableGroup22.Name = "rEVIEWED_DATE2";
            tableGroup22.ReportItem = this.textBox67;
            tableGroup22.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ENCOUNTER_NUMBER"));
            tableGroup21.Name = "eNCOUNTER_NUMBER2";
            tableGroup21.ReportItem = this.textBox66;
            tableGroup21.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            this.crosstab1.RowGroups.Add(tableGroup21);
            this.crosstab1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(12.622905731201172D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.crosstab1.StyleName = "Apex.TableNormal";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84166437387466431D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox83.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox83.StyleName = "Apex.TableBody";
            this.textBox83.Value = "= Fields.FacilityCode";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9249994158744812D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox84.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox84.StyleName = "Apex.TableBody";
            this.textBox84.Value = "= Fields.ConsultantCode";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.591665506362915D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox85.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox85.StyleName = "Apex.TableBody";
            this.textBox85.Value = "= Fields.ChangeTypeText";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3937486410140991D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox86.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox86.StyleName = "Apex.TableBody";
            this.textBox86.Value = "= Fields.ChangeReasonText";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4249992370605469D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox73.StyleName = "Apex.TableHeader";
            this.textBox73.Value = "Account";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2375013828277588D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox74.StyleName = "Apex.TableHeader";
            this.textBox74.Value = "Reviewed Date";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0395818948745728D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox75.StyleName = "Apex.TableHeader";
            this.textBox75.Value = "Facility Coder";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2166663408279419D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox76.StyleName = "Apex.TableHeader";
            this.textBox76.Value = "Facility MS-DRG";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70624923706054688D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox78.StyleName = "Apex.TableHeader";
            this.textBox78.Value = "Facility Discharge";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87291759252548218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox79.StyleName = "Apex.TableHeader";
            this.textBox79.Value = "Consultant Discharge";
            // 
            // textBox87
            // 
            sortingAction1.SortingExpression = "=IIF(Fields.PAID_BY_MSDRG_APC = 0, Fields.Payer, \r\nIIF(Fields.DRGVariance = 0.00," +
    " 0.00, \r\n\'$\' + (Fields.DRGVariance)\r\n)\r\n)";
            this.textBox87.Action = sortingAction1;
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox87.StyleName = "Apex.TableHeader";
            this.textBox87.Value = "Variance";
            // 
            // textBox55
            // 
            sortingAction2.SortingExpression = "= Fields.ConsultantMSDRGCode";
            this.textBox55.Action = sortingAction2;
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3729126453399658D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox55.StyleName = "Apex.TableHeader";
            this.textBox55.Value = "Consultant MS-DRG";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // InpatientMasterSummary
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "InpatientMasterSummary";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "ChartType";
            reportParameter2.Value = "";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.RoutingSql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter3.Mergeable = false;
            reportParameter3.Name = "Routing";
            reportParameter3.Value = "";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.Mergeable = false;
            reportParameter4.Name = "UserId";
            reportParameter4.Value = "";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.AvailableValues.DataSource = this.CoderSql;
            reportParameter5.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter5.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter5.MultiValue = true;
            reportParameter5.Name = "Coder";
            reportParameter5.Value = "";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "IcdType";
            reportParameter6.Value = "2";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "Startdate";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter7.Value = "";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "EndDate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "";
            reportParameter8.Visible = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Name = "ChartTypeName";
            reportParameter9.Value = "Inpatient";
            reportParameter9.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Color = System.Drawing.Color.Black;
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector3});
            styleRule6.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule6.Style.Font.Name = "Tahoma";
            styleRule6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule7.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector4});
            styleRule7.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule7.Style.Font.Name = "Tahoma";
            styleRule7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableGroup")});
            styleRule8.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector5});
            styleRule8.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule8.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule8.Style.Font.Name = "Book Antiqua";
            styleRule8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5,
            styleRule6,
            styleRule7,
            styleRule8});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(15.586556434631348D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource MasterSummarysql1;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource DtlMasterSummarysql2;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.Crosstab crosstab1;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox55;
    }
}