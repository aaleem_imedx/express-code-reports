namespace ReportsLibrary.ThemisReports
{
    partial class InpatientTrendingAccuracy
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction3 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction4 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction5 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction6 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction7 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction8 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction9 = new Telerik.Reporting.SortingAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InpatientTrendingAccuracy));
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette1 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup8 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup9 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup10 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette2 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette3 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.GraphGroup graphGroup7 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.Drawing.ColorPalette colorPalette4 = new Telerik.Reporting.Drawing.ColorPalette();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.GraphGroup graphGroup11 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup12 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup13 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup14 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup15 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup16 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.IPTrendingAccuracySql = new Telerik.Reporting.SqlDataSource();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.graph1 = new Telerik.Reporting.Graph();
            this.cartesianCoordinateSystem2 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.B1 = new Telerik.Reporting.BarSeries();
            this.B2 = new Telerik.Reporting.BarSeries();
            this.B3 = new Telerik.Reporting.BarSeries();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.cartesianCoordinateSystem1 = new Telerik.Reporting.CartesianCoordinateSystem();
            this.barSeries1 = new Telerik.Reporting.BarSeries();
            this.barSeries2 = new Telerik.Reporting.BarSeries();
            this.barSeries3 = new Telerik.Reporting.BarSeries();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4666669368743896D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox1.StyleName = "Apex.TableHeader";
            this.textBox1.Value = "Date Range";
            // 
            // textBox3
            // 
            sortingAction1.SortingExpression = "= Fields.NumOfCharts";
            sortingAction1.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox3.Action = sortingAction1;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0812495946884155D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox3.StyleName = "Apex.TableHeader";
            this.textBox3.Value = "# of Charts";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.466667652130127D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0812500715255737D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.758333683013916D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000011920928955D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5291652679443359D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3416670560836792D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000003576278687D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.80000090599060059D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.80000090599060059D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0000003576278687D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox9);
            this.table1.Body.SetCellContent(0, 1, this.textBox11);
            this.table1.Body.SetCellContent(0, 2, this.textBox12);
            this.table1.Body.SetCellContent(0, 4, this.textBox13);
            this.table1.Body.SetCellContent(0, 5, this.textBox14);
            this.table1.Body.SetCellContent(0, 7, this.textBox15);
            this.table1.Body.SetCellContent(0, 8, this.textBox16);
            this.table1.Body.SetCellContent(0, 3, this.textBox10);
            this.table1.Body.SetCellContent(0, 6, this.textBox18);
            this.table1.Body.SetCellContent(0, 9, this.textBox22);
            tableGroup2.Name = "group3";
            tableGroup2.ReportItem = this.textBox1;
            tableGroup3.Name = "group4";
            tableGroup3.ReportItem = this.textBox3;
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.Name = "startDate";
            tableGroup1.ReportItem = this.textBox21;
            tableGroup5.Name = "group5";
            tableGroup5.ReportItem = this.textBox4;
            tableGroup6.Name = "group6";
            tableGroup6.ReportItem = this.textBox2;
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.Name = "msDrgChanges";
            tableGroup4.ReportItem = this.textBox23;
            tableGroup8.Name = "group7";
            tableGroup8.ReportItem = this.textBox5;
            tableGroup9.Name = "group8";
            tableGroup9.ReportItem = this.textBox6;
            tableGroup10.Name = "group9";
            tableGroup10.ReportItem = this.textBox17;
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.ChildGroups.Add(tableGroup9);
            tableGroup7.ChildGroups.Add(tableGroup10);
            tableGroup7.Name = "cmCodeCount";
            tableGroup7.ReportItem = this.textBox25;
            tableGroup12.Name = "group10";
            tableGroup12.ReportItem = this.textBox7;
            tableGroup13.Name = "group11";
            tableGroup13.ReportItem = this.textBox8;
            tableGroup14.Name = "group12";
            tableGroup14.ReportItem = this.textBox19;
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.ChildGroups.Add(tableGroup13);
            tableGroup11.ChildGroups.Add(tableGroup14);
            tableGroup11.Name = "pcsCodeCount";
            tableGroup11.ReportItem = this.textBox28;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.DataSource = this.IPTrendingAccuracySql;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox9,
            this.textBox11,
            this.textBox12,
            this.textBox10,
            this.textBox13,
            this.textBox14,
            this.textBox18,
            this.textBox15,
            this.textBox16,
            this.textBox22,
            this.textBox1,
            this.textBox23,
            this.textBox4,
            this.textBox2,
            this.textBox25,
            this.textBox5,
            this.textBox6,
            this.textBox17,
            this.textBox28,
            this.textBox7,
            this.textBox8,
            this.textBox19,
            this.textBox21,
            this.textBox3});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.8999614715576172D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "detail";
            this.table1.RowGroups.Add(tableGroup15);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(11.777087211608887D), Telerik.Reporting.Drawing.Unit.Inch(0.59999996423721313D));
            this.table1.StyleName = "Apex.TableNormal";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4666669368743896D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox9.StyleName = "Apex.TableBody";
            this.textBox9.Value = "=Format(\"{0}-{1}\",Fields.StartDate, Fields.EndDate)";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0812495946884155D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox11.StyleName = "Apex.TableBody";
            this.textBox11.Value = "= Fields.NumOfCharts";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75833350419998169D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox12.StyleName = "Apex.TableBody";
            this.textBox12.Value = "= Fields.MsDrgChanges";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5291664600372315D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox13.StyleName = "Apex.TableBody";
            this.textBox13.Value = "= Fields.CmCodeCount";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3416666984558106D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox14.StyleName = "Apex.TableBody";
            this.textBox14.Value = "= Fields.CmCodeChanges";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox15.StyleName = "Apex.TableBody";
            this.textBox15.Value = "= Fields.PcsCodeCount";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox16.StyleName = "Apex.TableBody";
            this.textBox16.Value = "= Fields.PcsCodeChanges";
            // 
            // textBox10
            // 
            this.textBox10.Format = "{0:P0}";
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox10.StyleName = "Apex.TableBody";
            this.textBox10.Value = "=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100 / Fields.NumOfC" +
    "harts))))/100,0)";
            // 
            // textBox18
            // 
            this.textBox18.Format = "{0:P0}";
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox18.StyleName = "Apex.TableBody";
            this.textBox18.Value = "=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCod" +
    "eCount))))/100,0)";
            // 
            // textBox22
            // 
            this.textBox22.Format = "{0:P0}";
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox22.StyleName = "Apex.TableBody";
            this.textBox22.Value = "=iif(Fields.PcsCodeCount<>0,(100-((Round(Fields.PcsCodeChanges * 100 / Fields.Pcs" +
    "CodeCount))))/100,0)";
            // 
            // textBox4
            // 
            sortingAction2.SortingExpression = "= Fields.MsDrgChanges";
            sortingAction2.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox4.Action = sortingAction2;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75833350419998169D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox4.StyleName = "Apex.TableHeader";
            this.textBox4.Value = "Changes";
            // 
            // textBox2
            // 
            sortingAction3.SortingExpression = "=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100.0 / Fields.NumO" +
    "fCharts))))/100,0)";
            sortingAction3.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox2.Action = sortingAction3;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.StyleName = "Apex.TableHeader";
            this.textBox2.Value = "Accuracy";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7583341598510742D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.StyleName = "Apex.TableHeader";
            this.textBox23.Value = "MS-DRG";
            // 
            // textBox5
            // 
            sortingAction4.SortingExpression = "= Fields.CmCodeCount";
            sortingAction4.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox5.Action = sortingAction4;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5291664600372315D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.StyleName = "Apex.TableHeader";
            this.textBox5.Value = "Count";
            // 
            // textBox6
            // 
            sortingAction5.SortingExpression = "= Fields.CmCodeChanges";
            sortingAction5.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox6.Action = sortingAction5;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3416666984558106D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox6.StyleName = "Apex.TableHeader";
            this.textBox6.Value = "Changes";
            // 
            // textBox17
            // 
            sortingAction6.SortingExpression = "=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100.0 / Fields.CmC" +
    "odeCount))))/100,0)";
            sortingAction6.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox17.Action = sortingAction6;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox17.StyleName = "Apex.TableHeader";
            this.textBox17.Value = "Accuracy";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8708329200744629D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.StyleName = "Apex.TableHeader";
            this.textBox25.Value = "CM Codes";
            // 
            // textBox7
            // 
            sortingAction7.SortingExpression = "= Fields.PcsCodeCount";
            sortingAction7.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox7.Action = sortingAction7;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox7.StyleName = "Apex.TableHeader";
            this.textBox7.Value = "Pcs Code Count";
            // 
            // textBox8
            // 
            sortingAction8.SortingExpression = "= Fields.PcsCodeChanges";
            sortingAction8.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox8.Action = sortingAction8;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox8.StyleName = "Apex.TableHeader";
            this.textBox8.Value = "Pcs Code Changes";
            // 
            // textBox19
            // 
            sortingAction9.SortingExpression = "=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100.0 / Fields.CmC" +
    "odeCount))))/100,0)";
            sortingAction9.Targets.AddRange(new Telerik.Reporting.IActionTarget[] {
            this.table1});
            this.textBox19.Action = sortingAction9;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox19.StyleName = "Apex.TableHeader";
            this.textBox19.Value = "Accuracy";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.StyleName = "Apex.TableHeader";
            this.textBox28.Value = "PCS Codes";
            // 
            // IPTrendingAccuracySql
            // 
            this.IPTrendingAccuracySql.ConnectionString = "THEMISConnectionString";
            this.IPTrendingAccuracySql.Name = "IPTrendingAccuracySql";
            this.IPTrendingAccuracySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= Parameters.ChartType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@BeginDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupMode", System.Data.DbType.Byte, "= Parameters.GroupMode.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupByNumberOfCharts", System.Data.DbType.Int32, "= Parameters.NumberofCharts.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@GroupByTimeFrame", System.Data.DbType.AnsiString, "= Parameters.TimeFrame.Value")});
            this.IPTrendingAccuracySql.SelectCommand = "dbo.usp_Metis_IP_Trending_Select";
            this.IPTrendingAccuracySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5479171276092529D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox21.StyleName = "Apex.TableHeader";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Segoe UI";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox20.Style.Font.Underline = true;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "Inpatient Trending Accuracy";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(4.5000004768371582D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.graph1});
            this.detail.Name = "detail";
            // 
            // graph1
            // 
            graphGroup1.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100 / Fields.NumOfC" +
            "harts))))/100,0)"));
            graphGroup1.Name = "graphGroup";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCod" +
            "eCount))))/100,0)"));
            graphGroup2.Name = "graphGroup1";
            graphGroup3.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.PcsCodeCount<>0,(100-((Round(Fields.PcsCodeChanges * 100 / Fields.Pcs" +
            "CodeCount))))/100,0)"));
            graphGroup3.Name = "graphGroup2";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CategoryGroups.Add(graphGroup2);
            this.graph1.CategoryGroups.Add(graphGroup3);
            colorPalette1.Colors.Add(System.Drawing.Color.Blue);
            colorPalette1.Colors.Add(System.Drawing.Color.PaleGreen);
            colorPalette1.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128))))));
            this.graph1.ColorPalette = colorPalette1;
            this.graph1.CoordinateSystems.Add(this.cartesianCoordinateSystem2);
            this.graph1.DataSource = this.IPTrendingAccuracySql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.31458505988121033D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.B1);
            this.graph1.Series.Add(this.B2);
            this.graph1.Series.Add(this.B3);
            graphGroup8.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.StartDate"));
            graphGroup8.Name = "graphGroup3";
            graphGroup9.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.StartDate"));
            graphGroup9.Name = "graphGroup4";
            graphGroup10.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.StartDate"));
            graphGroup10.Name = "graphGroup5";
            this.graph1.SeriesGroups.Add(graphGroup8);
            this.graph1.SeriesGroups.Add(graphGroup9);
            this.graph1.SeriesGroups.Add(graphGroup10);
            this.graph1.SeriesGroups.Add(graphGroup5);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(10.885416984558106D), Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "graph1";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // cartesianCoordinateSystem2
            // 
            this.cartesianCoordinateSystem2.Name = "cartesianCoordinateSystem2";
            this.cartesianCoordinateSystem2.XAxis = this.graphAxis4;
            this.cartesianCoordinateSystem2.YAxis = this.graphAxis5;
            // 
            // graphAxis4
            // 
            this.graphAxis4.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.None;
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.Transparent;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MajorGridLineStyle.Visible = false;
            this.graphAxis4.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "GraphAxis1";
            categoryScale1.SpacingSlotCount = 5D;
            this.graphAxis4.Scale = categoryScale1;
            this.graphAxis4.Title = "Period";
            // 
            // graphAxis5
            // 
            this.graphAxis5.LabelFormat = "{0:P0}";
            this.graphAxis5.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.MinorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis5.Name = "GraphAxis2";
            numericalScale1.SpacingSlotCount = -45D;
            this.graphAxis5.Scale = numericalScale1;
            this.graphAxis5.Title = "Percent Accuracy";
            // 
            // B1
            // 
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100 / Fields.NumOfC" +
            "harts))))/100,0)"));
            graphGroup4.Name = "graphGroup";
            this.B1.CategoryGroup = graphGroup4;
            colorPalette2.Colors.Add(System.Drawing.Color.Blue);
            this.B1.ColorPalette = colorPalette2;
            this.B1.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.B1.DataPointLabel = "=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100 / Fields.NumOfC" +
    "harts))))/100,0)";
            this.B1.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.Center;
            this.B1.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B1.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B1.DataPointLabelFormat = "{0:P0}";
            this.B1.DataPointLabelStyle.Visible = false;
            this.B1.LegendItem.Style.Visible = false;
            this.B1.Name = "B1";
            graphGroup5.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.rowNum+10"));
            graphGroup5.Name = "graphGroup6";
            this.B1.SeriesGroup = graphGroup5;
            this.B1.ToolTip.Text = "=100-((Round(Fields.MsDrgChanges * 100.0 / Fields.NumOfCharts)))";
            this.B1.ToolTip.Title = "MS Drg % Accuracy";
            this.B1.Y = "=iif(Fields.NumOfCharts<>0,(100-((Round(Fields.MsDrgChanges * 100 / Fields.NumOfC" +
    "harts))))/100,0)";
            // 
            // B2
            // 
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCod" +
            "eCount))))/100,0)"));
            graphGroup6.Name = "graphGroup1";
            this.B2.CategoryGroup = graphGroup6;
            colorPalette3.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128))))));
            this.B2.ColorPalette = colorPalette3;
            this.B2.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.B2.DataPointLabel = "=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCod" +
    "eCount))))/100,0)";
            this.B2.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.Center;
            this.B2.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B2.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B2.DataPointLabelFormat = "{0:P2}";
            this.B2.DataPointLabelStyle.Visible = false;
            this.B2.LegendItem.Style.Visible = false;
            this.B2.Name = "B2";
            this.B2.SeriesGroup = graphGroup5;
            this.B2.ToolTip.Text = "=100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCodeCount)))";
            this.B2.ToolTip.Title = "CM % Accuracy";
            this.B2.Y = "=iif(Fields.CmCodeCount<>0,(100-((Round(Fields.CmCodeChanges * 100 / Fields.CmCod" +
    "eCount))))/100,0)";
            // 
            // B3
            // 
            graphGroup7.Groupings.Add(new Telerik.Reporting.Grouping("=iif(Fields.PcsCodeCount<>0,(100-((Round(Fields.PcsCodeChanges * 100 / Fields.Pcs" +
            "CodeCount))))/100,0)"));
            graphGroup7.Name = "graphGroup2";
            this.B3.CategoryGroup = graphGroup7;
            colorPalette4.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))));
            this.B3.ColorPalette = colorPalette4;
            this.B3.CoordinateSystem = this.cartesianCoordinateSystem2;
            this.B3.DataPointLabel = "=iif(Fields.PcsCodeCount<>0,(100-((Round(Fields.PcsCodeChanges * 100 / Fields.Pcs" +
    "CodeCount))))/100,0)";
            this.B3.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.Center;
            this.B3.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B3.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.B3.DataPointLabelFormat = "{0:P2}";
            this.B3.DataPointLabelStyle.Visible = false;
            this.B3.LegendItem.Style.Visible = false;
            this.B3.Name = "B3";
            this.B3.SeriesGroup = graphGroup5;
            this.B3.ToolTip.Text = "=100-((Round(Fields.PcsCodeChanges * 100 / Fields.PcsCodeCount)))";
            this.B3.ToolTip.Title = "PC % Accuracy";
            this.B3.Y = "=iif(Fields.PcsCodeCount>0,(100-((Round(Fields.PcsCodeChanges * 100 / Fields.PcsC" +
    "odeCount))))/100,0)";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // graphAxis2
            // 
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            this.graphAxis2.Scale = numericalScale2;
            // 
            // graphAxis3
            // 
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "GraphAxis1";
            this.graphAxis3.Scale = categoryScale2;
            // 
            // graphAxis1
            // 
            this.graphAxis1.LabelPlacement = Telerik.Reporting.GraphAxisLabelPlacement.NextToAxis;
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorTickMarkDisplayType = Telerik.Reporting.GraphAxisTickMarkDisplayType.None;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "graphAxis1";
            numericalScale3.DataPointTicks = true;
            numericalScale3.Minimum = 0D;
            this.graphAxis1.Scale = numericalScale3;
            // 
            // cartesianCoordinateSystem1
            // 
            this.cartesianCoordinateSystem1.Name = "cartesianCoordinateSystem1";
            this.cartesianCoordinateSystem1.XAxis = this.graphAxis3;
            this.cartesianCoordinateSystem1.YAxis = this.graphAxis1;
            // 
            // barSeries1
            // 
            graphGroup11.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.MsDrgChanges * 100.0 / Fields.NumOfCharts))))/100"));
            graphGroup11.Name = "graphGroup";
            this.barSeries1.CategoryGroup = graphGroup11;
            this.barSeries1.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries1.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries1.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            graphGroup12.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.MsDrgChanges * 100.0 / Fields.NumOfCharts))))/100"));
            graphGroup12.Name = "graphGroup3";
            this.barSeries1.SeriesGroup = graphGroup12;
            this.barSeries1.Y = "=(100-((Round(Fields.MsDrgChanges * 100.0 / Fields.NumOfCharts))))/100";
            // 
            // barSeries2
            // 
            graphGroup13.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.CmCodeChanges * 100.0 / Fields.CmCodeCount))))/100"));
            graphGroup13.Name = "graphGroup1";
            this.barSeries2.CategoryGroup = graphGroup13;
            this.barSeries2.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries2.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries2.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            graphGroup14.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.CmCodeChanges * 100.0 / Fields.CmCodeCount))))/100"));
            graphGroup14.Name = "graphGroup4";
            this.barSeries2.SeriesGroup = graphGroup14;
            this.barSeries2.Y = "=(100-((Round(Fields.CmCodeChanges * 100.0 / Fields.CmCodeCount))))/100";
            // 
            // barSeries3
            // 
            graphGroup15.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.PcsCodeChanges * 100.0 / Fields.PcsCodeCount))))/100"));
            graphGroup15.Name = "graphGroup2";
            this.barSeries3.CategoryGroup = graphGroup15;
            this.barSeries3.CoordinateSystem = this.cartesianCoordinateSystem1;
            this.barSeries3.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries3.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            graphGroup16.Groupings.Add(new Telerik.Reporting.Grouping("=(100-((Round(Fields.PcsCodeChanges * 100.0 / Fields.PcsCodeCount))))/100"));
            graphGroup16.Name = "graphGroup5";
            this.barSeries3.SeriesGroup = graphGroup16;
            this.barSeries3.Y = "=(100-((Round(Fields.PcsCodeChanges * 100.0 / Fields.PcsCodeCount))))/100";
            // 
            // InpatientTrendingAccuracy
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "InpatientTrendingAccuracy";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "= AllDistinctValues(Fields.MEDICAL_FACILITY_KEY)";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "2";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.Mergeable = false;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "1";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "= AllDistinctValues(Fields.CRA_PERSONNEL_ID)";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Value = "3655";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartTypeName";
            reportParameter6.Value = "Inpatient";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "IcdType";
            reportParameter7.Value = "2";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "Startdate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "08-05-2002";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "EndDate";
            reportParameter9.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter9.Value = "08-05-2017";
            reportParameter9.Visible = true;
            reportParameter10.AllowBlank = false;
            reportParameter10.AllowNull = true;
            reportParameter10.AutoRefresh = true;
            reportParameter10.Mergeable = false;
            reportParameter10.Name = "GroupMode";
            reportParameter10.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter10.Value = "2";
            reportParameter10.Visible = true;
            reportParameter11.AllowBlank = false;
            reportParameter11.AllowNull = true;
            reportParameter11.AutoRefresh = true;
            reportParameter11.Mergeable = false;
            reportParameter11.Name = "NumberofCharts";
            reportParameter11.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter11.Value = "10";
            reportParameter11.Visible = true;
            reportParameter12.AllowBlank = false;
            reportParameter12.AllowNull = true;
            reportParameter12.AutoRefresh = true;
            reportParameter12.Mergeable = false;
            reportParameter12.Name = "TimeFrame";
            reportParameter12.Value = "week";
            reportParameter12.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(11.777126312255859D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource IPTrendingAccuracySql;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem1;
        private Telerik.Reporting.BarSeries barSeries1;
        private Telerik.Reporting.BarSeries barSeries2;
        private Telerik.Reporting.BarSeries barSeries3;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.CartesianCoordinateSystem cartesianCoordinateSystem2;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.BarSeries B1;
        private Telerik.Reporting.BarSeries B2;
        private Telerik.Reporting.BarSeries B3;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox20;
    }
}