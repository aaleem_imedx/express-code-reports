namespace ReportsLibrary.ThemisReports
{
    partial class OPMasterSummary
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.SortingAction sortingAction1 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction2 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction3 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction4 = new Telerik.Reporting.SortingAction();
            Telerik.Reporting.SortingAction sortingAction5 = new Telerik.Reporting.SortingAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OPMasterSummary));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector3 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.OPMasterSummarySql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.OPMSDetailSql = new Telerik.Reporting.SqlDataSource();
            this.crosstab1 = new Telerik.Reporting.Crosstab();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083336114883423D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox53.StyleName = "Apex.TableHeader";
            this.textBox53.Value = "Charts Reviewed";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7604166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox55.StyleName = "Apex.TableHeader";
            this.textBox55.Value = "Variances";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox57.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox57.Style.Color = System.Drawing.Color.Black;
            this.textBox57.StyleName = "Apex.TableHeader";
            this.textBox57.Value = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76875084638595581D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox20.StyleName = "Apex.TableHeader";
            this.textBox20.Value = "A";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87291526794433594D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox31.StyleName = "Apex.TableHeader";
            this.textBox31.Value = "C";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93541663885116577D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox33.StyleName = "Apex.TableHeader";
            this.textBox33.Value = "D";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1875011920928955D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox21.StyleName = "Apex.TableHeader";
            this.textBox21.Value = "R";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437498807907105D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox35.StyleName = "Apex.TableHeader";
            this.textBox35.Value = "Code/ Modifier Changes";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2479171752929688D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox34.StyleName = "Apex.TableHeader";
            this.textBox34.Value = "Coder Codes";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324325799942017D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox13.StyleName = "Apex.TableHeader";
            this.textBox13.Value = "Facility Code";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4970159530639648D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox26.StyleName = "Apex.TableHeader";
            this.textBox26.Value = "Consultant Code";
            // 
            // textBox75
            // 
            sortingAction1.SortingExpression = "= Fields.ChangeTypeText";
            this.textBox75.Action = sortingAction1;
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0490992069244385D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox75.StyleName = "Apex.TableHeader";
            this.textBox75.Value = "Change Type";
            // 
            // textBox77
            // 
            sortingAction2.SortingExpression = "= Fields.ChangeReasonText";
            this.textBox77.Action = sortingAction2;
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324324607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox77.StyleName = "Apex.TableHeader";
            this.textBox77.Value = "Change Reason";
            // 
            // textBox78
            // 
            sortingAction3.SortingExpression = "= Fields.OP_Mod1_Mod2_Type";
            this.textBox78.Action = sortingAction3;
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2886824607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox78.StyleName = "Apex.TableHeader";
            this.textBox78.Value = "Modifier Change";
            // 
            // textBox79
            // 
            sortingAction4.SortingExpression = "= Fields.Mod1ChangeReason";
            this.textBox79.Action = sortingAction4;
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324324607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox79.StyleName = "Apex.TableHeader";
            this.textBox79.Value = "Modifier 1 Reason";
            // 
            // textBox80
            // 
            sortingAction5.SortingExpression = "= Fields.Mod2ChangeReason";
            this.textBox80.Action = sortingAction5;
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3720158338546753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox80.StyleName = "Apex.TableHeader";
            this.textBox80.Value = "Modifier 2 Reason";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9999881386756897D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox83.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox83.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox83.StyleName = "";
            this.textBox83.Value = "=iif(Fields.ApcVariance=0,\'\',\'$\' + Fields.ApcVariance)";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2157644033432007D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox66.StyleName = "Apex.TableGroup";
            this.textBox66.Value = "= Fields.ConsultantApcCode";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3511794805526733D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox65.StyleName = "Apex.TableGroup";
            this.textBox65.Value = "= Fields.FacilityApcCode";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1011812686920166D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox64.StyleName = "Apex.TableGroup";
            this.textBox64.Value = "= Fields.FacilityCoder";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81993061304092407D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox63.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox63.StyleName = "Apex.TableGroup";
            this.textBox63.Value = "= Format(\'{0:d}\',Fields.REVIEWED_DATE)";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0595108270645142D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox62.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox62.StyleName = "Apex.TableGroup";
            this.textBox62.Value = "= Fields.ENCOUNTER_NUMBER";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\',Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // OPMasterSummarySql
            // 
            this.OPMasterSummarySql.ConnectionString = "THEMISConnectionString";
            this.OPMasterSummarySql.Name = "OPMasterSummarySql";
            this.OPMasterSummarySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.String, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.String, "= join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.String, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.OPMasterSummarySql.SelectCommand = "dbo.usp_Metis_OP_MasterSummary_Select";
            this.OPMasterSummarySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox50});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5854578018188477D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7145426273345947D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Segoe UI";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox50.Style.Font.Underline = true;
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "Outpatient Master Summary";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(6.7000002861022949D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2,
            this.crosstab1});
            this.detail.Name = "detail";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2083348035812378D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7604166269302368D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.99999964237213135D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.76875019073486328D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87291574478149414D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.93541735410690308D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1875005960464478D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8437516689300537D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.2479152679443359D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table2.Body.SetCellContent(0, 3, this.textBox41);
            this.table2.Body.SetCellContent(0, 4, this.textBox43);
            this.table2.Body.SetCellContent(0, 5, this.textBox45);
            this.table2.Body.SetCellContent(0, 8, this.textBox46);
            this.table2.Body.SetCellContent(0, 0, this.textBox54);
            this.table2.Body.SetCellContent(0, 1, this.textBox56);
            this.table2.Body.SetCellContent(0, 2, this.textBox58);
            this.table2.Body.SetCellContent(0, 6, this.textBox32);
            this.table2.Body.SetCellContent(0, 7, this.textBox36);
            this.table2.Body.SetCellContent(1, 0, this.textBox37);
            this.table2.Body.SetCellContent(1, 1, this.textBox38);
            this.table2.Body.SetCellContent(1, 2, this.textBox39);
            this.table2.Body.SetCellContent(1, 3, this.textBox40);
            this.table2.Body.SetCellContent(1, 4, this.textBox42);
            this.table2.Body.SetCellContent(1, 5, this.textBox44);
            this.table2.Body.SetCellContent(1, 6, this.textBox47);
            this.table2.Body.SetCellContent(1, 7, this.textBox48);
            this.table2.Body.SetCellContent(1, 8, this.textBox49);
            tableGroup1.Name = "group3";
            tableGroup1.ReportItem = this.textBox53;
            tableGroup2.Name = "group4";
            tableGroup2.ReportItem = this.textBox55;
            tableGroup3.Name = "group5";
            tableGroup3.ReportItem = this.textBox57;
            tableGroup4.Name = "aDD_COUNT";
            tableGroup4.ReportItem = this.textBox20;
            tableGroup5.Name = "cHANGE_COUNT";
            tableGroup5.ReportItem = this.textBox31;
            tableGroup6.Name = "dELETE_COUNT";
            tableGroup6.ReportItem = this.textBox33;
            tableGroup7.Name = "group6";
            tableGroup7.ReportItem = this.textBox21;
            tableGroup8.Name = "group7";
            tableGroup8.ReportItem = this.textBox35;
            tableGroup9.Name = "fACILITY_CODE_COUNT";
            tableGroup9.ReportItem = this.textBox34;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnGroups.Add(tableGroup2);
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.DataSource = this.OPMSDetailSql;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox54,
            this.textBox56,
            this.textBox58,
            this.textBox41,
            this.textBox43,
            this.textBox45,
            this.textBox32,
            this.textBox36,
            this.textBox46,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox42,
            this.textBox44,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox53,
            this.textBox55,
            this.textBox57,
            this.textBox20,
            this.textBox31,
            this.textBox33,
            this.textBox21,
            this.textBox35,
            this.textBox34});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.14401562511920929D), Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D));
            this.table2.Name = "table2";
            tableGroup11.Name = "group8";
            tableGroup12.Name = "group9";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.ChildGroups.Add(tableGroup12);
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detail";
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(11.82500171661377D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.table2.StyleName = "Apex.TableNormal";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76875084638595581D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox41.StyleName = "Apex.TableBody";
            this.textBox41.Value = "= Fields.ADD_COUNT";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87291526794433594D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox43.StyleName = "Apex.TableBody";
            this.textBox43.Value = "= Fields.CHANGE_COUNT";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93541663885116577D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox45.StyleName = "Apex.TableBody";
            this.textBox45.Value = "= Fields.DELETE_COUNT";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2479171752929688D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox46.StyleName = "Apex.TableBody";
            this.textBox46.Value = "= Fields.FACILITY_CODE_COUNT";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083336114883423D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox54.StyleName = "Apex.TableBody";
            this.textBox54.Value = "= Fields.CHART_COUNT";
            // 
            // textBox56
            // 
            this.textBox56.Format = "{0:C2}";
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7604166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox56.StyleName = "Apex.TableBody";
            this.textBox56.Value = "= Fields.APC_VARIANCE";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox58.StyleName = "Apex.TableBody";
            this.textBox58.Value = "Code Totals";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1875011920928955D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox32.StyleName = "Apex.TableBody";
            this.textBox32.Value = "= Fields.RESEQUENCE_COUNT";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437498807907105D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox36.StyleName = "Apex.TableBody";
            this.textBox36.Value = "= Fields.TOTAL_CHANGES";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox37.StyleName = "Apex.TableBody";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7604166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox38.StyleName = "Apex.TableBody";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox39.StyleName = "Apex.TableBody";
            this.textBox39.Value = "Modifiers Totals";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76875084638595581D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox40.StyleName = "Apex.TableBody";
            this.textBox40.Value = "= Fields.MOD_ADD_COUNT";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87291526794433594D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.StyleName = "Apex.TableBody";
            this.textBox42.Value = "= Fields.MOD_CHANGE_COUNT";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93541663885116577D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox44.StyleName = "Apex.TableBody";
            this.textBox44.Value = "= Fields.MOD_DELETE_COUNT";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1875011920928955D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox47.StyleName = "Apex.TableBody";
            this.textBox47.Value = "= Fields.MOD_RESEQUENCE_COUNT";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437504768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox48.StyleName = "Apex.TableBody";
            this.textBox48.Value = "= Fields.MOD_TOTAL_COUNT";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.247913122177124D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox49.StyleName = "Apex.TableBody";
            // 
            // OPMSDetailSql
            // 
            this.OPMSDetailSql.ConnectionString = "THEMISConnectionString";
            this.OPMSDetailSql.Name = "OPMSDetailSql";
            this.OPMSDetailSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "= join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\',Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.OPMSDetailSql.SelectCommand = "dbo.usp_Metis_OP_MasterSummaryTotals_Select";
            this.OPMSDetailSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // crosstab1
            // 
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1324336528778076D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4970180988311768D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0491005182266235D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1324336528778076D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2886835336685181D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1324336528778076D)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3720176219940186D)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.crosstab1.Body.SetCellContent(0, 0, this.textBox76);
            this.crosstab1.Body.SetCellContent(0, 1, this.textBox74);
            this.crosstab1.Body.SetCellContent(0, 2, this.textBox51);
            this.crosstab1.Body.SetCellContent(0, 3, this.textBox52);
            this.crosstab1.Body.SetCellContent(0, 4, this.textBox59);
            this.crosstab1.Body.SetCellContent(0, 5, this.textBox60);
            this.crosstab1.Body.SetCellContent(0, 6, this.textBox61);
            tableGroup13.Name = "eNCOUNTER_NUMBER1";
            tableGroup13.ReportItem = this.textBox13;
            tableGroup14.Name = "rEVIEWED_DATE1";
            tableGroup14.ReportItem = this.textBox26;
            tableGroup15.Name = "facilityCoder1";
            tableGroup15.ReportItem = this.textBox75;
            tableGroup16.Name = "facilityApcCode1";
            tableGroup16.ReportItem = this.textBox77;
            tableGroup17.Name = "consultantApcCode1";
            tableGroup17.ReportItem = this.textBox78;
            tableGroup18.Name = "mod1ChangeReason1";
            tableGroup18.ReportItem = this.textBox79;
            tableGroup19.Name = "mod2ChangeReason1";
            tableGroup19.ReportItem = this.textBox80;
            this.crosstab1.ColumnGroups.Add(tableGroup13);
            this.crosstab1.ColumnGroups.Add(tableGroup14);
            this.crosstab1.ColumnGroups.Add(tableGroup15);
            this.crosstab1.ColumnGroups.Add(tableGroup16);
            this.crosstab1.ColumnGroups.Add(tableGroup17);
            this.crosstab1.ColumnGroups.Add(tableGroup18);
            this.crosstab1.ColumnGroups.Add(tableGroup19);
            this.crosstab1.Corner.SetCellContent(0, 0, this.textBox69);
            this.crosstab1.Corner.SetCellContent(0, 1, this.textBox70);
            this.crosstab1.Corner.SetCellContent(0, 2, this.textBox71);
            this.crosstab1.Corner.SetCellContent(0, 3, this.textBox72);
            this.crosstab1.Corner.SetCellContent(0, 4, this.textBox73);
            this.crosstab1.Corner.SetCellContent(0, 5, this.textBox81);
            this.crosstab1.DataSource = this.OPMasterSummarySql;
            this.crosstab1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox81,
            this.textBox76,
            this.textBox74,
            this.textBox51,
            this.textBox52,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox13,
            this.textBox26,
            this.textBox75,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox83});
            this.crosstab1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.14229457080364227D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.crosstab1.Name = "crosstab1";
            this.crosstab1.NoDataMessage = "No Data Found.";
            this.crosstab1.NoDataStyle.Font.Bold = true;
            tableGroup26.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup26.Name = "detail";
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.Name = "group11";
            tableGroup25.ReportItem = this.textBox83;
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ConsultantApcCode"));
            tableGroup24.Name = "consultantApcCode2";
            tableGroup24.ReportItem = this.textBox66;
            tableGroup24.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.ConsultantApcCode", Telerik.Reporting.SortDirection.Asc));
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FacilityApcCode"));
            tableGroup23.Name = "facilityApcCode2";
            tableGroup23.ReportItem = this.textBox65;
            tableGroup23.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.FacilityApcCode", Telerik.Reporting.SortDirection.Asc));
            tableGroup22.ChildGroups.Add(tableGroup23);
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.FacilityCoder"));
            tableGroup22.Name = "facilityCoder2";
            tableGroup22.ReportItem = this.textBox64;
            tableGroup22.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.FacilityCoder", Telerik.Reporting.SortDirection.Asc));
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.REVIEWED_DATE"));
            tableGroup21.Name = "rEVIEWED_DATE2";
            tableGroup21.ReportItem = this.textBox63;
            tableGroup21.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.REVIEWED_DATE", Telerik.Reporting.SortDirection.Asc));
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ENCOUNTER_NUMBER"));
            tableGroup20.Name = "eNCOUNTER_NUMBER2";
            tableGroup20.ReportItem = this.textBox62;
            tableGroup20.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.ENCOUNTER_NUMBER", Telerik.Reporting.SortDirection.Asc));
            this.crosstab1.RowGroups.Add(tableGroup20);
            this.crosstab1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(15.1516752243042D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.crosstab1.StyleName = "Apex.TableNormal";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324325799942017D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox76.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox76.StyleName = "Apex.TableBody";
            this.textBox76.Value = "= Fields.FacilityCode";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4970159530639648D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox74.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox74.StyleName = "Apex.TableBody";
            this.textBox74.Value = "= Fields.ConsultantCode";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0490992069244385D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox51.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox51.StyleName = "Apex.TableBody";
            this.textBox51.Value = "= Fields.ChangeTypeText";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324324607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox52.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox52.StyleName = "Apex.TableBody";
            this.textBox52.Value = "= Fields.ChangeReasonText";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2886824607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox59.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox59.StyleName = "Apex.TableBody";
            this.textBox59.Value = "= Fields.OP_Mod1_Mod2_Type";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1324324607849121D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox60.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox60.StyleName = "Apex.TableBody";
            this.textBox60.Value = "= Fields.Mod1ChangeReason";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3720158338546753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox61.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox61.StyleName = "Apex.TableBody";
            this.textBox61.Value = "= Fields.Mod2ChangeReason";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0595108270645142D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox69.StyleName = "Apex.TableHeader";
            this.textBox69.Value = "Account";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81993061304092407D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox70.StyleName = "Apex.TableHeader";
            this.textBox70.Value = "Reviewed Date";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1011812686920166D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox71.StyleName = "Apex.TableHeader";
            this.textBox71.Value = "Facility Coder";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3511794805526733D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox72.StyleName = "Apex.TableHeader";
            this.textBox72.Value = "Facility APC ";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2157644033432007D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox73.StyleName = "Apex.TableHeader";
            this.textBox73.Value = "Reviewer APC";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9999881386756897D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox81.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox81.StyleName = "Apex.TableHeader";
            this.textBox81.Value = "Variance";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // OPMasterSummary
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "OPMasterSummary";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "";
            reportParameter3.Visible = true;
            reportParameter4.AllowBlank = false;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.Value = "";
            reportParameter5.Visible = true;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "ChartTypeName";
            reportParameter6.Value = "-1";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "IcdType";
            reportParameter7.Value = "2";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "Startdate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "EndDate";
            reportParameter9.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter9.Value = "";
            reportParameter9.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableGroup")});
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector3});
            styleRule5.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Font.Name = "Book Antiqua";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(15.518738746643066D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource OPMasterSummarySql;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.SqlDataSource OPMSDetailSql;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.Crosstab crosstab1;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox83;
    }
}