namespace ReportsLibrary.ThemisReports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for Inpatient_Overall_Accuracy.
    /// </summary>
    public partial class Inpatient_Overall_Accuracy : Telerik.Reporting.Report
    {
        public Inpatient_Overall_Accuracy()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}