namespace ReportsLibrary.ThemisReports
{
    partial class OPUnderlyingCauseOfError
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OPUnderlyingCauseOfError));
            Telerik.Reporting.GraphGroup graphGroup1 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle1 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale1 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale1 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.GraphGroup graphGroup2 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup3 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle2 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale2 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale2 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.GraphGroup graphGroup4 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.GraphGroup graphGroup5 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle3 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale3 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale3 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.GraphGroup graphGroup6 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.GraphGroup graphGroup7 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup10 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphTitle graphTitle4 = new Telerik.Reporting.GraphTitle();
            Telerik.Reporting.NumericalScale numericalScale4 = new Telerik.Reporting.NumericalScale();
            Telerik.Reporting.CategoryScale categoryScale4 = new Telerik.Reporting.CategoryScale();
            Telerik.Reporting.GraphGroup graphGroup8 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.GraphGroup graphGroup9 = new Telerik.Reporting.GraphGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector3 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule6 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule7 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector4 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule8 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector5 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule9 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector6 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.FacilitySql = new Telerik.Reporting.SqlDataSource();
            this.RoutingSql = new Telerik.Reporting.SqlDataSource();
            this.ChartTypesql = new Telerik.Reporting.SqlDataSource();
            this.CoderSql = new Telerik.Reporting.SqlDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.graph1 = new Telerik.Reporting.Graph();
            this.polarCoordinateSystem1 = new Telerik.Reporting.PolarCoordinateSystem();
            this.graphAxis1 = new Telerik.Reporting.GraphAxis();
            this.graphAxis2 = new Telerik.Reporting.GraphAxis();
            this.OPCMSql = new Telerik.Reporting.SqlDataSource();
            this.barSeries8 = new Telerik.Reporting.BarSeries();
            this.graph2 = new Telerik.Reporting.Graph();
            this.polarCoordinateSystem2 = new Telerik.Reporting.PolarCoordinateSystem();
            this.graphAxis3 = new Telerik.Reporting.GraphAxis();
            this.graphAxis4 = new Telerik.Reporting.GraphAxis();
            this.CptsUnderLyingErrorSql = new Telerik.Reporting.SqlDataSource();
            this.barSeries1 = new Telerik.Reporting.BarSeries();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.PcUnderLyingErrorsql = new Telerik.Reporting.SqlDataSource();
            this.PcGraph = new Telerik.Reporting.Graph();
            this.polarCoordinateSystem3 = new Telerik.Reporting.PolarCoordinateSystem();
            this.graphAxis5 = new Telerik.Reporting.GraphAxis();
            this.graphAxis6 = new Telerik.Reporting.GraphAxis();
            this.barSeries2 = new Telerik.Reporting.BarSeries();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.MODSql = new Telerik.Reporting.SqlDataSource();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.Modgraph = new Telerik.Reporting.Graph();
            this.polarCoordinateSystem4 = new Telerik.Reporting.PolarCoordinateSystem();
            this.graphAxis7 = new Telerik.Reporting.GraphAxis();
            this.graphAxis8 = new Telerik.Reporting.GraphAxis();
            this.barSeries3 = new Telerik.Reporting.BarSeries();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox28.StyleName = "Apex.TableHeader";
            this.textBox28.Value = "Change Reason";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.643750011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox30.StyleName = "Apex.TableHeader";
            this.textBox30.Value = "Add";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333326578140259D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox31.StyleName = "Apex.TableHeader";
            this.textBox31.Value = "Change";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50833302736282349D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox32.StyleName = "Apex.TableHeader";
            this.textBox32.Value = "Delete";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.83124983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox33.StyleName = "Apex.TableHeader";
            this.textBox33.Value = "Resequence";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62499964237213135D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox34.StyleName = "Apex.TableHeader";
            this.textBox34.Value = "Total";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox35.StyleName = "Apex.TableHeader";
            this.textBox35.Value = "Percent";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.7291646003723145D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox40.Style.Color = System.Drawing.Color.White;
            this.textBox40.StyleName = "Apex.TableHeader";
            this.textBox40.Value = "ICD-10 CM Underlying Cause of Error";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox19.StyleName = "Apex.TableHeader";
            this.textBox19.Value = "Change Reason";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.56041646003723145D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox21.StyleName = "Apex.TableHeader";
            this.textBox21.Value = "Add";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6437498927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox22.StyleName = "Apex.TableHeader";
            this.textBox22.Value = "Change";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54999983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox29.StyleName = "Apex.TableHeader";
            this.textBox29.Value = "Delete";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583295583724976D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox37.StyleName = "Apex.TableHeader";
            this.textBox37.Value = "Total";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox38.StyleName = "Apex.TableHeader";
            this.textBox38.Value = "Percent";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.8875002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox54.Style.Color = System.Drawing.Color.White;
            this.textBox54.StyleName = "Apex.TableHeader";
            this.textBox54.Value = "CPT/HCPCPS Underlying Cause of Error";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox57.StyleName = "Apex.TableHeader";
            this.textBox57.Value = "Change Reason";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.51875030994415283D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox58.StyleName = "Apex.TableHeader";
            this.textBox58.Value = "Add";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57083404064178467D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox59.StyleName = "Apex.TableHeader";
            this.textBox59.Value = "Change";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85208350419998169D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox60.StyleName = "Apex.TableHeader";
            this.textBox60.Value = "Delete";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71874898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox62.StyleName = "Apex.TableHeader";
            this.textBox62.Value = "Total";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox63.StyleName = "Apex.TableHeader";
            this.textBox63.Value = "Percent";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.4604167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox61.Style.Color = System.Drawing.Color.White;
            this.textBox61.StyleName = "Apex.TableHeader";
            this.textBox61.Value = "ICD-10 PCS Underlying Cause of Error";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox77.StyleName = "Apex.TableHeader";
            this.textBox77.Value = "Change Reason";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37291643023490906D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox78.StyleName = "Apex.TableHeader";
            this.textBox78.Value = "Add";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52916634082794189D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox79.StyleName = "Apex.TableHeader";
            this.textBox79.Value = "Change";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.529166042804718D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox80.StyleName = "Apex.TableHeader";
            this.textBox80.Value = "Delete";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5833325982093811D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox81.StyleName = "Apex.TableHeader";
            this.textBox81.Value = "Total";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333224058151245D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox82.StyleName = "Apex.TableHeader";
            this.textBox82.Value = "Percent";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0854134559631348D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox84.Style.Color = System.Drawing.Color.White;
            this.textBox84.StyleName = "Apex.TableHeader";
            this.textBox84.Value = "CPT/HCPCS Modifier Underlying Cause of Error";
            // 
            // FacilitySql
            // 
            this.FacilitySql.CommandTimeout = 0;
            this.FacilitySql.ConnectionString = "THEMISConnectionString";
            this.FacilitySql.Name = "FacilitySql";
            this.FacilitySql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@UserId", System.Data.DbType.Int32, "= Parameters.UserId.Value")});
            this.FacilitySql.SelectCommand = "dbo.usp_Metis_FacilitiesForUser_Select";
            this.FacilitySql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // RoutingSql
            // 
            this.RoutingSql.CommandTimeout = 0;
            this.RoutingSql.ConnectionString = "THEMISConnectionString";
            this.RoutingSql.Name = "RoutingSql";
            this.RoutingSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RoutingType", System.Data.DbType.Int32, "= Parameters.Routing.Value")});
            this.RoutingSql.SelectCommand = "dbo.usp_ChartRouting_Select";
            this.RoutingSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // ChartTypesql
            // 
            this.ChartTypesql.CommandTimeout = 0;
            this.ChartTypesql.ConnectionString = "THEMISConnectionString";
            this.ChartTypesql.Name = "ChartTypesql";
            this.ChartTypesql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequestCHART_TYPE", System.Data.DbType.String, "= Parameters.ChartTypeName.Value")});
            this.ChartTypesql.SelectCommand = resources.GetString("ChartTypesql.SelectCommand");
            // 
            // CoderSql
            // 
            this.CoderSql.CommandTimeout = 0;
            this.CoderSql.ConnectionString = "THEMISConnectionString";
            this.CoderSql.Name = "CoderSql";
            this.CoderSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@RequesticlFacilities", System.Data.DbType.String, "=join(\',\', Parameters.FacilityName.Value)")});
            this.CoderSql.SelectCommand = resources.GetString("CoderSql.SelectCommand");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20003931224346161D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.99999994039535522D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5854167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Segoe UI";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox12.Style.Font.Underline = true;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "OutPatient Underlying Cause Of Error";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(16.200908660888672D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.graph1,
            this.graph2,
            this.textBox7,
            this.textBox8,
            this.table2,
            this.table1,
            this.table3,
            this.PcGraph,
            this.textBox64,
            this.table4,
            this.textBox83,
            this.Modgraph});
            this.detail.Name = "detail";
            // 
            // graph1
            // 
            graphGroup1.Name = "catgorygroup";
            this.graph1.CategoryGroups.Add(graphGroup1);
            this.graph1.CoordinateSystems.Add(this.polarCoordinateSystem1);
            this.graph1.DataSource = this.OPCMSql;
            this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.0001970529556274D));
            this.graph1.Name = "graph1";
            this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph1.Series.Add(this.barSeries8);
            this.graph1.SeriesGroups.Add(graphGroup2);
            this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(2.9000000953674316D));
            this.graph1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle1.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle1.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle1.Style.Visible = false;
            graphTitle1.Text = "graph1";
            this.graph1.Titles.Add(graphTitle1);
            // 
            // polarCoordinateSystem1
            // 
            this.polarCoordinateSystem1.AngularAxis = this.graphAxis1;
            this.polarCoordinateSystem1.Name = "polarCoordinateSystem1";
            this.polarCoordinateSystem1.RadialAxis = this.graphAxis2;
            // 
            // graphAxis1
            // 
            this.graphAxis1.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MajorGridLineStyle.Visible = false;
            this.graphAxis1.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis1.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis1.MinorGridLineStyle.Visible = false;
            this.graphAxis1.Name = "graphAxis1";
            this.graphAxis1.Scale = numericalScale1;
            this.graphAxis1.Style.Visible = false;
            // 
            // graphAxis2
            // 
            this.graphAxis2.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MajorGridLineStyle.Visible = false;
            this.graphAxis2.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis2.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis2.MinorGridLineStyle.Visible = false;
            this.graphAxis2.Name = "graphAxis2";
            categoryScale1.PositionMode = Telerik.Reporting.AxisPositionMode.OnTicks;
            categoryScale1.SpacingSlotCount = 0D;
            this.graphAxis2.Scale = categoryScale1;
            this.graphAxis2.Style.Visible = false;
            // 
            // OPCMSql
            // 
            this.OPCMSql.ConnectionString = "THEMISConnectionString";
            this.OPCMSql.Name = "OPCMSql";
            this.OPCMSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "=join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "=join(\',\', Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "=join(\',\',Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@CodeTypeId", System.Data.DbType.Int32, "1")});
            this.OPCMSql.SelectCommand = "dbo.usp_Metis_UnderlyingCauseOfError_Select";
            this.OPCMSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // barSeries8
            // 
            this.barSeries8.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Stacked100;
            this.barSeries8.CategoryGroup = graphGroup1;
            this.barSeries8.CoordinateSystem = this.polarCoordinateSystem1;
            this.barSeries8.DataPointLabel = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete)+ sum(Fields.Resequenc" +
    "e))*10000.0/Exec(\'graph1\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)+" +
    "sum(Fields.Resequence)))/100";
            this.barSeries8.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.InsideEnd;
            this.barSeries8.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries8.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries8.DataPointLabelFormat = "{0:# ,##0.00}%";
            this.barSeries8.LegendItem.Value = "= Fields.DISPLAY_TEXT";
            graphGroup2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_TEXT"));
            graphGroup2.Name = "seriesgroup";
            this.barSeries8.SeriesGroup = graphGroup2;
            this.barSeries8.X = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete)+ sum(Fields.Resequenc" +
    "e))*10000.0/Exec(\'graph1\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)+" +
    "sum(Fields.Resequence)))/100";
            // 
            // graph2
            // 
            graphGroup3.Name = "CatgoryGroup";
            this.graph2.CategoryGroups.Add(graphGroup3);
            this.graph2.CoordinateSystems.Add(this.polarCoordinateSystem2);
            this.graph2.DataSource = this.CptsUnderLyingErrorSql;
            this.graph2.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.graph2.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(4.9004340171813965D));
            this.graph2.Name = "graph2";
            this.graph2.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.graph2.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.graph2.Series.Add(this.barSeries1);
            this.graph2.SeriesGroups.Add(graphGroup4);
            this.graph2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D));
            this.graph2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.graph2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle2.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle2.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle2.Style.Visible = false;
            graphTitle2.Text = "graph2";
            this.graph2.Titles.Add(graphTitle2);
            // 
            // polarCoordinateSystem2
            // 
            this.polarCoordinateSystem2.AngularAxis = this.graphAxis3;
            this.polarCoordinateSystem2.Name = "polarCoordinateSystem2";
            this.polarCoordinateSystem2.RadialAxis = this.graphAxis4;
            // 
            // graphAxis3
            // 
            this.graphAxis3.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MajorGridLineStyle.Visible = false;
            this.graphAxis3.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis3.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis3.MinorGridLineStyle.Visible = false;
            this.graphAxis3.Name = "graphAxis3";
            this.graphAxis3.Scale = numericalScale2;
            this.graphAxis3.Style.Visible = false;
            // 
            // graphAxis4
            // 
            this.graphAxis4.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MajorGridLineStyle.Visible = false;
            this.graphAxis4.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis4.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis4.MinorGridLineStyle.Visible = false;
            this.graphAxis4.Name = "graphAxis4";
            categoryScale2.PositionMode = Telerik.Reporting.AxisPositionMode.OnTicks;
            categoryScale2.SpacingSlotCount = 0D;
            this.graphAxis4.Scale = categoryScale2;
            this.graphAxis4.Style.Visible = false;
            // 
            // CptsUnderLyingErrorSql
            // 
            this.CptsUnderLyingErrorSql.ConnectionString = "THEMISConnectionString";
            this.CptsUnderLyingErrorSql.Name = "CptsUnderLyingErrorSql";
            this.CptsUnderLyingErrorSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "2"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "=join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.CptsUnderLyingErrorSql.SelectCommand = "dbo.usp_Metis_OP_CPTCauseOfError_Select";
            this.CptsUnderLyingErrorSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // barSeries1
            // 
            this.barSeries1.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Stacked100;
            this.barSeries1.CategoryGroup = graphGroup3;
            this.barSeries1.CoordinateSystem = this.polarCoordinateSystem2;
            this.barSeries1.DataPointLabel = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete))*10000.0/Exec(\'graph2" +
    "\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)))/100";
            this.barSeries1.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.InsideEnd;
            this.barSeries1.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries1.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries1.DataPointLabelFormat = "{0:# ,##0.00}%";
            this.barSeries1.LegendItem.Value = "= Fields.DISPLAY_TEXT";
            graphGroup4.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_TEXT"));
            graphGroup4.Name = "SeriesGroup";
            this.barSeries1.SeriesGroup = graphGroup4;
            this.barSeries1.X = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete))*10000.0/Exec(\'graph2" +
    "\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)))/100";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.8999218940734863D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Value = "ICD-10 CM Underlying Cause of Error: The table below shows the number of code cha" +
    "nges used to calculate the ICD-10 CM coding accuracy rates";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.041706085205078125D), Telerik.Reporting.Drawing.Unit.Inch(3.9002761840820312D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.8582944869995117D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Value = "CPT/HCPCPS Underlying Cause of Error: The table below shows the number of code ch" +
    "anges used to calculate the CPT/HCPCS coding accuracy rates.";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4875006675720215D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.64374959468841553D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63333308696746826D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.50833284854888916D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.83124911785125732D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.62499958276748657D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.999999463558197D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox16);
            this.table2.Body.SetCellContent(0, 1, this.textBox20);
            this.table2.Body.SetCellContent(0, 2, this.textBox23);
            this.table2.Body.SetCellContent(0, 3, this.textBox24);
            this.table2.Body.SetCellContent(0, 4, this.textBox25);
            this.table2.Body.SetCellContent(0, 5, this.textBox26);
            this.table2.Body.SetCellContent(0, 6, this.textBox27);
            this.table2.Body.SetCellContent(1, 0, this.textBox46);
            this.table2.Body.SetCellContent(1, 1, this.textBox47);
            this.table2.Body.SetCellContent(1, 2, this.textBox48);
            this.table2.Body.SetCellContent(1, 3, this.textBox49);
            this.table2.Body.SetCellContent(1, 4, this.textBox50);
            this.table2.Body.SetCellContent(1, 5, this.textBox53);
            this.table2.Body.SetCellContent(1, 6, this.textBox51);
            tableGroup2.Name = "group";
            tableGroup2.ReportItem = this.textBox28;
            tableGroup3.Name = "group1";
            tableGroup3.ReportItem = this.textBox30;
            tableGroup4.Name = "group2";
            tableGroup4.ReportItem = this.textBox31;
            tableGroup5.Name = "group3";
            tableGroup5.ReportItem = this.textBox32;
            tableGroup6.Name = "group4";
            tableGroup6.ReportItem = this.textBox33;
            tableGroup7.Name = "group8";
            tableGroup7.ReportItem = this.textBox34;
            tableGroup8.Name = "group11";
            tableGroup8.ReportItem = this.textBox35;
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.ChildGroups.Add(tableGroup4);
            tableGroup1.ChildGroups.Add(tableGroup5);
            tableGroup1.ChildGroups.Add(tableGroup6);
            tableGroup1.ChildGroups.Add(tableGroup7);
            tableGroup1.ChildGroups.Add(tableGroup8);
            tableGroup1.Name = "dISPLAY_TEXT";
            tableGroup1.ReportItem = this.textBox40;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.DataSource = this.OPCMSql;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox20,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox53,
            this.textBox51,
            this.textBox40,
            this.textBox28,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.20011822879314423D));
            this.table2.Name = "table2";
            this.table2.NoDataMessage = "No Data Found.";
            this.table2.NoDataStyle.Font.Bold = true;
            tableGroup10.Name = "group7";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.GroupKeepTogether = true;
            tableGroup9.Name = "detail";
            tableGroup12.Name = "group10";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Name = "group9";
            this.table2.RowGroups.Add(tableGroup9);
            this.table2.RowGroups.Add(tableGroup11);
            this.table2.RowHeadersPrintOnEveryPage = true;
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.7291646003723145D), Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D));
            this.table2.StyleName = "";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox16.StyleName = "Apex.TableBody";
            this.textBox16.Value = "= Fields.DISPLAY_TEXT";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.643750011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox20.StyleName = "Apex.TableBody";
            this.textBox20.Value = "= Fields.Add";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333326578140259D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox23.StyleName = "Apex.TableBody";
            this.textBox23.Value = "= Fields.Change";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50833302736282349D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox24.StyleName = "Apex.TableBody";
            this.textBox24.Value = "= Fields.Delete";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.83124983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox25.StyleName = "Apex.TableBody";
            this.textBox25.Value = "= Fields.Resequence";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62499964237213135D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox26.StyleName = "Apex.TableBody";
            this.textBox26.Value = "= Fields.Add+ Fields.Change+ Fields.Delete+ Fields.Resequence";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox27.StyleName = "Apex.TableBody";
            this.textBox27.Value = resources.GetString("textBox27.Value");
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox46.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox46.StyleName = "Apex.TableBody";
            this.textBox46.Value = "Total";
            // 
            // textBox47
            // 
            this.textBox47.Format = "";
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.643750011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox47.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox47.StyleName = "Apex.TableBody";
            this.textBox47.Value = "=sum(Fields.Add)";
            // 
            // textBox48
            // 
            this.textBox48.Format = "";
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63333326578140259D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox48.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox48.StyleName = "Apex.TableBody";
            this.textBox48.Value = "=sum(Fields.Change)";
            // 
            // textBox49
            // 
            this.textBox49.Format = "";
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50833302736282349D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox49.StyleName = "Apex.TableBody";
            this.textBox49.Value = "=sum(Fields.Delete)";
            // 
            // textBox50
            // 
            this.textBox50.Format = "";
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.83124983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox50.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox50.StyleName = "Apex.TableBody";
            this.textBox50.Value = "=Sum(Fields.Resequence)";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62499964237213135D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox53.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox53.StyleName = "Apex.TableBody";
            this.textBox53.Value = "=sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)+sum(Fields.Resequence)";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox51.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox51.StyleName = "Apex.TableBody";
            this.textBox51.Value = resources.GetString("textBox51.Value");
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4875015020370483D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.56041675806045532D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.64375013113021851D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.54999971389770508D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.645832896232605D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.999999463558197D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox1);
            this.table1.Body.SetCellContent(0, 1, this.textBox2);
            this.table1.Body.SetCellContent(0, 2, this.textBox3);
            this.table1.Body.SetCellContent(0, 3, this.textBox4);
            this.table1.Body.SetCellContent(0, 4, this.textBox6);
            this.table1.Body.SetCellContent(0, 5, this.textBox9);
            this.table1.Body.SetCellContent(1, 0, this.textBox10);
            this.table1.Body.SetCellContent(1, 1, this.textBox11);
            this.table1.Body.SetCellContent(1, 2, this.textBox13);
            this.table1.Body.SetCellContent(1, 3, this.textBox14);
            this.table1.Body.SetCellContent(1, 4, this.textBox17);
            this.table1.Body.SetCellContent(1, 5, this.textBox18);
            tableGroup14.Name = "group12";
            tableGroup14.ReportItem = this.textBox19;
            tableGroup15.Name = "group13";
            tableGroup15.ReportItem = this.textBox21;
            tableGroup16.Name = "group14";
            tableGroup16.ReportItem = this.textBox22;
            tableGroup17.Name = "group15";
            tableGroup17.ReportItem = this.textBox29;
            tableGroup18.Name = "group16";
            tableGroup18.ReportItem = this.textBox37;
            tableGroup19.Name = "group17";
            tableGroup19.ReportItem = this.textBox38;
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.ChildGroups.Add(tableGroup16);
            tableGroup13.ChildGroups.Add(tableGroup17);
            tableGroup13.ChildGroups.Add(tableGroup18);
            tableGroup13.ChildGroups.Add(tableGroup19);
            tableGroup13.Name = "dISPLAY_TEXT";
            tableGroup13.ReportItem = this.textBox54;
            this.table1.ColumnGroups.Add(tableGroup13);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.DataSource = this.CptsUnderLyingErrorSql;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox6,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox13,
            this.textBox14,
            this.textBox17,
            this.textBox18,
            this.textBox54,
            this.textBox19,
            this.textBox21,
            this.textBox22,
            this.textBox29,
            this.textBox37,
            this.textBox38});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.10035514831543D));
            this.table1.Name = "table1";
            this.table1.NoDataMessage = "No Data Found.";
            this.table1.NoDataStyle.Font.Bold = true;
            tableGroup21.Name = "group7";
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.GroupKeepTogether = true;
            tableGroup20.Name = "detail";
            tableGroup23.Name = "group10";
            tableGroup22.ChildGroups.Add(tableGroup23);
            tableGroup22.Name = "group9";
            this.table1.RowGroups.Add(tableGroup20);
            this.table1.RowGroups.Add(tableGroup22);
            this.table1.RowHeadersPrintOnEveryPage = true;
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.8875002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D));
            this.table1.StyleName = "";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox1.StyleName = "Apex.TableBody";
            this.textBox1.Value = "= Fields.DISPLAY_TEXT";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.56041646003723145D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox2.StyleName = "Apex.TableBody";
            this.textBox2.Value = "= Fields.Add";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6437498927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox3.StyleName = "Apex.TableBody";
            this.textBox3.Value = "= Fields.Change";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54999983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox4.StyleName = "Apex.TableBody";
            this.textBox4.Value = "= Fields.Delete";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583295583724976D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox6.StyleName = "Apex.TableBody";
            this.textBox6.Value = "= Fields.Add+ Fields.Change+ Fields.Delete";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox9.StyleName = "Apex.TableBody";
            this.textBox9.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete))*10000" +
    ")/Exec(\'table1\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)))/100)+\'%\'" +
    "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox10.StyleName = "Apex.TableBody";
            this.textBox10.Value = "Total";
            // 
            // textBox11
            // 
            this.textBox11.Format = "";
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.56041646003723145D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox11.StyleName = "Apex.TableBody";
            this.textBox11.Value = "=sum(Fields.Add)";
            // 
            // textBox13
            // 
            this.textBox13.Format = "";
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6437498927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox13.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox13.StyleName = "Apex.TableBody";
            this.textBox13.Value = "=sum(Fields.Change)";
            // 
            // textBox14
            // 
            this.textBox14.Format = "";
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54999983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox14.StyleName = "Apex.TableBody";
            this.textBox14.Value = "=sum(Fields.Delete)";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583295583724976D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox17.StyleName = "Apex.TableBody";
            this.textBox17.Value = "=sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox18.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox18.StyleName = "Apex.TableBody";
            this.textBox18.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete))*10000" +
    ")/Exec(\'table1\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)))/100)+\'%\'" +
    "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.80000108480453491D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.51875030994415283D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.57083410024642944D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.85208380222320557D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71874916553497314D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.99999910593032837D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox5);
            this.table3.Body.SetCellContent(0, 1, this.textBox15);
            this.table3.Body.SetCellContent(0, 2, this.textBox36);
            this.table3.Body.SetCellContent(0, 3, this.textBox39);
            this.table3.Body.SetCellContent(0, 4, this.textBox41);
            this.table3.Body.SetCellContent(0, 5, this.textBox42);
            this.table3.Body.SetCellContent(1, 0, this.textBox43);
            this.table3.Body.SetCellContent(1, 1, this.textBox44);
            this.table3.Body.SetCellContent(1, 2, this.textBox45);
            this.table3.Body.SetCellContent(1, 3, this.textBox52);
            this.table3.Body.SetCellContent(1, 4, this.textBox55);
            this.table3.Body.SetCellContent(1, 5, this.textBox56);
            tableGroup25.Name = "group18";
            tableGroup25.ReportItem = this.textBox57;
            tableGroup26.Name = "group19";
            tableGroup26.ReportItem = this.textBox58;
            tableGroup27.Name = "group20";
            tableGroup27.ReportItem = this.textBox59;
            tableGroup28.Name = "group21";
            tableGroup28.ReportItem = this.textBox60;
            tableGroup29.Name = "group22";
            tableGroup29.ReportItem = this.textBox62;
            tableGroup30.Name = "group23";
            tableGroup30.ReportItem = this.textBox63;
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.ChildGroups.Add(tableGroup26);
            tableGroup24.ChildGroups.Add(tableGroup27);
            tableGroup24.ChildGroups.Add(tableGroup28);
            tableGroup24.ChildGroups.Add(tableGroup29);
            tableGroup24.ChildGroups.Add(tableGroup30);
            tableGroup24.Name = "dISPLAY_TEXT";
            tableGroup24.ReportItem = this.textBox61;
            this.table3.ColumnGroups.Add(tableGroup24);
            this.table3.ColumnHeadersPrintOnEveryPage = true;
            this.table3.DataSource = this.PcUnderLyingErrorsql;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox15,
            this.textBox36,
            this.textBox39,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox52,
            this.textBox55,
            this.textBox56,
            this.textBox61,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox62,
            this.textBox63});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(8.200592041015625D));
            this.table3.Name = "table3";
            this.table3.NoDataMessage = "No Data Found.";
            this.table3.NoDataStyle.Font.Bold = true;
            tableGroup32.Name = "group7";
            tableGroup31.ChildGroups.Add(tableGroup32);
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.GroupKeepTogether = true;
            tableGroup31.Name = "detail";
            tableGroup34.Name = "group10";
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.Name = "group9";
            this.table3.RowGroups.Add(tableGroup31);
            this.table3.RowGroups.Add(tableGroup33);
            this.table3.RowHeadersPrintOnEveryPage = true;
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.4604172706604D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table3.StyleName = "";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.StyleName = "Apex.TableBody";
            this.textBox5.Value = "= Fields.DISPLAY_TEXT";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.51875030994415283D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox15.StyleName = "Apex.TableBody";
            this.textBox15.Value = "= Fields.Add";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57083404064178467D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox36.StyleName = "Apex.TableBody";
            this.textBox36.Value = "= Fields.Change";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85208350419998169D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox39.StyleName = "Apex.TableBody";
            this.textBox39.Value = "= Fields.Delete";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71874898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox41.StyleName = "Apex.TableBody";
            this.textBox41.Value = "= Fields.Add+ Fields.Change+ Fields.Delete";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.StyleName = "Apex.TableBody";
            this.textBox42.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)+Sum(Fi" +
    "elds.Resequence))*10000)/Exec(\'table3\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fi" +
    "elds.Delete)))/100)+\'%\'\r\n";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000013113021851D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox43.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox43.StyleName = "Apex.TableBody";
            this.textBox43.Value = "Total";
            // 
            // textBox44
            // 
            this.textBox44.Format = "";
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.51875030994415283D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox44.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox44.StyleName = "Apex.TableBody";
            this.textBox44.Value = "=sum(Fields.Add)";
            // 
            // textBox45
            // 
            this.textBox45.Format = "";
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57083404064178467D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox45.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox45.StyleName = "Apex.TableBody";
            this.textBox45.Value = "=sum(Fields.Change)";
            // 
            // textBox52
            // 
            this.textBox52.Format = "";
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85208350419998169D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox52.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox52.StyleName = "Apex.TableBody";
            this.textBox52.Value = "=sum(Fields.Delete)";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71874898672103882D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox55.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox55.StyleName = "Apex.TableBody";
            this.textBox55.Value = "=sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox56.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox56.StyleName = "Apex.TableBody";
            this.textBox56.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)+Sum(Fi" +
    "elds.Resequence))*10000)/Exec(\'table3\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fi" +
    "elds.Delete)))/100)+\'%\'\r\n";
            // 
            // PcUnderLyingErrorsql
            // 
            this.PcUnderLyingErrorsql.ConnectionString = "THEMISConnectionString";
            this.PcUnderLyingErrorsql.Name = "PcUnderLyingErrorsql";
            this.PcUnderLyingErrorsql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.String, "=join(\',\',Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.String, "= Parameters.IcdType.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.String, "=join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.String, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.String, "=join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@CodeTypeId", System.Data.DbType.Int32, "2")});
            this.PcUnderLyingErrorsql.SelectCommand = "dbo.usp_Metis_UnderlyingCauseOfError_Select";
            this.PcUnderLyingErrorsql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // PcGraph
            // 
            graphGroup5.Name = "CatgoryGroup";
            this.PcGraph.CategoryGroups.Add(graphGroup5);
            this.PcGraph.CoordinateSystems.Add(this.polarCoordinateSystem3);
            this.PcGraph.DataSource = this.PcUnderLyingErrorsql;
            this.PcGraph.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.PcGraph.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PcGraph.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(9.0006704330444336D));
            this.PcGraph.Name = "PcGraph";
            this.PcGraph.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.PcGraph.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PcGraph.Series.Add(this.barSeries2);
            this.PcGraph.SeriesGroups.Add(graphGroup6);
            this.PcGraph.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(3.0999996662139893D));
            this.PcGraph.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.PcGraph.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.PcGraph.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.PcGraph.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle3.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle3.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle3.Text = "";
            this.PcGraph.Titles.Add(graphTitle3);
            // 
            // polarCoordinateSystem3
            // 
            this.polarCoordinateSystem3.AngularAxis = this.graphAxis5;
            this.polarCoordinateSystem3.Name = "polarCoordinateSystem2";
            this.polarCoordinateSystem3.RadialAxis = this.graphAxis6;
            // 
            // graphAxis5
            // 
            this.graphAxis5.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MajorGridLineStyle.Visible = false;
            this.graphAxis5.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis5.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis5.MinorGridLineStyle.Visible = false;
            this.graphAxis5.Name = "graphAxis3";
            this.graphAxis5.Scale = numericalScale3;
            this.graphAxis5.Style.Visible = false;
            // 
            // graphAxis6
            // 
            this.graphAxis6.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MajorGridLineStyle.Visible = false;
            this.graphAxis6.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis6.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis6.MinorGridLineStyle.Visible = false;
            this.graphAxis6.Name = "graphAxis4";
            categoryScale3.PositionMode = Telerik.Reporting.AxisPositionMode.OnTicks;
            categoryScale3.SpacingSlotCount = 0D;
            this.graphAxis6.Scale = categoryScale3;
            this.graphAxis6.Style.Visible = false;
            // 
            // barSeries2
            // 
            this.barSeries2.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Stacked100;
            this.barSeries2.CategoryGroup = graphGroup5;
            this.barSeries2.CoordinateSystem = this.polarCoordinateSystem3;
            this.barSeries2.DataPointLabel = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete)+ sum(Fields.Resequenc" +
    "e))*10000.0/Exec(\'Pcgraph\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)" +
    "+sum(Fields.Resequence)))/100";
            this.barSeries2.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries2.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries2.DataPointLabelFormat = "{0:#,##0.00}%";
            this.barSeries2.LegendItem.Value = "= Fields.DISPLAY_TEXT";
            graphGroup6.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_TEXT"));
            graphGroup6.Name = "SeriesGroup";
            this.barSeries2.SeriesGroup = graphGroup6;
            this.barSeries2.X = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete)+ sum(Fields.Resequenc" +
    "e))*10000.0/Exec(\'Pcgraph\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)" +
    "+sum(Fields.Resequence)))/100";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(8.0005130767822266D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.5000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Value = "ICD-10 PCS Underlying Cause of Error: The table below shows the number of code ch" +
    "anges used to calculate the ICD-10 PCS coding accuracy rates.";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4874999523162842D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.37291660904884338D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.52916634082794189D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.52916598320007324D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.58333271741867065D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.58333230018615723D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox65);
            this.table4.Body.SetCellContent(0, 1, this.textBox66);
            this.table4.Body.SetCellContent(0, 2, this.textBox67);
            this.table4.Body.SetCellContent(0, 3, this.textBox68);
            this.table4.Body.SetCellContent(0, 4, this.textBox69);
            this.table4.Body.SetCellContent(0, 5, this.textBox70);
            this.table4.Body.SetCellContent(1, 0, this.textBox71);
            this.table4.Body.SetCellContent(1, 1, this.textBox72);
            this.table4.Body.SetCellContent(1, 2, this.textBox73);
            this.table4.Body.SetCellContent(1, 3, this.textBox74);
            this.table4.Body.SetCellContent(1, 4, this.textBox75);
            this.table4.Body.SetCellContent(1, 5, this.textBox76);
            tableGroup36.Name = "group24";
            tableGroup36.ReportItem = this.textBox77;
            tableGroup37.Name = "group25";
            tableGroup37.ReportItem = this.textBox78;
            tableGroup38.Name = "group26";
            tableGroup38.ReportItem = this.textBox79;
            tableGroup39.Name = "group27";
            tableGroup39.ReportItem = this.textBox80;
            tableGroup40.Name = "group28";
            tableGroup40.ReportItem = this.textBox81;
            tableGroup41.Name = "group29";
            tableGroup41.ReportItem = this.textBox82;
            tableGroup35.ChildGroups.Add(tableGroup36);
            tableGroup35.ChildGroups.Add(tableGroup37);
            tableGroup35.ChildGroups.Add(tableGroup38);
            tableGroup35.ChildGroups.Add(tableGroup39);
            tableGroup35.ChildGroups.Add(tableGroup40);
            tableGroup35.ChildGroups.Add(tableGroup41);
            tableGroup35.Name = "dISPLAY_TEXT";
            tableGroup35.ReportItem = this.textBox84;
            this.table4.ColumnGroups.Add(tableGroup35);
            this.table4.ColumnHeadersPrintOnEveryPage = true;
            this.table4.DataSource = this.MODSql;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox84,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(12.300827026367188D));
            this.table4.Name = "table4";
            this.table4.NoDataMessage = "No Data Found.";
            this.table4.NoDataStyle.Font.Bold = true;
            tableGroup43.Name = "group7";
            tableGroup42.ChildGroups.Add(tableGroup43);
            tableGroup42.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup42.GroupKeepTogether = true;
            tableGroup42.Name = "detail";
            tableGroup45.Name = "group10";
            tableGroup44.ChildGroups.Add(tableGroup45);
            tableGroup44.Name = "group9";
            this.table4.RowGroups.Add(tableGroup42);
            this.table4.RowGroups.Add(tableGroup44);
            this.table4.RowHeadersPrintOnEveryPage = true;
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.085413932800293D), Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D));
            this.table4.StyleName = "";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox65.StyleName = "Apex.TableBody";
            this.textBox65.Value = "= Fields.DISPLAY_TEXT";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37291643023490906D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox66.StyleName = "Apex.TableBody";
            this.textBox66.Value = "= Fields.Add";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52916634082794189D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox67.StyleName = "Apex.TableBody";
            this.textBox67.Value = "= Fields.Change";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.529166042804718D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox68.StyleName = "Apex.TableBody";
            this.textBox68.Value = "= Fields.Delete";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5833325982093811D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox69.StyleName = "Apex.TableBody";
            this.textBox69.Value = "= Fields.Add+ Fields.Change+ Fields.Delete";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333224058151245D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox70.StyleName = "Apex.TableBody";
            this.textBox70.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete))*10000" +
    ")/Exec(\'table4\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)))/100)+\'%\'" +
    "";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4875001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox71.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox71.StyleName = "Apex.TableBody";
            this.textBox71.Value = "Total";
            // 
            // textBox72
            // 
            this.textBox72.Format = "";
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37291643023490906D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox72.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox72.StyleName = "Apex.TableBody";
            this.textBox72.Value = "=sum(Fields.Add)";
            // 
            // textBox73
            // 
            this.textBox73.Format = "";
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52916634082794189D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox73.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox73.StyleName = "Apex.TableBody";
            this.textBox73.Value = "=sum(Fields.Change)";
            // 
            // textBox74
            // 
            this.textBox74.Format = "";
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.529166042804718D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox74.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox74.StyleName = "Apex.TableBody";
            this.textBox74.Value = "=sum(Fields.Delete)";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5833325982093811D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox75.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox75.StyleName = "Apex.TableBody";
            this.textBox75.Value = "=sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58333224058151245D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox76.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            this.textBox76.StyleName = "Apex.TableBody";
            this.textBox76.Value = "= Format(\'{0:N2}\',(((Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete))*10000" +
    ")/Exec(\'table4\',Sum(Fields.Add)+Sum(Fields.Change)+Sum(Fields.Delete)))/100)+\'%\'" +
    "";
            // 
            // MODSql
            // 
            this.MODSql.ConnectionString = "THEMISConnectionString";
            this.MODSql.Name = "MODSql";
            this.MODSql.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilities", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.FacilityName.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterIcdTypes", System.Data.DbType.AnsiString, "2"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterChartTypes", System.Data.DbType.AnsiString, "=join(\',\',Parameters.ChartType.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterRoutings", System.Data.DbType.AnsiString, "= Parameters.Routing.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@FilterFacilityCoders", System.Data.DbType.AnsiString, "= Join(\',\', Parameters.Coder.Value)"),
            new Telerik.Reporting.SqlDataSourceParameter("@StartDate", System.Data.DbType.Date, "= Parameters.Startdate.Value"),
            new Telerik.Reporting.SqlDataSourceParameter("@EndDate", System.Data.DbType.Date, "= Parameters.EndDate.Value")});
            this.MODSql.SelectCommand = "dbo.usp_Metis_OP_ModCauseOfError_Select";
            this.MODSql.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(12.100749015808106D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.19999949634075165D));
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Value = "CPT/HCPCS Modifier Underlying Cause of Error: The table below shows the number of" +
    " code changes used to calculate the modifier accuracy rates.";
            // 
            // Modgraph
            // 
            graphGroup7.Name = "CatgoryGroup";
            this.Modgraph.CategoryGroups.Add(graphGroup7);
            this.Modgraph.CoordinateSystems.Add(this.polarCoordinateSystem4);
            this.Modgraph.DataSource = this.MODSql;
            this.Modgraph.Legend.Style.LineColor = System.Drawing.Color.LightGray;
            this.Modgraph.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Modgraph.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(13.100906372070313D));
            this.Modgraph.Name = "Modgraph";
            this.Modgraph.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
            this.Modgraph.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Modgraph.Series.Add(this.barSeries3);
            graphGroup10.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_TEXT"));
            graphGroup10.Name = "SeriesGroup";
            this.Modgraph.SeriesGroups.Add(graphGroup10);
            this.Modgraph.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D));
            this.Modgraph.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.Modgraph.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.Modgraph.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.Modgraph.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            graphTitle4.Position = Telerik.Reporting.GraphItemPosition.TopCenter;
            graphTitle4.Style.LineColor = System.Drawing.Color.LightGray;
            graphTitle4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            graphTitle4.Style.Visible = false;
            graphTitle4.Text = "";
            this.Modgraph.Titles.Add(graphTitle4);
            // 
            // polarCoordinateSystem4
            // 
            this.polarCoordinateSystem4.AngularAxis = this.graphAxis7;
            this.polarCoordinateSystem4.Name = "polarCoordinateSystem2";
            this.polarCoordinateSystem4.RadialAxis = this.graphAxis8;
            // 
            // graphAxis7
            // 
            this.graphAxis7.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MajorGridLineStyle.Visible = false;
            this.graphAxis7.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis7.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis7.MinorGridLineStyle.Visible = false;
            this.graphAxis7.Name = "graphAxis3";
            this.graphAxis7.Scale = numericalScale4;
            this.graphAxis7.Style.Visible = false;
            // 
            // graphAxis8
            // 
            this.graphAxis8.MajorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MajorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MajorGridLineStyle.Visible = false;
            this.graphAxis8.MinorGridLineStyle.LineColor = System.Drawing.Color.LightGray;
            this.graphAxis8.MinorGridLineStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.graphAxis8.MinorGridLineStyle.Visible = false;
            this.graphAxis8.Name = "graphAxis4";
            categoryScale4.PositionMode = Telerik.Reporting.AxisPositionMode.OnTicks;
            categoryScale4.SpacingSlotCount = 0D;
            this.graphAxis8.Scale = categoryScale4;
            this.graphAxis8.Style.Visible = false;
            // 
            // barSeries3
            // 
            this.barSeries3.ArrangeMode = Telerik.Reporting.GraphSeriesArrangeMode.Stacked100;
            graphGroup8.Name = "CatgoryGroup";
            this.barSeries3.CategoryGroup = graphGroup8;
            this.barSeries3.CoordinateSystem = this.polarCoordinateSystem4;
            this.barSeries3.DataPointLabel = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete))*10000.0/Exec(\'Modgra" +
    "ph\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)))/100";
            this.barSeries3.DataPointLabelAlignment = Telerik.Reporting.BarDataPointLabelAlignment.InsideEnd;
            this.barSeries3.DataPointLabelConnectorStyle.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries3.DataPointLabelConnectorStyle.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.barSeries3.DataPointLabelFormat = "{0:# ,##0.00}%";
            this.barSeries3.LegendItem.Value = "= Fields.DISPLAY_TEXT";
            graphGroup9.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.DISPLAY_TEXT"));
            graphGroup9.Name = "SeriesGroup";
            this.barSeries3.SeriesGroup = graphGroup9;
            this.barSeries3.X = "= ((sum(Fields.Add)+ sum(Fields.Change)+ sum(Fields.Delete))*10000.0/Exec(\'Modgra" +
    "ph\',sum(Fields.Add)+sum(Fields.Change)+sum(Fields.Delete)))/100";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.29909387230873108D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // OPUnderlyingCauseOfError
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "OPUnderlyingCauseOfError";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.AllowNull = true;
            reportParameter1.AutoRefresh = true;
            reportParameter1.AvailableValues.DataSource = this.FacilitySql;
            reportParameter1.AvailableValues.DisplayMember = "= Fields.FACILITY_NAME";
            reportParameter1.AvailableValues.ValueMember = "= Fields.MEDICAL_FACILITY_KEY";
            reportParameter1.MultiValue = true;
            reportParameter1.Name = "FacilityName";
            reportParameter1.Value = "";
            reportParameter1.Visible = true;
            reportParameter2.AllowBlank = false;
            reportParameter2.AllowNull = true;
            reportParameter2.AutoRefresh = true;
            reportParameter2.AvailableValues.DataSource = this.RoutingSql;
            reportParameter2.AvailableValues.DisplayMember = "= Fields.ROUTING_TEXT";
            reportParameter2.AvailableValues.ValueMember = "= Fields.CHART_ROUTING_KEY";
            reportParameter2.Mergeable = false;
            reportParameter2.Name = "Routing";
            reportParameter2.Value = "";
            reportParameter2.Visible = true;
            reportParameter3.AllowBlank = false;
            reportParameter3.AllowNull = true;
            reportParameter3.AutoRefresh = true;
            reportParameter3.AvailableValues.DataSource = this.ChartTypesql;
            reportParameter3.AvailableValues.DisplayMember = "= Fields.CHART_TYPE_TEXT";
            reportParameter3.AvailableValues.ValueMember = "= Fields.CHART_TYPE_KEY";
            reportParameter3.MultiValue = true;
            reportParameter3.Name = "ChartType";
            reportParameter3.Value = "";
            reportParameter3.Visible = true;
            reportParameter4.AllowNull = true;
            reportParameter4.AutoRefresh = true;
            reportParameter4.AvailableValues.DataSource = this.CoderSql;
            reportParameter4.AvailableValues.DisplayMember = "= Fields.DISPLAY_NAME";
            reportParameter4.AvailableValues.ValueMember = "= Fields.CRA_PERSONNEL_ID";
            reportParameter4.MultiValue = true;
            reportParameter4.Name = "Coder";
            reportParameter4.Value = "";
            reportParameter4.Visible = true;
            reportParameter5.AllowBlank = false;
            reportParameter5.AllowNull = true;
            reportParameter5.AutoRefresh = true;
            reportParameter5.Mergeable = false;
            reportParameter5.Name = "UserId";
            reportParameter5.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter5.Value = "";
            reportParameter5.Visible = true;
            reportParameter6.AllowBlank = false;
            reportParameter6.AllowNull = true;
            reportParameter6.AutoRefresh = true;
            reportParameter6.Mergeable = false;
            reportParameter6.Name = "IcdType";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter6.Value = "2";
            reportParameter6.Visible = true;
            reportParameter7.AllowBlank = false;
            reportParameter7.AllowNull = true;
            reportParameter7.AutoRefresh = true;
            reportParameter7.Mergeable = false;
            reportParameter7.Name = "Startdate";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter7.Value = "";
            reportParameter7.Visible = true;
            reportParameter8.AllowBlank = false;
            reportParameter8.AllowNull = true;
            reportParameter8.AutoRefresh = true;
            reportParameter8.Mergeable = false;
            reportParameter8.Name = "EndDate";
            reportParameter8.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter8.Value = "";
            reportParameter8.Visible = true;
            reportParameter9.AllowBlank = false;
            reportParameter9.AllowNull = true;
            reportParameter9.AutoRefresh = true;
            reportParameter9.Mergeable = false;
            reportParameter9.Name = "CodeTypeId";
            reportParameter9.Value = "1";
            reportParameter9.Visible = true;
            reportParameter10.AllowBlank = false;
            reportParameter10.AllowNull = true;
            reportParameter10.AutoRefresh = true;
            reportParameter10.Mergeable = false;
            reportParameter10.Name = "ChartTypeName";
            reportParameter10.Value = "-1";
            reportParameter10.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Apex.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Book Antiqua";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Book Antiqua";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(103)))), ((int)(((byte)(109)))));
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(185)))), ((int)(((byte)(102)))));
            styleRule4.Style.Font.Name = "Book Antiqua";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Apex.TableGroup")});
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector3});
            styleRule5.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(237)))));
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Font.Name = "Book Antiqua";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule6.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule6.Style.Color = System.Drawing.Color.Black;
            styleRule6.Style.Font.Name = "Tahoma";
            styleRule6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule7.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector4});
            styleRule7.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule7.Style.Font.Name = "Tahoma";
            styleRule7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule8.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector5});
            styleRule8.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule8.Style.Font.Name = "Tahoma";
            styleRule8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector6.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableGroup")});
            styleRule9.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector6});
            styleRule9.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule9.Style.Font.Name = "Tahoma";
            styleRule9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5,
            styleRule6,
            styleRule7,
            styleRule8,
            styleRule9});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(5.9000000953674316D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource FacilitySql;
        private Telerik.Reporting.SqlDataSource RoutingSql;
        private Telerik.Reporting.SqlDataSource ChartTypesql;
        private Telerik.Reporting.SqlDataSource CoderSql;
        private Telerik.Reporting.SqlDataSource OPCMSql;
        private Telerik.Reporting.Graph graph1;
        private Telerik.Reporting.PolarCoordinateSystem polarCoordinateSystem1;
        private Telerik.Reporting.GraphAxis graphAxis1;
        private Telerik.Reporting.GraphAxis graphAxis2;
        private Telerik.Reporting.SqlDataSource CptsUnderLyingErrorSql;
        private Telerik.Reporting.Graph graph2;
        private Telerik.Reporting.PolarCoordinateSystem polarCoordinateSystem2;
        private Telerik.Reporting.GraphAxis graphAxis3;
        private Telerik.Reporting.GraphAxis graphAxis4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.BarSeries barSeries8;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.BarSeries barSeries1;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.SqlDataSource PcUnderLyingErrorsql;
        private Telerik.Reporting.Graph PcGraph;
        private Telerik.Reporting.PolarCoordinateSystem polarCoordinateSystem3;
        private Telerik.Reporting.GraphAxis graphAxis5;
        private Telerik.Reporting.GraphAxis graphAxis6;
        private Telerik.Reporting.BarSeries barSeries2;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.SqlDataSource MODSql;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.Graph Modgraph;
        private Telerik.Reporting.PolarCoordinateSystem polarCoordinateSystem4;
        private Telerik.Reporting.GraphAxis graphAxis7;
        private Telerik.Reporting.GraphAxis graphAxis8;
        private Telerik.Reporting.BarSeries barSeries3;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox84;
    }
}