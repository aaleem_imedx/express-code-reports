﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportsLibrary.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("SELECT v.PatientChartId\r\n\t\t  ,\tmf.FACILITY_NAME\r\n\t\t  , v.AdmissionDate\r\n         " +
            " , v.ChartNotes\r\n          , v.CodeChangeComments\r\n          , v.CodeSequence\r\n " +
            "         , v.CodeSort\r\n          , v.ConsulantCodeChangeTypeId\r\n          , v.Co" +
            "nsultant\r\n          , v.ConsultantBaseRate\r\n          , v.ConsultantCode\r\n      " +
            "    , v.ConsultantDischDisp\r\n          , v.ConsultantMSDRGCode\r\n          , v.Co" +
            "nsultantMSDRGWeight\r\n          , v.ConsultantPOA\r\n          , v.DischargeDate\r\n " +
            "         , v.DRGVariance\r\n          , v.ENCOUNTER_NUMBER\r\n          , v.Facility" +
            "BaseRate\r\n          , v.FacilityChartPaidByMsDrg\r\n          , v.FacilityCode\r\n  " +
            "        , v.FacilityCoder\r\n          , v.FacilityDischDisp\r\n          , v.facili" +
            "tyDrgGroupId\r\n          , v.FacilityMSDRGCode\r\n          , v.FacilityMSDRGWeight" +
            "\r\n          , v.FacilityPOA\r\n          , v.ICD_TYPE_ID\r\n          , v.MR_NUMBER\r" +
            "\n          , v.Payer\r\n          , v.POAChangeComments\r\n          , v.REVIEWED_DA" +
            "TE\r\n          , v.REVIEWED_DATE_ICD\r\n          , v.ROUTING\r\n          , v.MS_DRG" +
            "_COMMENTS\r\n    FROM \r\n\t\tdbo.PATIENT_CHART_CRA_VIEW AS v -- Join on new ICD-10 vi" +
            "ew\r\n\t\tJOIN tvf_CSVtoTable(@FilterChartTypes) ct ON v.CHART_TYPE_KEY = ct.Val\r\n\t\t" +
            "JOIN tvf_CSVtoTable(@FilterFacilities) f ON v.MEDICAL_FACILITY_KEY = f.Val\r\n\t\tJO" +
            "IN tvf_CSVtoTable(@FilterRoutings) r ON v.ROUTING = r.Val\r\n\t\tJOIN tvf_CSVtoTable" +
            "(@FilterIcdTypes) icd ON v.ICD_TYPE_ID = icd.Val\r\n\t\tJOIN CRA_FACILITY_PERSONNEL_" +
            "XREF cfpx ON v.FacilityCoder_XREF_ID = cfpx.CRA_FACILITY_PERSONNEL_XREF_ID\r\n\t\tJO" +
            "IN tvf_CSVtoTable(@FilterFacilityCoders) fc ON fc.Val = cfpx.CRA_PERSONNEL_ID\r\n\t" +
            "\tJOIN MEDICAL_FACILITY mf on v.MEDICAL_FACILITY_KEY = mf.MEDICAL_FACILITY_KEY\r\n " +
            "   WHERE v.CHART_STATUS_KEY IN (10,12,14)\r\n\t\tAND CAST(v.REVIEWED_DATE AS DATE) B" +
            "ETWEEN @StartDate AND @EndDate\r\n\t\tAND ISNULL(v.question_4, 0) = 0  AND v.ENCOUNT" +
            "ER_NUMBER=@Account")]
        public string set {
            get {
                return ((string)(this["set"]));
            }
            set {
                this["set"] = value;
            }
        }
    }
}
